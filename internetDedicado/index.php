<?php session_start();

	require_once("seguridad.php");
	require_once("../header.php");
?>
<body>
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Calculadora de Internet DedicadoTest</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
			<a href=<?php echo $URL_home_pricipal; ?>>Volver</a>
		</span>
 
	</header>
	<section class="main">
		<article class="panelIzq">
			<?php
			require('funciones.php');
			require_once('../conexion/conexion_bd.php');

				$rut_ejecutivo =null;
				$nombre_ejecutivo=null;
				$segmento_cliente=null;
				$rut_cliente=null;
				$nombre_cliente=null;
				
				$numero_oft=null; 
				$costo_oft=null;
				$cargo_inicial=null;
				$cargo_inicial_text=null;
				$region=null;	
				$comuna=null;
				$bwn=null;
				$bwi=null;
				$descuento=null;
				$medio_acceso=null;

			if (isset($_POST['calcular'])) {
				$rut_ejecutivo =$_POST['rut_ejecutivo'];
				$nombre_ejecutivo=$_POST['nombre_ejecutivo'];
				$segmento_cliente=$_POST['segmento_cliente'];
				$rut_cliente=$_POST['rut_cliente'];
				$nombre_cliente=$_POST['nombre_cliente'];
				
				$numero_oft=$_POST['numero_oft']; 
				$costo_oft=$_POST['costo_oft'];
				$cargo_inicial=$_POST['cargo_inicial'];
				if (isset($_POST['cargo_inicial']) and $cargo_inicial=="Si") {
					$cargo_inicial_text=$_POST['cargo_inicial_text'];
				}
				//else{
				//	$cargo_inicial_text=null;
				//}
				$region=$_POST['region'];
				$comuna=$_POST['comuna'];
				$bwn=$_POST['bwn'];
				$bwi=$_POST['bwi'];
				$descuento=$_POST['descuento'];
				$medio_acceso=$_POST['medio_acceso'];			
			}

			if (isset($_POST['cargo_inicial'])) {
				$rut_ejecutivo =$_POST['rut_ejecutivo'];
				$nombre_ejecutivo=$_POST['nombre_ejecutivo'];
				$segmento_cliente=$_POST['segmento_cliente'];
				$rut_cliente=$_POST['rut_cliente'];
				$nombre_cliente=$_POST['nombre_cliente'];
				
				$numero_oft=$_POST['numero_oft']; 
				$costo_oft=$_POST['costo_oft'];
				$cargo_inicial=$_POST['cargo_inicial'];
				//$cargo_inicial_text=$_POST['cargo_inicial_text'];
				$region=$_POST['region'];
				$comuna=$_POST['comuna'];
				$bwn=$_POST['bwn'];
				$bwi=$_POST['bwi'];			
				$descuento=$_POST['descuento'];
				$medio_acceso=$_POST['medio_acceso'];
			}

			if (isset($_POST['region'])) {
				$rut_ejecutivo =$_POST['rut_ejecutivo'];
				$nombre_ejecutivo=$_POST['nombre_ejecutivo'];
				$segmento_cliente=$_POST['segmento_cliente'];
				$rut_cliente=$_POST['rut_cliente'];
				$nombre_cliente=$_POST['nombre_cliente'];
				
				$numero_oft=$_POST['numero_oft']; 
				$costo_oft=$_POST['costo_oft'];	
				$cargo_inicial=$_POST['cargo_inicial'];
				//$cargo_inicial_text=$_POST['cargo_inicial_text'];
				$region=$_POST['region'];
				$comuna=$_POST['comuna'];
				$bwn=$_POST['bwn'];
				$bwi=$_POST['bwi'];			
				$descuento=$_POST['descuento'];
				$medio_acceso=$_POST['medio_acceso'];
			}

			if (isset($_POST['bwn'])) {
				$rut_ejecutivo =$_POST['rut_ejecutivo'];
				$nombre_ejecutivo=$_POST['nombre_ejecutivo'];
				$segmento_cliente=$_POST['segmento_cliente'];
				$rut_cliente=$_POST['rut_cliente'];
				$nombre_cliente=$_POST['nombre_cliente'];
				
				$numero_oft=$_POST['numero_oft']; 
				$costo_oft=$_POST['costo_oft'];	
				$cargo_inicial=$_POST['cargo_inicial'];
				//$cargo_inicial_text=$_POST['cargo_inicial_text'];
				$region=$_POST['region'];
				$comuna=$_POST['comuna'];
				$bwn=$_POST['bwn'];
				$bwi=$_POST['bwi'];			
				$descuento=$_POST['descuento'];
				$medio_acceso=$_POST['medio_acceso'];
			}

			if (isset($_POST['bwi'])) {
				$rut_ejecutivo =$_POST['rut_ejecutivo'];
				$nombre_ejecutivo=$_POST['nombre_ejecutivo'];
				$segmento_cliente=$_POST['segmento_cliente'];
				$rut_cliente=$_POST['rut_cliente'];
				$nombre_cliente=$_POST['nombre_cliente'];
				
				$numero_oft=$_POST['numero_oft']; 
				$costo_oft=$_POST['costo_oft'];	
				$cargo_inicial=$_POST['cargo_inicial'];
				//$cargo_inicial_text=$_POST['cargo_inicial_text'];
				$region=$_POST['region'];
				$comuna=$_POST['comuna'];
				$bwn=$_POST['bwn'];
				$bwi=$_POST['bwi'];			
				$descuento=$_POST['descuento'];
				$medio_acceso=$_POST['medio_acceso'];
			}

			
			if (isset($_POST['recotizar'])) {
				$rut_ejecutivo =null;
				$nombre_ejecutivo=null;
				$segmento_cliente=null;
				$rut_cliente=null;
				$nombre_cliente=null;
				
				$numero_oft=null; 
				$costo_oft=null;
				$cargo_inicial=null;
				$cargo_inicial_text=null;
				$region=null;
				$comuna=null;
				$bwn=null;
				$bwi=null;				
				$descuento=null;
				$medio_acceso=null;
			}

			if (isset($_POST['limpiar'])) {
				$rut_ejecutivo =null;
				$nombre_ejecutivo=null;
				$segmento_cliente=null;
				$rut_cliente=null;
				$nombre_cliente=null;
				
				$numero_oft=null; 
				$costo_oft=null;
				$cargo_inicial=null;
				$cargo_inicial_text=null;
				$region=null;
				$comuna=null;
				$bwn=null;
				$bwi=null;		
				$descuento=null;	
				$medio_acceso=null;	
			}

			?>
			<h3>Favor ingrese los siguientes datos:</h3>
			<form action="index.php" method="POST" name="form">
						
						<table>
							<tr>
								<td>Rut Ejecutivo:	</td>
								<td><input type="text" name="rut_ejecutivo"  onchange="Valida_Rut(this)" value="<?php echo $rut_ejecutivo; ?>"></input></td>
								<td>(Ej: 99999999-9)</td>
							</tr>
							<tr>
								<td>Nombre Ejecutivo:</td>
								<td><input type="text" name="nombre_ejecutivo" value="<?php echo $nombre_ejecutivo; ?>"></input></td>
							</tr>
							<tr><td> <br> </td></tr>
							<tr>
								<?php 

								$consulta_segmento_cliente= "SELECT * FROM SEGMENTO_CLIENTE order by nombre_segmento";
								$ejecutar_segmento_cliente=mysql_query($consulta_segmento_cliente,$conexion) or die(mysql_error());
								$filas_segmento_cliente=mysql_fetch_assoc($ejecutar_segmento_cliente);
								$numero_segmento_cliente=mysql_num_rows($ejecutar_segmento_cliente);
								
								?>

								<td>Segmento Cliente:</td>
								<td><select name="segmento_cliente">
									<option>Ingrese Segmento</option>
									<?php do{?>
									<option <?php if ($segmento_cliente==$filas_segmento_cliente['nombre_segmento']) { echo "selected"; }?>><?php echo $filas_segmento_cliente['nombre_segmento']?></option>
									<?php } while ($filas_segmento_cliente= mysql_fetch_assoc($ejecutar_segmento_cliente));?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Rut Cliente:</td>
								<td><input type="text" name="rut_cliente" onchange="Valida_Rut(this)" value="<?php echo $rut_cliente; ?>"></input></td>
								<td>(Ej: 99999999-9)</td>
							</tr>
							<tr>
								<td>Nombre Cliente:</td>
								<td><input type="text" name="nombre_cliente" value="<?php echo $nombre_cliente; ?>"></input></td>
							</tr>
							<tr><td> <br> </td></tr>
							<tr>
								<td>Número OFT:</td>
								<td><input type="text" name="numero_oft" maxlength ="11" onchange="soloNumero(this)" value="<?php echo $numero_oft; ?>"></input></td>
							</tr>
							<tr>
								<?php 

								$consulta_segmento_cliente= "SELECT * FROM MEDIO_ACCESO";
								$ejecutar_segmento_cliente=mysql_query($consulta_segmento_cliente,$conexion) or die(mysql_error());
								$filas_segmento_cliente=mysql_fetch_assoc($ejecutar_segmento_cliente);
								$numero_segmento_cliente=mysql_num_rows($ejecutar_segmento_cliente);
								
								?>
								<td>Medio de Acceso:</td>
								<td><select name="medio_acceso">
									<option>Ingrese Acceso</option>
									<?php do{?>
									<option  value="<?php echo $filas_segmento_cliente['id_medio_acceso']?>" <?php if ($medio_acceso==$filas_segmento_cliente['id_medio_acceso']) { echo "selected"; }?>><?php echo $filas_segmento_cliente['nombre_medio_acceso']?></option>
									<?php } while ($filas_segmento_cliente= mysql_fetch_assoc($ejecutar_segmento_cliente));?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Costo Total OFT (CLP):</td>
								<td><input type="text" name="costo_oft" onkeyup="format(this)" onchange="format(this)" value="<?php echo $costo_oft; ?>"></input></td>
								<td>Desea pasar OFT a cargo inicial:</td>
								<td>
									<select name="cargo_inicial" id="cargo_inicial" onchange="submit()">
										
										<option <?php if ($cargo_inicial=="No") { echo "selected"; }?>>No</option>
										<option <?php if ($cargo_inicial=="Si") { echo "selected"; }?>>Si</option>
									</select>	
								</td>
							</tr>
							<?php if (isset($_POST['cargo_inicial']) and $cargo_inicial=="Si") {
										if ($segmento_cliente!="Ingrese Segmento" and str_replace('.', '', $_POST['costo_oft'])>1000000) {
											
											$consulta_segmento_cliente= "SELECT * FROM SEGMENTO_CLIENTE where nombre_segmento='$segmento_cliente'";
											mysql_query("SET NAMES 'utf8'");
											$ejecutar_segmento_cliente=mysql_query($consulta_segmento_cliente,$conexion) or die(mysql_error());
											$filas_segmento_cliente=mysql_fetch_assoc($ejecutar_segmento_cliente);

											if (str_replace('.', '', $_POST['costo_oft']) > $filas_segmento_cliente['factibilidad_renta']) {
												$limite_renta = str_replace('.', '', $_POST['costo_oft'])-$filas_segmento_cliente['factibilidad_renta'];
												$texto_inicial = $limite_renta;
											} else {
												
												if (isset($_POST['cargo_inicial_text'])) {
													$limite_renta = 0;
													$texto_inicial = $_POST['cargo_inicial_text'];
												} else {
													$limite_renta = 0;
													$texto_inicial = null;
												}
												

												
											}
											
											?>
											<tr>
												<td width="150">Cuánto desea pasar a cargo inicial (CLP):</td>
												<td ><input type="text" name="cargo_inicial_text" id="cargo_inicial_text" autofocus onfocus="formatoCargoInicial(this,<?php echo $limite_renta ?>,<?php echo str_replace('.', '', $_POST['costo_oft']) ?>)" onchange="formatoCargoInicial(this,<?php echo $limite_renta ?>,<?php echo str_replace('.', '', $_POST['costo_oft']) ?>)" value="<?php echo $texto_inicial ?>"></input></td>
												<?php if ($texto_inicial!=null){ ?>
													<td class="letra_chica" colspan="2">(Este es el mínimo valor permitido a pasar como cargo inicial)</td>
												<?php } ?>
											</tr>
										<?php 
												
											} else{ ?>
									
											<script type="text/javascript">alert("Para pasar factibilidad a cargo inicial debe:\n- Ingresar Segmento Cliente\n- Ingresar costo de OFT\n- El costo de OFT debe ser mayor a $1.000.000");</script>
											<script type="text/javascript">document.getElementById("cargo_inicial").value = "No";</script>									
									<?php
									} 
									}?>
							 
							<tr><td> <br> </td></tr>
							<tr>
								<td colspan=3><strong> Favor indicar los datos de la zona donde se instalará el servicio:</strong></td>
							</tr>
							<tr>
								<?php 

								$consulta_region= "SELECT * FROM region";
								mysql_query("SET NAMES 'utf8'");
								$ejecutar_region=mysql_query($consulta_region,$conexion) or die(mysql_error());
								$filas_region=mysql_fetch_assoc($ejecutar_region);
								$numero_region=mysql_num_rows($ejecutar_region);
								
								?>

								<td>Región:</td>
								<td colspan="3"><select name="region" onchange="submit()" size="1">
									<option>Ingrese Región donde se instalará el ID</option>
									<?php do{?>
									<option value="<?php echo $filas_region['id']?>" <?php if ($region==$filas_region['id']) { echo "selected"; }?>><?php echo $filas_region['nombre']?></option>
									<?php } while ($filas_region= mysql_fetch_assoc($ejecutar_region));?>
									</select>
								</td>
							</tr>
							<tr>
							<?php


									$consulta_comuna= "SELECT * FROM comuna where comuna.id_region='$region'";
									mysql_query("SET NAMES 'utf8'");
									$ejecutar_comuna=mysql_query($consulta_comuna,$conexion) or die(mysql_error());
									$filas_comuna=mysql_fetch_assoc($ejecutar_comuna);
									$numero_comuna=mysql_num_rows($ejecutar_comuna);
								?>

								<td>Comuna:</td>
								<td>
									<select name="comuna">
									
							        <?php
							        if ($region!="0"){ 
							          do{ ?>
							            <option value="<?php echo $filas_comuna['id']?>" <?php if ($comuna==$filas_comuna['id']) { echo "selected"; }?>><?php echo $filas_comuna['nombre']?></option>
							          <?php
							          }while($filas_comuna = mysql_fetch_array($ejecutar_comuna));
							          
							        }else{
							          echo "<option selected>Ingrese Comuna donde se instalará el ID</option>";
							        }
							        ?>
							        </select>
								</td>
							</tr>
							<tr>

							<tr>
								<?php 

								$consulta_bwn= "SELECT * FROM tarifas where vigencia='1' group by bw_nac";
								mysql_query("SET NAMES 'utf8'");
								$ejecutar_bwn=mysql_query($consulta_bwn,$conexion) or die(mysql_error());
								$filas_bwn=mysql_fetch_assoc($ejecutar_bwn);
								$numero_bwn=mysql_num_rows($ejecutar_bwn);
								
								?>

								<td>BW Nacional:</td>
								<td><select name="bwn" onchange="submit()">
									<option>Ingrese BW</option>
									<?php do{?>
									<option value="<?php echo $filas_bwn['bw_nac']?>" <?php if ($bwn==$filas_bwn['bw_nac']) { echo "selected"; }?>><?php echo $filas_bwn['bw_nac']?></option>
									<?php } while ($filas_bwn= mysql_fetch_assoc($ejecutar_bwn));?>
									</select>
								</td>
							</tr>
							<tr>
							<?php


									$consulta_bwi= "SELECT * FROM tarifas where tarifas.bw_nac='$bwn' and vigencia = '1' group by tarifas.bw_int";
									mysql_query("SET NAMES 'utf8'");
									$ejecutar_bwi=mysql_query($consulta_bwi,$conexion) or die(mysql_error());
									$filas_bwi=mysql_fetch_assoc($ejecutar_bwi);
									$numero_bwi=mysql_num_rows($ejecutar_bwi);

								?>

								<td>BW Internacional:</td>
								<td>
									<select name="bwi" onchange="submit()" size="1">
									
							        <?php
							        if ($bwi!="0"){ 
							          do{ ?>
							            <option value="<?php echo $filas_bwi['bw_int']?>" <?php if ($bwi==$filas_bwi['bw_int']) { echo "selected"; }?>><?php echo $filas_bwi['bw_int']?></option>
							          <?php
							      	}while($filas_bwi = mysql_fetch_array($ejecutar_bwi));
							         
							        }else{
							          echo "<option selected>Ingrese BW</option>";
							        }
							        ?>
							        </select>
								</td>
							</tr>

							<tr>
								<td colspan=3><strong> Descuentos:</strong></td>
							</tr>
							<tr>
								<?php 

								$consulta_descuento= "SELECT * FROM descuento WHERE vigencia='1'";
								mysql_query("SET NAMES 'utf8'");
								$ejecutar_descuento=mysql_query($consulta_descuento,$conexion) or die(mysql_error());
								$filas_descuento=mysql_fetch_assoc($ejecutar_descuento);
								$numero_descuento=mysql_num_rows($ejecutar_descuento);
								
								?>

								<td>Tipo de Descuento:</td>
								<td colspan="3"><select id="descuento" name="descuento" size="1" onchange="submit()">
									
									<?php do{?>
									<option value="<?php echo $filas_descuento['id_descuento']?>" <?php if ($descuento==$filas_descuento['id_descuento']) { echo "selected"; }?>><?php echo $filas_descuento['nombre_descuento']?></option>
									<?php } while ($filas_descuento= mysql_fetch_assoc($ejecutar_descuento));?>
									</select>

								<?php 

								if (isset($_POST['descuento'])) { 

									$id_descuento = $_POST['descuento'];
									$medio_acceso = $_POST['medio_acceso'];

									$consulta_descuento= "SELECT * FROM descuento where id_descuento='$id_descuento'";
									mysql_query("SET NAMES 'utf8'");
									$ejecutar_descuento=mysql_query($consulta_descuento,$conexion) or die(mysql_error());
									$filas_descuento=mysql_fetch_assoc($ejecutar_descuento);
									$numero_descuento=mysql_num_rows($ejecutar_descuento);

									if (str_replace('.', '', $_POST['costo_oft'])>$filas_descuento['limite_oft']) { ?>
										<script type="text/javascript">alert("Este descuento no es aplicable para este tipo de cotización");</script>
										<script type="text/javascript">document.form.descuento.selectedIndex="0";</script>
										
									<?php

									} else{

									if ($filas_descuento['id_descuento']=="4") { 
										//if ($medio_acceso=="2") { 
										//if ($region=="15") { 
										switch ($bwn.$bwi) {
										    case 101: break; 
										        
										    case 1010: break; 

										    case 1515: break; 
										        
										    case 2020: break; 
										        
										    case 3030: break;  

										    case 5050: break; 
										        
										    case 100100: break; 
										    
										    default:
										        ?>
													<script type="text/javascript">alert("Este descuento no es aplicable para este tipo de cotización");</script>
													<script type="text/javascript">document.form.descuento.selectedIndex="0";</script>
												<?php
										        break;    
										}
										/*}else{
											?>
													<script type="text/javascript">alert("Este descuento no es aplicable para este tipo de cotización");</script>
													<script type="text/javascript">document.form.descuento.selectedIndex="0";</script>
												<?php
										}*/
										/*}else{
													?>
													<script type="text/javascript">alert("Este descuento no es aplicable para este tipo de cotización");</script>
													<script type="text/javascript">document.form.descuento.selectedIndex="0";</script>
												<?php	
										}*/
									}
									} 
									?>
									
									
								<?php  } else {
									# code...
								}
								

								 ?>

								</td>
							</tr>
                                                        <table><tr>
								<td><br><input type="submit" name="calcular" value="Calcular" class="boton"></td>
								<td><br><input type="submit" name="limpiar" value="Limpiar" class="boton"></td>
                                                                <td> <br><input type="submit" name="volver" value="Volver" class="boton" onClick="document.form.action='<?php echo $URL_home_pricipal; ?>'; document.form.submit();"></td>
							</tr></table>
							
                                                        
                                                        
						</table>
		</article>
		<article class="panelDer">
			<?php
			if (isset($_POST['calcular'])){ 

				if ($_POST['rut_ejecutivo'] != "" and $_POST['nombre_ejecutivo'] != "" and $_POST['segmento_cliente'] != "Ingrese Segmento" and $_POST['rut_cliente'] != "" and 
					$_POST['nombre_cliente'] != "" and $_POST['numero_oft'] != "" and $_POST['costo_oft'] != "" and $_POST['region'] != "" and $_POST['comuna'] != ""
					and $_POST['bwn'] != "" and $_POST['bwi'] != "" and $_POST['medio_acceso']!="Ingrese Acceso"){

				$consulta_descuento= "SELECT * FROM descuento where id_descuento='$id_descuento'";
									$ejecutar_descuento=mysql_query($consulta_descuento,$conexion) or die(mysql_error());
									$filas_descuento=mysql_fetch_assoc($ejecutar_descuento);
									$numero_descuento=mysql_num_rows($ejecutar_descuento);

									if (str_replace('.', '', $_POST['costo_oft'])>$filas_descuento['limite_oft']) { 
										echo "- El descuento escogido no es aplicable para este tipo de cotización";							
									 
				} else {
					
				$rut_ejecutivo =$_POST['rut_ejecutivo'];
				$nombre_ejecutivo=$_POST['nombre_ejecutivo'];
				$segmento_cliente=$_POST['segmento_cliente'];
				$rut_cliente=$_POST['rut_cliente'];
				$nombre_cliente=$_POST['nombre_cliente'];
				
				$numero_oft=$_POST['numero_oft']; 
				$costo_oft=str_replace('.', '', $_POST['costo_oft']);

				
				if (isset($_POST['cargo_inicial']) and $cargo_inicial=="Si") {
					$cargo_inicial_text=str_replace('.', '', $_POST['cargo_inicial_text']);
				}

				$region=$_POST['region'];
				$comuna=$_POST['comuna'];
				$bwn=$_POST['bwn'];
				$bwi=$_POST['bwi'];	
				$id_descuento=$_POST['descuento'];
				$id_medio_acceso=$_POST['medio_acceso'];

				$consulta_descuento= "SELECT * FROM descuento where id_descuento='$id_descuento'";
								mysql_query("SET NAMES 'utf8'");
								$ejecutar_descuento=mysql_query($consulta_descuento,$conexion) or die(mysql_error());
								$filas_descuento=mysql_fetch_assoc($ejecutar_descuento);
								$numero_descuento=mysql_num_rows($ejecutar_descuento);
				$porcentaje_descuento=$filas_descuento['porcentaje_descuento'];
				$nombre_descuento=$filas_descuento['nombre_descuento'];

				$consulta_tarifa= "SELECT tarifas.id as 'id_tarifa',tarifas.*, comuna_zona.*, comuna.nombre as 'nombre_comuna', zona.* 
									FROM tarifas, comuna_zona, comuna, zona WHERE
									comuna.id=comuna_zona.id_comuna 
									and comuna_zona.id_zona=zona.id
									and zona.id=tarifas.id_zona
									and tarifas.bw_nac = '$bwn'
									and tarifas.bw_int = '$bwi'
									and tarifas.vigencia = '1'
									and comuna.id='$comuna'";
				mysql_query("SET NAMES 'utf8'");
				$ejecutar_tarifa=mysql_query($consulta_tarifa,$conexion) or die(mysql_error());
				$filas_tarifa=mysql_fetch_assoc($ejecutar_tarifa);
				$numero_tarifa=mysql_num_rows($ejecutar_tarifa);

				$nombre_comuna = $filas_tarifa['nombre_comuna'];
				$id_tarifa = $filas_tarifa['id_tarifa'];

				$cartilla_12 = $filas_tarifa['precio_12'];
				$cartilla_24 = $filas_tarifa['precio_24'];
				$cartilla_36 = $filas_tarifa['precio_36'];
				$cartilla_48 = $filas_tarifa['precio_48'];

				$consulta_parametros= "SELECT * FROM parametros WHERE id = (SELECT MAX( id ) FROM parametros )";
				mysql_query("SET NAMES 'utf8'");
				$ejecutar_parametros=mysql_query($consulta_parametros,$conexion) or die(mysql_error());
				$filas_parametros=mysql_fetch_assoc($ejecutar_parametros);

				$consulta_parametros_factor= "SELECT * FROM parametros_factor WHERE id = (SELECT MAX( id ) FROM parametros_factor )";
				mysql_query("SET NAMES 'utf8'");
				$ejecutar_parametros_factor=mysql_query($consulta_parametros_factor,$conexion) or die(mysql_error());
				$filas_parametros_factor=mysql_fetch_assoc($ejecutar_parametros_factor);

				$consulta_segmento_cliente= "SELECT * FROM segmento_cliente WHERE nombre_segmento='$segmento_cliente'";
				mysql_query("SET NAMES 'utf8'");
				$ejecutar_segmento_cliente=mysql_query($consulta_segmento_cliente,$conexion) or die(mysql_error());
				$filas_segmento_cliente=mysql_fetch_assoc($ejecutar_segmento_cliente);

				$dolar = $filas_parametros['dolar'];
				$uf = $filas_parametros['uf'];
				$tasa_wacc = $filas_parametros['tasa_wacc'];

				$wacc_mensual = $tasa_wacc/12;

				$factor_12 = $filas_parametros_factor['factor_12'];
				$factor_24 = $filas_parametros_factor['factor_24'];
				$factor_36 = $filas_parametros_factor['factor_36'];
				$factor_48 = $filas_parametros_factor['factor_48'];

				$precio_total_12 = ($cartilla_12+sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_12));
				$precio_total_24 = ($cartilla_24+sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_24));
				$precio_total_36 = ($cartilla_36+sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_36));

				$precio_total_12_dscto = round(($cartilla_12+sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_12))*(1-$porcentaje_descuento),2);
				$precio_total_24_dscto = round(($cartilla_24+sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_24))*(1-$porcentaje_descuento),2);
				$precio_total_36_dscto = round(($cartilla_36+sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_36))*(1-$porcentaje_descuento),2);
				
				if (stristr($segmento_cliente, "PYME")==FALSE) {
					$precio_total_48 = ($cartilla_48+sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_48));
					$precio_total_48_dscto = round(($cartilla_48+sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_48))*(1-$porcentaje_descuento),2);
				} else {
					$precio_total_48 =0;
				}
				

				

				$precio_cuota_1 = calculoCuotas($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], 1, $filas_segmento_cliente['margen'],$wacc_mensual,$uf);
				$precio_cuota_2 = calculoCuotas($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], 2, $filas_segmento_cliente['margen'],$wacc_mensual,$uf);
				$precio_cuota_3 = calculoCuotas($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], 3, $filas_segmento_cliente['margen'],$wacc_mensual,$uf);
				
				$codigo_identificador = cadenaAleatoria().$numero_oft;

				$fecha_cotizacion = date("Y-m-d");

				
								if ($numero_tarifa !=0) {

									$insertar_traslado = "INSERT INTO REGISTRO_INTERNET(id, rut_canal, nombre_canal, segmento_canal, rut_cliente, 
										nombre_cliente, nro_oft, costo_oft, precio_12, precio_24, precio_36, precio_48, cuota_1, cuota_2, cuota_3, 
										fecha, id_tarifa, codigo_identificador,ingresado, ingresado_precio, ingresado_cuota, fecha_ingreso,id_descuento,id_medio_acceso,id_comuna) 
										VALUES('', '$rut_ejecutivo', '$nombre_ejecutivo', '$segmento_cliente', '$rut_cliente', 
										'$nombre_cliente', '$numero_oft', '$costo_oft', $precio_total_12, $precio_total_24, $precio_total_36, $precio_total_48, 
										$precio_cuota_1, $precio_cuota_2, $precio_cuota_3, '$fecha_cotizacion', '$id_tarifa','$codigo_identificador','','','','','$id_descuento','$id_medio_acceso','$comuna')";
				
					mysql_query("SET NAMES 'utf8'");
					mysql_query("LOCK TABLES REGISTRO_INTERNET WRITE",$conexion);
					$ejecuta_traslado = mysql_query($insertar_traslado, $conexion) or die(mysql_error());
					$ultimo_id = mysql_insert_id($conexion);
					mysql_query("UNLOCK TABLES",$conexion);
								?>
								<h3>Tarifas por Internet Dedicado</h3>
								<table>
									<?php if ($nombre_comuna=="PUDAHUEL"){
											echo "Estimados, si el servicio será entregado en Aeropuerto o Datacenter NO SE PUEDE UTILIZAR Cartilla. Si es así, favor levantar requerimiento a Mesa de Precios";
										
									} else{
										if($nombre_comuna=="SANTIAGO"){
										echo "Estimados, si el servicio será entregado en Datacenter NO SE PUEDE UTILIZAR Cartilla. Si es así, favor levantar requerimiento a Mesa de Precios";
										}
									}
										

									 ?>
									<tr>
										<td colspan="3" align="left">
											- Según OFT <?php echo $numero_oft;?> , las tarifas para ID <?php echo $bwn;?>/<?php echo $bwi;?> son las siguientes:
										</td>
									</tr>
									
									<tr>
										<?php 
										if (isset($_POST['cargo_inicial_text'])) {
											$validador_cargo_inicial = str_replace('.', '', $_POST['cargo_inicial_text']);
										}else{
											$validador_cargo_inicial = "";
										}



										if ($costo_oft>$filas_segmento_cliente['factibilidad_renta'] or $validador_cargo_inicial) { ?>
											<table>
												<tr>
													<td>- Cargo inicial:</td>
												</tr>
												<tr>
													<td >1 cuota de:</td>
													<td align="center"><?php echo $precio_cuota_1; ?></td>
													<td align ="center">UF/Mes + IVA</td>
												</tr>
												<tr>
													<td >2 cuotas de:</td>
													<td align="center"><?php echo $precio_cuota_2; ?></td>
													<td align ="center">UF/Mes + IVA</td>
												</tr>
												<tr>
													<td >3 cuotas de:</td>
													<td align="center"><?php echo $precio_cuota_3; ?></td>
													<td align ="center">UF/Mes + IVA</td>
												</tr>
												<tr>
													<td><br>- Adicionalmente una renta de:</td>
												</tr>
											</table>
										<?php } ?>
										<table >
											<tr>
												<th align ="center"></th>
												<th align ="center">Precio Cartilla</th>
												<th align ="center"></th>
												<?php if ($costo_oft-$cargo_inicial_text>1000000){ ?> 
												<th align ="center">Sobrecosto a renta</th>
												<th align ="center"></th>
												<th align ="center" colspan="2">Tarifa Total a Renta</th>
												<?php } ?>
												<?php if ($porcentaje_descuento!=0){ ?> 
												<th align ="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
												<th align ="center" colspan="2"><?php echo "Tarifa con ".($nombre_descuento) ?></th>
												<?php } ?>
											</tr>
											<tr>
												<td >12 pagos de:</td>
												<td align="center"><?php echo $cartilla_12; ?></td>
												<?php if ($costo_oft-$cargo_inicial_text>1000000){ ?> 
												<td align ="center"> + </td>
												<td align ="center"><?php echo sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_12); ?></td>
												<td align ="center"> = </td>
												<td align ="center"><?php echo $precio_total_12; ?></td>
												<?php } ?>
												<td align ="center">UF/Mes + IVA</td>
												<?php if ($porcentaje_descuento!=0){ ?> 
												<td align ="center">/</td>
												<td align ="center"><?php echo $precio_total_12_dscto; ?></td>
												<td align ="center">UF/Mes + IVA</td>
												<?php } ?>
											</tr>
											<tr>
												<td >24 pagos de:</td>
												<td align="center"><?php echo $cartilla_24; ?></td>
												<?php if ($costo_oft-$cargo_inicial_text>1000000){ ?> 
												<td align ="center"> + </td>
												<td align ="center"><?php echo sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_24); ?></td>
												<td align ="center"> = </td>
												<td align ="center"><?php echo $precio_total_24; ?></td>
												<?php } ?>
												<td align ="center">UF/Mes + IVA</td>
												<?php if ($porcentaje_descuento!=0){ ?> 
												<td align ="center">/</td>
												<td align ="center"><?php echo $precio_total_24_dscto; ?></td>
												<td align ="center">UF/Mes + IVA</td>
												<?php } ?>
											</tr>
											<tr>
												<td >36 pagos de:</td>
												<td align="center"><?php echo $cartilla_36; ?></td>
												<?php if ($costo_oft-$cargo_inicial_text>1000000){ ?> 
												<td align ="center"> + </td>
												<td align ="center"><?php echo sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_36); ?></td>
												<td align ="center"> = </td>
												<td align ="center"><?php echo $precio_total_36; ?></td>
												<?php } ?>
												<td align ="center">UF/Mes + IVA</td>
												<?php if ($porcentaje_descuento!=0){ ?> 
												<td align ="center">/</td>
												<td align ="center"><?php echo $precio_total_36_dscto; ?></td>
												<td align ="center">UF/Mes + IVA</td>
												<?php } ?>
											</tr>
											<?php 
											if (stristr($segmento_cliente, "PYME")==FALSE) { ?>
											<tr>
												<td >48 pagos de:</td>
												<td align="center"><?php echo $cartilla_48; ?></td>
												<?php if ($costo_oft-$cargo_inicial_text>1000000){ ?> 
												<td align ="center"> + </td>
												<td align ="center"><?php echo sobrecosto($cargo_inicial_text,$costo_oft, $filas_segmento_cliente['factibilidad_renta'], $factor_48); ?></td>
												<td align ="center"> = </td>
												<td align ="center"><?php echo $precio_total_48; ?></td>
												<?php } ?>
												<td align ="center">UF/Mes + IVA</td>
												<?php if ($porcentaje_descuento!=0){ ?> 
												<td align ="center">/</td>
												<td align ="center"><?php echo $precio_total_48_dscto; ?></td>
												<td align ="center">UF/Mes + IVA</td>
												<?php } ?>
											</tr>
											
											<?php
											}
											?>
											
										</table>
									</tr>
								</table>
								<table>
									<tr>
										<td>- Código Identificador:</td>
										<td><?php echo $ultimo_id; ?></td>
									</tr>
									<tr>
										<td Colspan = "2">
											(Favor enviar este código a Soporte Comercial en caso de que Cliente acepte esta propuesta)
										</td>
									</tr>
									<tr><br></tr>
									<tr><br></tr>
									<tr>
										<td colspan="2">
											(*) Esta oferta es válida mientras OFT se encuentre vigente.
										</td>
									</tr>
									<tr>
										<td align="center"><br><input type="submit" name="recotizar" value="Realizar Otra Cotización" class="boton">
										</td>
									</tr>
								</table>
								</section >
							</td>
							<?php 
							} else{
								echo "<h3>Esta comuna se encuentra fuera de Cartilla. Favor levantar requerimiento a Mesa de Precios. <br><br>Gracias</h3> <br>";
								
								}
			}
			}
							else{
								echo "<h3>Para poder realizar la cotización, debe ingresar los campos solicitados:</h3> <br>";
								if ($_POST['rut_ejecutivo']==""){echo "-Falta Ingresar Rut Ejecutivo <br><br>";}
								if ($_POST['nombre_ejecutivo']==""){echo "-Falta Ingresar Nombre Ejecutivo <br><br>";}
								if ($_POST['segmento_cliente']=="Ingrese Segmento"){echo "-Debe escoger Segmento del Cliente <br><br>";}
								if ($_POST['rut_cliente']==""){echo "-Falta Ingresar Rut Cliente <br><br>";}
								if ($_POST['nombre_cliente']==""){echo "-Falta Ingresar Nombre Cliente <br><br>";}
								if ($_POST['numero_oft']==""){echo "-Falta Ingresar Número de OFT <br><br>";}
								if ($_POST['costo_oft']==""){echo "-Falta Ingresar Costo OFT <br><br>";} 
								if ($_POST['region']=="Ingrese Región donde se instalará el ID"){echo "-Debe escoger Región <br><br>";}
								if ($_POST['comuna']==""){echo "-Debe escoger Comuna <br><br>";}
								if ($_POST['bwn']=="Ingrese BW"){echo "-Debe escoger BW Nacional <br><br>";}
								if ($_POST['bwi']==""){echo "-Debe escoger BW Internacional <br><br>";}
								if ($_POST['medio_acceso']=="Ingrese Acceso"){echo "-Debe escoger Medio de Acceso <br><br>";}
								}
							}
							?> 
						</tr>
					</table>
				</form>
		</article>
	</section>
		<?php  
			require('../footer.php');
		?>
</body>
</html>