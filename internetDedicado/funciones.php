<?php

function cadenaAleatoria($length = 10) 
				{ 
	    		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length); 
	    		}

function sobrecosto($cargo_inicial,$factibilidad, $costo_segmento, $factor){

	if ($cargo_inicial==0){
		
		if ($factibilidad > 1000000) {
		
			if ($factibilidad <= $costo_segmento) {
				return round((($factibilidad-1000000)/100000)*$factor,2);
			} else {
				return round(((($costo_segmento-1000000))/100000)*$factor,2);
			}
			
			} else {
				return 0;
			}
	} else{
		if (($factibilidad-$cargo_inicial) > 1000000) {
		
			if (($factibilidad-$cargo_inicial) <= $costo_segmento) {
				return round(((($factibilidad-$cargo_inicial)-1000000)/100000)*$factor,2);
			} 
			
			} else {
				return 0;
			}
	}
}

function calculoCuotas($cargo_inicial,$factibilidad, $costo_segmento, $plazo, $margen,$wacc,$uf){
	if ($cargo_inicial==0) {

		if ($factibilidad > 1000000) {
			if ($factibilidad <= $costo_segmento) {
				return 0;
			} else {
				if ($plazo>1) {
					return round(($wacc*pow(1+$wacc, $plazo))*((($factibilidad-$costo_segmento)/$uf)/(1-$margen))/((pow(1+$wacc, $plazo))-1),2);
				}else{
					return round((($factibilidad-$costo_segmento)/$uf)/(1-$margen),2);
				}
				
			}
			
		} else {
			return 0;
		}

	}else{
		return round(($wacc*pow(1+$wacc, $plazo))*((($cargo_inicial)/$uf)/(1-$margen))/((pow(1+$wacc, $plazo))-1),2);
	}
}

function prueba(){

	 ?><script type="text/javascript">alert("Favor ingrese segmento cliente");</script> <?php

}


?>