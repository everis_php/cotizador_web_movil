<?php
    session_start();
    
      setcookie("sesion_usuario", "", time()-1);
      setcookie("tokens", "", time()-1);
      setcookie("sesion_actual", "", time()-1);
      setcookie("rut", "", time()-1);
      setcookie("nombre_usuario", "", time()-1);
      setcookie("apellido_usuario", "", time()-1);
      setcookie("correo", "", time()-1);
      setcookie("vista", "", time()-1);

      unset($_COOKIE["rut"]);
      unset($_COOKIE["tokens"]);
      unset($_COOKIE["nombre_usuario"]);
      unset($_COOKIE["apellido_usuario"]);
      unset($_COOKIE["correo"]);
      unset($_COOKIE["vista"]);
      unset($_COOKIE["sesion_actual"]);
      unset($_COOKIE["sesion_usuario"]);
      unset($_SESSION["sesion_token"]);

      session_unset();
      session_destroy();

    header("Location: index.php");
    exit;
?>