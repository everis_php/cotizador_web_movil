<?php
	session_start();

require_once('../header.php');
//require_once('seguridad.php');
require_once '../conexion/conexion_bd.php';

$infGral = [
    "Nombre" => "",
    "Rut" => "",
    "F_Prop" => "",
    "V_Prop" => "",
    "Folio" => ""
];
$consInfGral="select * from cow_neg_cotizacion where codi_coti=".$_GET["folio"].";";
$resultadoInfGral = $mysqli->query($consInfGral);
$filaInfGral=$resultadoInfGral->fetch_array();
//echo '<pre>';
//var_dump ($fila);
?>
  <body>
    <div class="row">
        <div class="col-md-4">
            <table border="4">
                <tr>
                    <td colspan="2" align="center" style="background-color: orange;">
                        INFORMACIÓN GENERAL
                    </td>
                </tr>
                <tr>
                    <td NOWRAP id="cab_sec">
                        Nombre Cliente
                    </td>

                    <td>
                        <?php 
                            echo $filaInfGral['NOMB_RAZONSOCIAL']; 
                        ?>
                    </td>
                </tr>
                <tr>
                    <td id="cab_sec">
                        RUT
                    </td>
                    <td>
                        <?php 
                            echo $filaInfGral['NRUT_CLIENTE'];

                        ?>
                    </td>
                </tr>
                <tr>
                    <td NOWRAP id="cab_sec">
                        Fecha Propuesta
                    </td>
                    <td>
                        <?php 
                            date_default_timezone_set("America/Santiago");
                            $fecha= date_create($filaInfGral['FECH_COTIZACION']);
                            echo date_format($fecha, 'd-m-Y');
                        ?>
                    </td>
                </tr>
                <tr>
                    <td NOWRAP id="cab_sec">
                        Vigencia Propuesta	
                    </td>
                    <td>
                        <?php 
                            //Fecha de Vigencia Propuesta se entiende que esta es superior en 30 días respecto a Fecha Propuesta
                            $nuevafecha = strtotime ( '+30 day' , strtotime ( $filaInfGral['FECH_COTIZACION'] ) ) ;
                            $nuevafecha = date ( 'd-m-Y' , $nuevafecha );
                            $infGral["V_Prop"] = $nuevafecha;
                            echo $nuevafecha;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td id="cab_sec">
                        Folio
                    </td>
                    <td>
		         <?php echo $filaInfGral['CODI_COTI'];?>       
                    </td>
		      	</tr> 
            </table>
        </div>
  		<div class="col-md-4"></div>
  		<div class="col-md-4">
                <br>
                <div class="col-md-8"></div>
                    <div class="col-md-4">
                        <img id=img width="150" height="100" src="<?php echo $URL_logo_Entel;?>">
                    </div>
  		</div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table border="4" width="100%">
		      	<!-- Cabecera de la tabla Detalle planes empresa conectada -->
                <thead>
                    <tr>
                        <td colspan="9" NOWRAP align="center" style="background-color: orange;">
                            <div id="cab_prim_full">
                                DETALLE PLANES EMPRESA CONECTADA
                            </div>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="4"></td>
                        <td colspan="3">Descuentos</td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td>Nombre/Cuota de trafico</td>
                        <td>Tipo de línea</td>
                        <td>Cantidad</td>
                        <td>Cargo Fijo S/IVA</td>
                        <td>Fidelizacion</td>
                        <td>Extra</td>
                        <td>Portabilidad*</td>
                        <td>Total</td>
                        <td>CF Final S/IVA</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $consDetPlanEmpCon="SELECT NOMB_PLAN,DESC_TIPOPLAN,CANT_EQUIPOS,VLOR_EQUIPOSINIVA,PORC_DSCTO_FIDELIZACION,PORC_DSCTO_EXTRA,PORC_DSCTO_PORTABILIDAD,SUMATORIA_DESCUNENTOS,VALOR_TOTAL_A_PAGAR
                        FROM cow_neg_detcotizacion 
                        WHERE cow_neg_cotizacion_CODI_COTI_1=".$_GET["folio"];
                        $resultadoDetPlanEmpConl = $mysqli->query($consDetPlanEmpCon);
                        $row_count_DetPlanEmpCon= $resultadoDetPlanEmpConl->num_rows;
                        $filaDetPlanEmpCon=$resultadoDetPlanEmpConl->fetch_array();
                            foreach($resultadoDetPlanEmpConl as $celdaDetPlanEmpConl){
                                echo '
                                    <tr>
                                        <td>'?>
                                            <?php echo $celdaDetPlanEmpConl['NOMB_PLAN']; 
                                            echo'
                                        </td>
                                        <td>'?>
                                            <?php echo $celdaDetPlanEmpConl['DESC_TIPOPLAN'];
                                            echo'
                                        </td>
                                        <td>'?>
                                            <?php echo $celdaDetPlanEmpConl['CANT_EQUIPOS'];
                                            echo'
                                        </td>
                                        <td>$ '?>
                                            <?php echo round($celdaDetPlanEmpConl['VLOR_EQUIPOSINIVA']);
                                            echo'
                                        </td>
                                        <td>'?>
                                            <?php echo round($celdaDetPlanEmpConl['PORC_DSCTO_FIDELIZACION']);
                                            echo'%
                                        </td>
                                        <td>'?>
                                            <?php echo round($celdaDetPlanEmpConl['PORC_DSCTO_EXTRA']);
                                            echo'%
                                        </td>
                                        <td>'?>
                                            <?php echo round($celdaDetPlanEmpConl['PORC_DSCTO_PORTABILIDAD']);
                                            echo'%
                                        </td>
                                        <td>'?>
                                            <?php echo round($celdaDetPlanEmpConl['SUMATORIA_DESCUNENTOS']);
                                            echo'%</td>
                                        <td>$'?>
                                            <?php echo round($celdaDetPlanEmpConl['VALOR_TOTAL_A_PAGAR']);
                                            echo' </td>
                                    </tr>';
                            }
                    ?>
                </tbody>
                <!--Datos Totales de las tablas -->
                <?php
                    $consDetPlanEmpConFinal="SELECT CANT_TOTALLINEAS,VLOR_FACTURACIONCOTI,VLOR_TOTALDESCFID,VLOR_TOTALDESCEXTRA,VLOR_TOTALDESCRET,VLOR_CF18NETO,VLOR_CF19NETO "
                                            ." FROM cow_neg_cotizacion"
                                            ." WHERE CODI_COTI=".$_GET["folio"];
                    $resultadoDetPlanEmpConlFinal = $mysqli->query($consDetPlanEmpConFinal);
                    $filaDetPlanEmpConTotal=$resultadoDetPlanEmpConlFinal->fetch_array();
                ?>
                            <tr style="font-weight: bold">
                                <td></td>
                                <td>TOTAL LINEAS</td>
                                <td><?php echo round($filaDetPlanEmpConTotal['CANT_TOTALLINEAS']);?></td>
                                <td>$ <?php echo round($filaDetPlanEmpConTotal['VLOR_FACTURACIONCOTI']);?></td>
                                <td>-$ <?php echo round($filaDetPlanEmpConTotal['VLOR_TOTALDESCFID']);?></td>
                                <td>-$ <?php echo round($filaDetPlanEmpConTotal['VLOR_TOTALDESCEXTRA']);?></td>
                                <td>-$ <?php echo round($filaDetPlanEmpConTotal['VLOR_TOTALDESCRET']);?></td>
                                <td></td>
                                <td></td>
                            </tr>
                        
                        <?php 
                        ?>
		      	<!-- Informacion cargo fijo neto 18 y desde 19 meses -->
		      	<tr >
                            <td colspan="5"></td>
                            <td colspan="3">Cargo fijo neto por 18 meses</td>
                            <td style="font-weight: bold"> $ <?php echo round($filaDetPlanEmpConTotal['VLOR_CF18NETO']);?></td>
		      	</tr>
		      	<tr>
                            <td colspan="5"></td>
                            <td colspan="3">Cargo fijo neto desde mes 19</td>
                            <td style="font-weight: bold"> $ <?php echo round($filaDetPlanEmpConTotal['VLOR_CF19NETO']);?></td>
		      	</tr>      	
		      	<tr>
                            <td colspan="9">*Descuento válido por 18 meses, aplica sólo para portabilidades con cargo fijo desde $16.990 iva incluido.</td>
		      	</tr>
		      	<tr>
                            <td colspan="9">*Descuento portabilidad se aplica automáticamente.</td>
		      	</tr>
		    </table>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<table border="4" width="100%">
  			<!-- Tabla DESCUENTO EN EQUIPOS (Valores Sin iva) -->
		      	<tr align="center">
		      		<td colspan="9" style="background-color: orange;">
		        		<div id="cab_prim_full">
		      				Tabla DESCUENTO EN EQUIPOS (Valores Sin iva)
		        		</div>      	
		        	</td>      	
		      	</tr>
		      	<tr>
		      		<!-- Cabecera tabla-->
		      		<td>Plan</td>
		      		<td>Modelo equipo</td>
		      		<td>Q EQUIPOS</td>
		      		<td>Valor Cuota Inicial</td>
		      		<td>Valor en Plan</td>
		      		<td>% Descuento</td>
		      		<td>Total Dscto Aprobado</td>
		      		<td>Total a Pagar</td>
		      		<td>Código Descuento</td>
		      	</tr>
		      	<!-- Detalle tabla -->
                        </table>
                </div>
        </div>
