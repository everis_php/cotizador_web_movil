<?php 

	session_start(); 
	
	require_once("seguridad.php");
	require_once("../header.php");
        $token=$_GET['token'];
        $DET_EST_COT = [
        "1"=>"INGRESADA",
        "2"=>"RECHAZADA",
        "3"=>"PENDIENTE",
        "4"=>"APROBADA",
        ];
?>		<style type="text/css">
			footer {
				/*position: absolute;*/
				bottom: 0;
			}
		</style>
	<body>
		<form method="POST" action="validaBusqueda.php?token=<?php echo $_COOKIE['tokens']?>" class="form-horizontal" onsubmit="return validaFormulario();">
			<div class="form-group">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<h3 class="text-center">Busqueda de solicitudes</h3>
				</div>
				<div class="col-md-4">
					<img class="pull-right img-rounded" src="<?php echo $URL_logo_Entel;?>">
				</div>	
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Folio: <input type="radio" name="tipoBusqueda"  value="folio"></label>
					<div class="col-md-9">
						<input type="number" onkeypress="return validaFolio(event);" id="folio" name="folio" class="form-control" min="0">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Rut: <input type="radio" name="tipoBusqueda"  value="rut"></label>
					<div class="col-md-9">
						<input type="text" id="rut" name="rut" class="form-control" onchange="validaRut(this)" onblur="formatoRut()">
					</div>	
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Usuario: <input type="radio" name="tipoBusqueda" value="usuario"></label>
					<div class="col-md-9">
						<input type="text" name="usuario" class="form-control" maxlength="20">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Estado Cotiz.: <input type="radio" name="tipoBusqueda" value="estadoCotizacion"></label>
					<div class="col-md-9">
						<select name="estadoCotizacion" class="form-control">
                                                <?php
                                                for($x=1;$x<=count($DET_EST_COT);$x++){
                                                    echo '<option value='.$x.'>'.$DET_EST_COT[$x].'</option>';    
                                                }
                                                ?>
						</select>
					</div>
				</div>
				<div class="form-group">
                                    <label class="control-label col-md-3">Fecha: <input type="radio" name="tipoBusqueda" value="rangFecha"></label>
					<div class="col-md-4">
                                            <label class="control-label col-md-4">Inicio</label>
						<input type="date" id="fechaInicio" name="fechaInicio" class="form-control" onchange="return evaluaFechas();">
					</div>
					<div class="col-md-4">
                                            <label class="control-label col-md-4">Fin</label>
						<input type="date"  id="fechaFin" name="fechaFin" class="form-control" onchange="return evaluaFechas();">
                                        </div>
				</div>
				<div class="form-group">
					<div class="col-md-3"></div>
					<div class="col-md-3">
						<input type="submit" name="submitButton" value="Buscar" class="btn btn-info text-center pull-left">
					</div>
					<div class="col-md-4">
                                            <a pull-right href="../cotizadorwebempresas/index.php?token=<?php echo $token; ?>" class="btn btn-warning text-left">Regresar</a>
                                        </div>
				</div>
			</div>
			<div class="col-md-3"></div>
		</form>
	</body>
<?php
	require_once("../footer.php");
?>