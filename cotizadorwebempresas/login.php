<?php 
	session_start(); 

	/**
	* @author Rodrigo Mason Cotizador manual Entel 30-11-17
	*/

	if (isset($_COOKIE["sesion_usuario"]) && $_COOKIE["sesion_usuario"]  == "activa") {
        header("Location: index.php?token=".$_COOKIE["tokens"]);
        exit;
    }else{

	require_once('../conexion/conexion_bd_login.php');
	require_once("../header.php"); 	

	if ($_GET["error"]) {
		?>
			<script type="text/javascript">alert('Rut o contraseña invalida!');</script> 
		<?php
	}

	if ($_GET["danger"]) {
		?>
			<script type="text/javascript">alert('Acceso no permitido!');</script> 
		<?php
	}

?> 
<body>
	<header>	
		<div>
			<img src="<?php echo $URL_logo_Entel;?>" alt="Logo" width="85"/>		
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Cotizador Web VP Empresas</span> 
		<span class="HeaderDerecha"> V1.0</span>
	</header>
	<section class="main">	
			<form action="../validator.php" method="POST" name="form" class="form-horizontal login_box">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4 tabla">
						<h3 align="center">Favor identifiquese:</h3>
						<div class="form-group">
							<label class="control-label col-sm-2" for="email">Rut:</label>
						    <div class="col-sm-10">
								<input class="form-control" placeholder="(Ej: 99999999-9)" type="text" name="rut" required>
							</div>
						</div>
						<div class="form-group">
						    <label class="control-label col-sm-2" for="email">Password:</label>
						    <div class="col-sm-10">
						     	<input class="form-control" type="Password" name="password" required placeholder="Ingrese Password">
						    </div>
	  					</div>
	  					<div class="form-group">
	  						<div class="col-md-4"></div>
	  						<div class="col-md-4">
	  							<input type="submit"  name="calcular" value="Ingresar" class="btn btn-info form-control">
	  						</div>
	  						<div class="col-md-4"></div>
	  					</div>
	  				</div>	
					<div class="col-md-4"></div>
				</div>
			</form>
	</section>
	<?php
		require_once('../footer.php');
	}
	
?>