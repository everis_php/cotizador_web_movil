<?php 
	session_start();
	
	require_once("seguridad.php");
	require_once("../header.php");
	
	?>
<body>
    <header>		
	<div>
            <img src="../Imagenes/Isotipo_Fondo_Azul.jpg" alt="Logo" width="85"/>			
	</div> <!-- / #logo-header -->
	<span class="HeaderTitulo">Cotizador Web VP Empresas 1.0</span> 
        <span class="HeaderDerecha"> V1.0 <br>
            <a href="../cerrarSesion.php">Cerrar Sesion</a>
        </span>
    </header>
    <table>
        <tr>
        <td height="55" align='center' width='350'>
            <h3>Bienvenido <?php echo $_COOKIE["nombre_usuario"]." ".$_COOKIE["apellido_usuario"]; ?></h3>
        </td>
        </tr>
    </table>
    <div class="container">
        <div class="row">
        <?php if($_COOKIE['vista']=="soporte" or $_COOKIE['vista']=="admin"){ ?>
            <div class="col-md-3">
                <div class="thumbnail">
                    <a href="<?php echo $URL_mis_casos; ?>">
                    <img src="<?php echo $URL_logo_mis_casos; ?>" alt="mis casos" style="width:100%">
                        <div class="caption" align="center">
                            <p>Mis Casos</p>
                        </div>
                    </a>
                </div>
            </div>
        <?php }?>
            <div class="col-md-3">
                <div class="thumbnail">
                    <a href="<?php echo $URL_busqueda; ?>">
                    <img src="<?php echo $URL_logo_busqueda; ?>" alt="mis casos" style="width:100%">
                        <div class="caption" align="center">
                            <p>Busqueda</p>
                        </div>
                    </a>
                </div>
            </div>
        <?php if($_COOKIE['vista']=="soporte" or $_COOKIE['vista']=="admin"){ ?>
                <div class="col-md-3">
                    <div class="thumbnail">
                        <a href="<?php echo $URL_internet_dedicado; ?>">
                        <img src="<?php echo $URL_logo_ID; ?>" alt="calculadora internet" style="width:100%">
                            <div class="caption" align="center">
                                <p>Calculadora de Internet Dedicado e Internet Trunk</p>
                            </div>
                        </a>
                    </div>
                </div>
        <?php }?>
            <?php if($_COOKIE['vista']=="soporte" or $_COOKIE['vista']=="admin"){ ?>
                <div class="col-md-3">
                    <div class="thumbnail">
                        <a href="<?php echo $URL_casos_portabilidad; ?>">
                        <img src="<?php echo $URL_logo_casos_portabilidad; ?>" alt="Casos Portabilidad" style="width:100%">
                            <div class="caption" align="center">
                                <p>Casos Portabilidad</p>
                            </div>
                        </a>
                    </div>
                </div>
        <?php }?>
        </div>
        <div class="row">
            <?php if($_COOKIE['vista']=="canal" or $_COOKIE['vista']=="admin"){ ?>
                    <div class="col-md-3">
                        <div class="thumbnail">
                            <a href="<?php echo $URL_portabilidad; ?>">
                            <img src="<?php echo $URL_logo_portabilidad; ?> " alt="Fjords" style="width:100%">
                                <div class="caption" align="center">
                                    <p>Cotizador Portabilidad</p>
                                </div>
                            </a>
                        </div>
                    </div>
            <?php }?>
            <?php if($_COOKIE['vista']=="mesa" or $_COOKIE['vista']=="admin"){ ?>
                    <div class="col-md-3">
                        <div class="thumbnail">
                            <a href="<?php echo $URL_mesa_precios; ?>">
                            <img src="<?php echo $URL_logo_mesa_precio; ?> " alt="Fjords" style="width:100%">
                                <div class="caption" align="center">
                                    <p>Mesa de Precios</p>
                                </div>
                            </a>
                        </div>
                    </div>
            <?php }?>
            <?php if($_COOKIE['vista']=="soporte" or $_COOKIE['vista']=="admin"){ ?>
                        <div class="col-md-3">
                            <div class="thumbnail">
                                <a href="<?php echo $URL_firewall; ?>">
                                <img src="<?php echo $URL_logo_firewall; ?>" alt="Fjords" style="width:100%">
                                    <div class="caption" align="center">
                                        <p>Cotizador de Firewall</p>
                                    </div>
                                </a>
                            </div>
                        </div>
            <?php }?>
        </div>
    </div>
 	<?php  require('../footer.php'); ?>