<?php
require_once('../conexion/conexion_bd.php');
function cadenaAleatoria($length = 10) 
				{ 
	    		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length); 
	    		}

function sobrecosto($cargo_inicial,$factibilidad, $costo_segmento, $factor){

	if ($cargo_inicial==0){
		
		if ($factibilidad > 1000000) {
		
			if ($factibilidad <= $costo_segmento) {
				return round((($factibilidad-1000000)/100000)*$factor,2);
			} else {
				return round(((($costo_segmento-1000000))/100000)*$factor,2);
			}
			
			} else {
				return 0;
			}
	} else{
		if (($factibilidad-$cargo_inicial) > 1000000) {
		
			if (($factibilidad-$cargo_inicial) <= $costo_segmento) {
				return round(((($factibilidad-$cargo_inicial)-1000000)/100000)*$factor,2);
			} 
			
			} else {
				return 0;
			}
	}
}

function calculoCuotas($cargo_inicial,$factibilidad, $costo_segmento, $plazo, $margen,$wacc,$uf){
	if ($cargo_inicial==0) {

		if ($factibilidad > 1000000) {
			if ($factibilidad <= $costo_segmento) {
				return 0;
			} else {
				if ($plazo>1) {
					return round(($wacc*pow(1+$wacc, $plazo))*((($factibilidad-$costo_segmento)/$uf)/(1-$margen))/((pow(1+$wacc, $plazo))-1),2);
				}else{
					return round((($factibilidad-$costo_segmento)/$uf)/(1-$margen),2);
				}
				
			}
			
		} else {
			return 0;
		}

	}else{
		return round(($wacc*pow(1+$wacc, $plazo))*((($cargo_inicial)/$uf)/(1-$margen))/((pow(1+$wacc, $plazo))-1),2);
	}
}

function prueba(){

	 ?><script type="text/javascript">alert("Favor ingrese segmento cliente");</script> <?php

}

function llenarCanales() {   

		$query = "SELECT * FROM cow_mae_canal;";
		$consulta = $mysqli->query($query);
		$combo = "";
		if ( $consulta->num_rows > 0 ) {
			$combo = '<select name="canales">';
			$combo .= '<option value="0">Selecciona...</option>' . "\n";
			while ( $resultados = $consulta->fetch_array()) {
				$combo .= '<option value="' . $resultados['CODI_CANAL'] . '">' . $resultados['NOMB_CANAL'] . "</option>\n";
			}
			$combo .= "</select>\n";
		}
		$mysqli->close();
		return $combo;
	}

function llenarTipoSolicitud() {   

		$query = "SELECT * FROM cow_mae_tipo_solicitud;";
		$consulta = $mysqli->query($query);
		$combo = "";
		if ( $consulta->num_rows > 0 ) {
			$combo= '<select name="tipo_solicitud">';
			$combo .= '<option value="">Selecciona...</option>' . "\n";
			while ( $resultados = $consulta->fetch_array()) {
				$combo .= '<option value="' . $resultados['CODI_TIPSOLICITUD'] . '">' . $resultados['NOMB_TIPSOLICITUD'] . "</option>\n";
			}
			$combo .= "</select>\n";
		}

		$mysqli->close();
		return $combo;
}

function llenarPlan($numero) {   

		$query = "SELECT NOMB_PLAN,VLOR_CARGOFIJO FROM cow_mae_plan;";
		$consulta = $mysqli->query($query);
		$combo = "";
		if ( $consulta->num_rows > 0 ) {
			$combo= '<select name="planes" onchange="cargarCargoFijo(value,'. $numero .');total_linea('. $numero .')">';
			$combo .= '<option value="0">Selecciona...</option>' . "\n";
			while ( $resultados = $consulta->fetch_array()) {
				$combo .= '<option value="' . $resultados['NOMB_PLAN'] . '">' . $resultados['VLOR_CARGOFIJO'] . "</option>\n";
			}
			$combo .= "</select>\n";
		}
		$mysqli->close();
		return $combo;
}
function llenarTipoLinea() {   

		$query = "SELECT * FROM cow_mae_tlinea;";
		$consulta = $mysqli->query($query);
		$combo = "";
		if ( $consulta->num_rows > 0 ) {
			$combo= '<select name="tipo_linea">';
			$combo .= '<option value="">Selecciona...</option>' . "\n";
			while ( $resultados = $consulta->fetch_array()) {
				$combo .= '<option value="' . $resultados['NOMB_TLINEA'] . '">' . $resultados['DESC_TLINEA'] . "</option>\n";
			}
			$combo .= "</select>\n";
		}
		$mysqli->close();
		return $combo;
}

function llenarEquipos() {   

		$query = "SELECT * FROM cow_mae_equipo;";
		$consulta = $mysqli->query($query);
		$combo = "";
		if ( $consulta->num_rows > 0 ) {
			$combo= '<select name="equipos">';
			$combo .= '<option value="">Selecciona...</option>' . "\n";
			while ( $resultados = $consulta->fetch_array()) {
				$combo .= '<option value="' . $resultados['NOMB_EQUIPOABREV'] . '">' . $resultados['NOMB_EQUIPOFULL'] . "</option>\n";
			}
			$combo .= "</select>\n";
		}
		$mysqli->close();
		return $combo;
}

function regresar(){
    header("location:index.php");
    exit;
}
?>