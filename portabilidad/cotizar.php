<?php

error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
session_start();

require_once('../header.php');
require_once ('funciones.php');

//require_once('seguridad.php');
$total_Registros = $_POST['total_filas_tabla'] - 1;
$infGral         = [
    "Nombre" => "",
    "Rut"    => "",
    "F_Prop" => "",
    "V_Prop" => "",
    "Folio"  => ""
]; 
$fecha_actual 			  = date('Y-m-d');
$fecha_vigencia_propuesta = strtotime ( '+30 day' , strtotime ( $fecha_actual ) ) ;
$fecha_vigencia_propuesta = date ( 'Y-m-d' , $fecha_vigencia_propuesta );
$folio                    = date('YmdHis');
$VLOR_TOTALCFSINIVA       = 0;


// Instacia variables para realizar insert de la cotización
$campos_insert_cotizacion[0]  = "NOMB_RAZONSOCIAL";
$campos_insert_cotizacion[1]  = "NRUT_CLIENTE";
$campos_insert_cotizacion[2]  = "NOMB_TIPOSOLICITUD";
$campos_insert_cotizacion[3]  = "NOMB_CANAL";
$campos_insert_cotizacion[4]  = "FECH_COTIZACION";
$campos_insert_cotizacion[5]  = "ESTD_COTIZACION";
$campos_insert_cotizacion[6]  = "CANT_TOTALLINEAS";
$campos_insert_cotizacion[7]  = "NOMB_USUARIOSOPORTE";
$campos_insert_cotizacion[8]  = "VLOR_TOTALCFSINIVA";
$campos_insert_cotizacion[9]  = "VLOR_FACTURACIONCOTI";
$campos_insert_cotizacion[10] = "VLOR_CF18NETO";
$campos_insert_cotizacion[11] = "VLOR_CF19NETO";
$campos_insert_cotizacion[12] = "VLOR_PORCDESCFID";
$campos_insert_cotizacion[13] = "VLOR_TOTALDESCFID";
$campos_insert_cotizacion[14] = "VLOR_TOTALDESCEXTRA";
$campos_insert_cotizacion[15] = "CANT_EQUIPOS";
$campos_insert_cotizacion[16] = "FOLIO";

$array_insert_cotizacion  = [
	"NOMB_RAZONSOCIAL"     => $_POST['nombre_cliente'],
	"NRUT_CLIENTE"         => $_POST['rut_cliente'],
	"NOMB_TIPOSOLICITUD"   => $_POST['tipo_solicitud'],
	"NOMB_CANAL"           => $_POST['canales'],    
	"FECH_COTIZACION"      => $fecha_actual,
	"ESTD_COTIZACION"      => "1",
	"CANT_TOTALLINEAS"     => $total_Registros,
	"NOMB_USUARIOSOPORTE"  => $_SESSION['nombre_usuario'],
	"VLOR_TOTALCFSINIVA"   => "",  //se llena mas abajo...
	"VLOR_FACTURACIONCOTI" => $_POST['facturacion_total'],
	"VLOR_CF18NETO"        => "", //se llena mas abajo...
	"VLOR_CF19NETO"        => "", //se llena mas abajo...
	"VLOR_PORCDESCFID"     => "", //se llena mas abajo...
	"VLOR_TOTALDESCFID"    => "", //se llena mas abajo... 
	"VLOR_TOTALDESCEXTRA"  => "", //se llena mas abajo... 
	"CANT_EQUIPOS"         => "",  //se llena mas abajo...
	"FOLIO"                => $folio, 
];

?>
  <body>
  	<div class="row">
  		<div class="col-md-4">
  			<table border="4">
		    	<tr>
		      		<td colspan="2" align="center" style="background-color: orange;">
		            	INFORMACIÓN GENERAL
					</td>
		      	</tr>
		      	<tr>
		        	<td NOWRAP id="cab_sec">
		            	Nombre Cliente
		          	</td>
		          	<td>
		               <?php 

		               $infGral["Nombre"] 	= $_POST['nombre_cliente'];
		               echo "".$_POST['nombre_cliente'];
		               
		               ?>
		          	</td>
		      	</tr>
		      	<tr>
		          	<td id="cab_sec">
		                RUT
		          	</td>
		          	<td>
		                <?php 

		                $infGral["Rut"] 	 = $_POST['rut_cliente'];
		                echo "".$_POST['rut_cliente'];

		                ?>
		          	</td>
		      	</tr>
		      	<tr>
		          	<td NOWRAP id="cab_sec">
		              	Fecha Propuesta
		          	</td>
		          	<td>
		                <?php 
		                date_default_timezone_set("America/Santiago");
		                echo $fecha_actual;
		                ?>
		          	</td>
		      	</tr>
		      	<tr>
		          	<td NOWRAP id="cab_sec">
						Vigencia Propuesta	
		          	</td>
		          	<td>
						<?= $fecha_vigencia_propuesta; ?>
		          	</td>
		      	</tr>
		      	<tr>
		          	<td id="cab_sec">
		                Folio
		          	</td>
		          	<td>
		                <?php 
		                echo $folio; 
		                ?>
		          	</td>
		      	</tr> 
		    </table>
  		</div>
  		<div class="col-md-4"></div>
  		<div class="col-md-4">
  			<br>
  			<div class="col-md-8"></div>
  			<div class="col-md-4">
  				<img id=img width="150" height="100" src="<?php echo $URL_logo_Entel;?>">
  			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<table border="4" width="100%">
		      	<!-- Cabecera de la tabla Detalle planes empresa conectada -->
		      	<thead>
		      		<tr>
			      		<td colspan="9" NOWRAP align="center" style="background-color: orange;">
			        		<div id="cab_prim_full">
			        			DETALLE PLANES EMPRESA CONECTADA
			      			</div>
			        	</td>
			      	</tr>
			      	<tr align="center">
			      		<td colspan="4"></td>
			      		<td colspan="3">Descuentos</td>
			      		<td colspan="2"></td>
			      	</tr>
  					<tr>
				      	<td>Nombre/Cuota de trafico</td>
				      	<td>Tipo de línea</td>
				      	<td>Cantidad</td>
				      	<td>Cargo Fijo S/IVA</td>
				      	<td>Fidelizacion</td>
				      	<td>Extra</td>
				      	<td>Portabilidad*</td>
				      	<td>Total</td>
				      	<td>CF Final S/IVA</td>
		      		</tr>
  				</thead>
  				<tbody>
  					<?php 

  						$por_fidelizacion        			         = (int)$_POST['porcentaje_fidelizacion']; 
  						$array_insert_cotizacion["VLOR_PORCDESCFID"] = $por_fidelizacion;

  						$desc_portabilidad         = get_descuento_portabilidad();
  						$total_cargo_fijo_siniva   = 0;
  						$canti_linea_total         = 0;
  						$total_final_extra         = 0; 
  						$total_final_portabili     = 0;
  						$cfx18                     = 0;
  						$cfx19                     = 0;
  						$acumulado_total_descuento = 0;
  						$total_lin                 = 0;

  						for ($i = 1; $i < ($total_Registros + 1); $i++) {

  							
  							// Parametros en duda, deben quitarse....
  							$extra             = 2;
  							$portabilidad      = 15;
  							$total_desc        = 29;
  							// Parametros en duda, deben quitarse....


  							$id_plan                   = $_POST['planes'.$i];
  							$plan                      = get_nombre_plan($id_plan);
  							$desc_extra                = get_descuento_extra_plan($id_plan);

							$tipoLinea                 = $_POST['tipo_lineas'.$i];
							$cant_linea                = $_POST['cant_linea'.$i];

							$total_lin 		           = $total_lin + $cant_linea;

							$cargo_fijo_coniva         = get_cargo_con_iva($id_plan);
							$cargo_fijo_siniva         = get_cargo_sin_iva($id_plan); 
							$VLOR_TOTALCFSINIVA        = $VLOR_TOTALCFSINIVA + $cargo_fijo_siniva;

							$total_descuento           = $por_fidelizacion + $desc_extra + $desc_portabilidad;
							$acumulado_total_descuento = $acumulado_total_descuento + $total_descuento;
							$total_final_fila          = $cargo_fijo_siniva * $cant_linea * (1 - ($total_descuento / 100));

							$total_cargo_fijo_siniva   = $total_cargo_fijo_siniva + ($cargo_fijo_siniva * $cant_linea); //(columna)


							$total_final_extra         = $total_final_extra + ($cargo_fijo_siniva * $cant_linea * ($desc_extra / 100));
							$total_final_portabili     = $total_final_portabili + ($cargo_fijo_siniva * $cant_linea * ($desc_portabilidad / 100));

							$cfx18       			   = $cfx18 + $total_final_fila;


							echo '
								<tr>
				      				<td>'.$plan.'</td>
							      	<td>'.$tipoLinea.'</td>
							      	<td>'.$cant_linea.'</td>
							      	<td> $ '.number_format($cargo_fijo_siniva, 0, '', '.').'</td> 
							      	<td>'.$por_fidelizacion.'%</td>
							      	<td>'.$desc_extra.' %</td>
							      	<td>'.$desc_portabilidad.'%</td>
							      	<td>'.$total_descuento.'%</td>
							      	<td>'.number_format(round($total_final_fila), 0, '', '.').'</td>
			      				</tr>
							';
							/*echo '
								<tr>
							      	<td>Empresa Conectada SIM 5GB</td>
							      	<td>Portabilidad</td>
							      	<td>5</td>
							      	<td> $ 11.756</td>
							      	<td>14%</td>
							      	<td>2%</td>
							      	<td>0%</td>
							      	<td>16%</td>
							      	<td>$ 49.376</td>
						      	</tr>
							';*/

  						} // cierre for

  						$array_insert_cotizacion["VLOR_TOTALCFSINIVA"]  = $VLOR_TOTALCFSINIVA;

  						$total_fidelizacion  							= ($total_cargo_fijo_siniva * $por_fidelizacion) / 100;
  						$cfx19      		 						    = $total_cargo_fijo_siniva + $total_fidelizacion + $total_final_extra;

						$array_insert_cotizacion["VLOR_CF18NETO"]       = $cfx18;
						$array_insert_cotizacion["VLOR_CF19NETO"]       = $cfx19;

						$array_insert_cotizacion["VLOR_TOTALDESCFID"]   =  $total_fidelizacion;
						$array_insert_cotizacion["VLOR_TOTALDESCEXTRA"] =  $total_final_extra;
						$array_insert_cotizacion["CANT_EQUIPOS"] 		=  $total_lin;
  					?>
  				</tbody>

		      	<!--Datos de las tablas -->
		      	
		      	<!-- Totale Lineas-->
		      	<tr>
			      	<td></td>
			      	<td>TOTAL LINEAS</td>
			      	<td>   <?= $total_lin;?> </td>
			      	<td>$  <?php echo number_format(round($total_cargo_fijo_siniva), 0, '', '.'); ?> </td>  
			      	<td>-$ <?php echo number_format($total_fidelizacion, 0, '', '.') ?> </td>
			      	<td>-$ <?php echo number_format($total_final_extra, 0, '', '.') ?> </td>
			      	<td>-$ <?php echo number_format($total_final_portabili, 0, '', '.') ?> </td>
			      	<td></td>
			      	<td></td>
		      	</tr>
		      	<!-- Informacion cargo fijo neto 18 y desde 19 meses -->
		      	<tr>
			      	<td colspan="5"></td>
			      	<td colspan="3">Cargo fijo neto por 18 meses</td>
			      	<td> $ <?php echo number_format($cfx18, 0, '', '.') ?></td>
		      	</tr>
		      	<tr>
			      	<td colspan="5"></td>
			      	<td colspan="3">Cargo fijo neto desde mes 19</td>
			      	<td> $ <?php echo number_format($cfx19, 0, '', '.') ?></td>
		      	</tr>      	
		      	<tr>
		      	<td colspan="9">* Descuento válido por 18 meses, aplica sólo para portabilidades con cargo fijo desde $16.990 iva incluido.</td>
		      	</tr>
		      	<tr>
		      	<td colspan="9">*Descuento portabilidad se aplica automáticamente.</td>
		      	</tr>
		    </table>
  		</div>
  	</div>




  	<?php 
//___________________________Aca se realiza insert a la tabla de Cotizaciones______________________________

	$campos = "(";

	for($i=0;$i<count($campos_insert_cotizacion);$i++){

		if($i == (count($campos_insert_cotizacion) - 1) ){
			$campos .= $campos_insert_cotizacion[$i];
		}
		else{
			$campos .= $campos_insert_cotizacion[$i].',';
		}
		
	}

	$campos .= ")";

	$values = "";

	$values = "(";
	$a      = 0;

	foreach ($array_insert_cotizacion as $key => $value){

		if($a == (count($array_insert_cotizacion) - 1) ){
			$values .= "'".$value."'";
		}
		else{
			$values .= "'".$value."'".',';
		}

		$a++;
	}

	$values .= ")";

	$tabla 			      = "cow_neg_cotizacion";
	$resp  			      = insertar_bd($campos,$tabla,$values);
	$id_insert_cotizacion = $resp[0];
  	?>





  	<div class="row">
  		<div class="col-md-12">
  			<table border="4" width="100%">
  			<!-- Tabla DESCUENTO EN EQUIPOS (Valores Sin iva) -->
		      	<tr align="center">
		      		<td colspan="9" style="background-color: orange;">
		        		<div id="cab_prim_full">
		      				Tabla DESCUENTO EN EQUIPOS (Valores Sin iva)
		        		</div>      	
		        	</td>      	
		      	</tr>
		      	<tr>
		      		<!-- Cabecera tabla-->
		      		<td>Plan</td>
		      		<td>Modelo equipo</td>
		      		<td>Q EQUIPOS</td>
		      		<td>Valor Cuota Inicial</td>
		      		<td>Valor en Plan</td>
		      		<td>% Descuento</td>
		      		<td>Total Descto Aprobado</td>
		      		<td>Total a Pagar</td>
		      		<td>Código Descuento</td>
		      	</tr>
		      	<!-- Detalle tabla -->
		      	<tbody>
  					<?php 

  						$total_valor_cuota_inicial = 0;
  						$total_valor_en_plan       = 0;
  						$total_cantidad_equipos    = 0;
  						$total_descuento_aprobado  = 0;
  						$total_final_a_pagar       = 0;

  						for ($i = 1; $i < ($total_Registros + 1); $i++) {

  							$cant_equipo  		       = (int)$_POST['cant_equipos'.$i];
  							$nombre_plan  		       = '';
  							$nombre_modelo_equipo      = '';
  							$valor_cuota_inicial;
  							$valor_en_plan;
  							$descuento                 = 0;
  							$alineado; //Tabla cow_mae_alineados
  							$codigo_desc               = '';
  							$tipoLinea                 = '';
  							$total_a_pagar             = 0;

							if($cant_equipo > 0){

								$id_equipo                 = $_POST['equipos'.$i];
  								$id_plan                   = $_POST['planes'.$i];
  								$tipoLinea                 = $_POST['tipo_lineas'.$i];

  								$nombre_plan               = get_nombre_plan($id_plan);
  								$nombre_modelo_equipo      = get_nombre_modelo_equipo($id_equipo);
  								$valor_cuota_inicial       = get_valor_cuota_inicial_equipoplan($id_plan,$id_equipo);
  								$valor_en_plan             = get_valor_en_plan_equipoplan($id_plan,$id_equipo);
  								$descuento                 = get_porcentaje_descuento_equipoplan($id_plan,$id_equipo);
  								$alineado                  = get_existencia_equipo_alineado($id_equipo);

  								$total_valor_cuota_inicial = $total_valor_cuota_inicial + ($valor_cuota_inicial * $cant_equipo);
  								$total_valor_en_plan       = $total_valor_en_plan + ($valor_en_plan * $cant_equipo);
  								$total_cantidad_equipos    = $total_cantidad_equipos + $cant_equipo;


  								$total_a_pagar              = $valor_en_plan * (1 - ($descuento/100)) * $cant_equipo;
  								$total_descuento            = ($valor_en_plan * $cant_equipo / 1.19) - $total_a_pagar * 1.19;

  								$total_descuento_aprobado   = $total_descuento_aprobado + $total_descuento;


  								if($alineado){
									$total_a_pagar = 0;
									$codigo_desc   = '296 Alineados';
  								}
  								else{
  									$codigo_desc   = '320 Mesa EV';
  								}

  								$total_final_a_pagar        = $total_final_a_pagar + $total_a_pagar;


								echo '
									<tr>
					      				<td>'.$nombre_plan.'</td>
								      	<td>'.$nombre_modelo_equipo.'</td>
								      	<td>'.$cant_equipo.'</td>
								      	<td>$ '.number_format(round($valor_cuota_inicial), 0, '', '.').'</td> 
								      	<td>$ '.number_format(round($valor_en_plan), 0, '', '.').'</td>
								      	<td>'.$descuento.'%</td>
								      	<td>$ '.number_format(round($total_descuento), 0, '', '.').'</td>  
								      	<td>$ '.number_format(round($total_a_pagar), 0, '', '.').'</td>  
								      	<td>'.$codigo_desc.'</td>  
				      				</tr>
								';  //Alineado consultar
							}

							// Instacia variables para realizar insert del detalle de la cotización
							$campos_insert_detalle_cotizacion[0]  = "NOMB_TIPOLINEA";
							$campos_insert_detalle_cotizacion[1]  = "CANT_LINEA";
							$campos_insert_detalle_cotizacion[2]  = "VLOR_PLANCONIVA";
							$campos_insert_detalle_cotizacion[3]  = "VLOR_DESCUENTOEXTRA";
							$campos_insert_detalle_cotizacion[4]  = "VLOR_TOTALDESCPLASINIVA";
							$campos_insert_detalle_cotizacion[5]  = "VLOR_PRECIOEQPLAN";
							$campos_insert_detalle_cotizacion[6]  = "VLOR_TOTALCARGOCONIVA";
							$campos_insert_detalle_cotizacion[7]  = "cow_mae_equipo_NOMB_EQUIPOFULL";
							$campos_insert_detalle_cotizacion[8]  = "cow_neg_cotizacion_CODI_COTI";
							$campos_insert_detalle_cotizacion[9]  = "CODI_EQUIPO";
							$campos_insert_detalle_cotizacion[10] = "DESC_TIPOPLAN"; 
							$campos_insert_detalle_cotizacion[11] = "CODI_PLAN"; 


							$array_insert_detalle_cotizacion  = [
								"NOMB_TIPOLINEA"  		  		 => "",
								"CANT_LINEA"      		  		 => "",
								"VLOR_PLANCONIVA" 		 		 => "",
								"VLOR_DESCUENTOEXTRA"     		 => "",
								"VLOR_TOTALDESCPLASINIVA" 		 => "",
								"VLOR_PRECIOEQPLAN"       		 => "",
								"VLOR_TOTALCARGOCONIVA"   		 => "",
								"cow_mae_equipo_NOMB_EQUIPOFULL" => "",
								"cow_neg_cotizacion_CODI_COTI"   => "",
								"CODI_EQUIPO"                    => "",
								"DESC_TIPOPLAN"                  => "",
								"CODI_PLAN"                      => ""
							];

							$array_insert_detalle_cotizacion["NOMB_TIPOLINEA"] 			       = $tipoLinea;
							$array_insert_detalle_cotizacion["CANT_LINEA"] 					   = $cant_equipo;
							$array_insert_detalle_cotizacion["VLOR_PLANCONIVA"] 			   = $valor_cuota_inicial;
							$array_insert_detalle_cotizacion["VLOR_DESCUENTOEXTRA"] 		   = $descuento;
							$array_insert_detalle_cotizacion["VLOR_TOTALDESCPLASINIVA"] 	   = $total_descuento;
							$array_insert_detalle_cotizacion["VLOR_PRECIOEQPLAN"] 	 		   = $valor_en_plan;
							$array_insert_detalle_cotizacion["VLOR_TOTALCARGOCONIVA"] 		   = $total_a_pagar;
							$array_insert_detalle_cotizacion["cow_mae_equipo_NOMB_EQUIPOFULL"] = $nombre_modelo_equipo;
							$array_insert_detalle_cotizacion["cow_neg_cotizacion_CODI_COTI"]   = $id_insert_cotizacion;
							$array_insert_detalle_cotizacion["CODI_EQUIPO"] 			       = $id_equipo;
							$array_insert_detalle_cotizacion["DESC_TIPOPLAN"] 		           = $nombre_plan;
							$array_insert_detalle_cotizacion["CODI_PLAN"] 		               = $id_plan;


							$campos_insert_deta_coti = "(";

							for($c=0;$c<count($campos_insert_detalle_cotizacion);$c++){

								if($c == (count($campos_insert_detalle_cotizacion) - 1) ){
									$campos_insert_deta_coti .= $campos_insert_detalle_cotizacion[$c];
								}
								else{
									$campos_insert_deta_coti .= $campos_insert_detalle_cotizacion[$c].',';
								}
								
							}

							$campos_insert_deta_coti .= ")";


							$values_insert_deta_coti = "";
							$values_insert_deta_coti = "(";
							$a      				 = 0;

							foreach ($array_insert_detalle_cotizacion as $key => $value){

								if($a == (count($array_insert_detalle_cotizacion) - 1) ){
									$values_insert_deta_coti .= "'".$value."'";
								}
								else{
									$values_insert_deta_coti .= "'".$value."'".',';
								}

								$a++;
							}

							$values_insert_deta_coti .= ")";

							$tabla_deta_coti      = "cow_neg_detcotizacion";
							$resp  			      = insertar_bd($campos_insert_deta_coti,$tabla_deta_coti,$values_insert_deta_coti);
							//echo "insert into $tabla_deta_coti $campos_insert_deta_coti values $values_insert_deta_coti <br><br>";
							$id_insert_deta_coti  = $resp[0];

							
  						} //Cierre for


  						//Actualizamos los valores finales.......................................
  						$campos_update  = " VLOR_TOTALPAGOEQUI = '$total_final_a_pagar', VALOR_TOTAL_CUOTA_INICIAL = '$total_valor_cuota_inicial', VALOR_TOTAL_EN_PLAN = '$total_valor_en_plan' , TOTAL_DESCUENTO_APROBADO = '$total_descuento_aprobado' , VALOR_TOTAL_A_PAGAR = '$total_final_a_pagar' ";
  						$where_update   = " cow_neg_cotizacion_CODI_COTI = $id_insert_cotizacion" ;
  						$tabla_update   = " cow_neg_detcotizacion ";

  						$resp_update    = update_bd($tabla_update,$campos_update,$where_update);

						/*
  						$array_insert_detalle_cotizacion["VLOR_TOTALPAGOEQUI"]			   = $total_final_a_pagar ;
						$array_insert_detalle_cotizacion["VALOR_TOTAL_CUOTA_INICIAL"]	   = $total_valor_cuota_inicial;
						$array_insert_detalle_cotizacion["VALOR_TOTAL_EN_PLAN"] 		   = $total_valor_en_plan;
						$array_insert_detalle_cotizacion["TOTAL_DESCUENTO_APROBADO"]       = $total_descuento_aprobado;
						$array_insert_detalle_cotizacion["VALOR_TOTAL_A_PAGAR"] 		   = $total_final_a_pagar; 
						*/
					?>
				</tbody>

		      	<!-- totales tabla -->
		      	<tr>
		      		<td></td>
		      		<td>Total</td>
		      		<td><?php echo $total_cantidad_equipos ?></td>
		      		<td>$ <?php echo number_format(round($total_valor_cuota_inicial), 0, '', '.') ?></td>
		      		<td>$ <?php echo number_format(round($total_valor_en_plan), 0, '', '.') ?></td>
		      		<td></td>
		      		<td> $ <?php echo number_format(round($total_descuento_aprobado), 0, '', '.') ?></td>
		      		<td> $ <?php echo number_format(round($total_final_a_pagar), 0, '', '.') ?></td>  
		      		<td></td>
		      	</tr>
		      	<br>
		      	<tr>
		      		<td colspan="9">* Descuento en equipo no considera aceleración de cuotas de acoc.</td>
		      	</tr>
      		</table>
  		</div>
  	</div>

  	<br>
  	<center>
  		<a href="../pdf.php?id_cotizacion=<?php echo $id_insert_cotizacion ?>"><img src="../Imagenes/pdf.png" width="100" style="cursor:pointer;"/> <br></a>
  		<h3>Exportar a PDF </h3>
  	</center>
  	<br>

<?php require_once('../footer.php'); ?>
