<?php
    error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
    session_start();
    require_once("seguridad.php");
    require_once('../header.php');
    require_once('../conexion/conexion_bd.php');
    require_once ('funciones.php');

    // Cambiar en caso de modificar cantidad de filas
    define("CANTIDAD_FILAS", 2);
    //Valor que se asigna a cada uno de los elementos de la tabla
    $inicio = 1;

?>
<style>
    .campo_vacio{
        border: solid!important;
        border-color: red!important;
        border-width: 1px!important;
    }
</style>
<script type="text/javascript">

    function aplicar_descuento_equipo(num){

        descuento           = parseInt($("#desc_equipos"+num).val());
        valor_total_equipos = parseInt($("#total"+num).val());

        resultado           = valor_total_equipos * descuento;
        valor_a_descontar   = resultado / 100;

        nuevo_total_equipos = valor_total_equipos - valor_a_descontar;
        $("#total"+num).val(parseFloat(nuevo_total_equipos));

        total_facturacion(num);

    }

	function total_facturacion(num){

        var x;
        var total = 0;
        var i;
        var y     = 1;

        for(i=0;i<num;i++){

            x                 = $("#total"+y).val();

            if(x>0){
                total = parseInt(total) + parseInt(x);
                
            }else{
                total = parseInt(total);
                
            }                               
            y++;
        }

        total = total/(1.19);
        total = total.toFixed(2);

        $("#fact_total").val(total);
        $("#fact_total").attr("disabled" ,true);
	}

	function cantidad_descuento(num){
		var x;
		x = $("#desc_equipos"+num).val();
		x = parseInt(x);

		if(x>100){
			alert("Descuento mayor al permitido");
		} 
	}
	
    function cargarCargoFijo(id_plan,numero) {

        var j = numero; // Fila en la que se realizo la selección

        $.ajax({
            url     : "funciones.php",
            type    : "POST",
            data    : { "get_cargo_con_iva" : '1' , 'id_plan' : id_plan }, 
            success : function(result){
                $("#cargo_fijo"+j).val(Math.round(result));
            }
        });

        $("#cant_equipos"+j).attr("disabled" ,false);
        $("#equipos"+j).attr("disabled" ,false);
        $("#desc_equipos"+j).attr("disabled" ,false);
    
    }

    function enviar_cotizacion(){
        
        total_siniva = $("#fact_total").val();

        $.ajax({
            url     : "funciones.php",
            type    : "POST",
            data    : { "get_fidelizacion_plan" : '1' , 'total_siniva' : total_siniva }, 
            success : function(result){
                $("#porcentaje_fidelizacion").val(result);
                valida_campos();

                //$("#cotizacion").submit();
            }
        });

    }

    function valida_campos(){

        var obj_cant_lineas  = $(".cant_linea_");
        var obj_sl_planes    = $(".slplanes");
        var obj_cant_equipos = $(".cant_equipos_"); 
        var obj_equipos      = $(".equipos_");

        var contador_vacios  = 0;

        obj_cant_lineas.each(function(){

            if($(this).val() == '' || $(this).val() == null){
                $(this).addClass('campo_vacio');
                contador_vacios++;
            }
            else{
                $(this).removeClass('campo_vacio');
            }
        });

        obj_sl_planes.each(function(){

            if($(this).val() == '0'){
                $(this).addClass('campo_vacio');
                contador_vacios++;
            }
            else{
                $(this).removeClass('campo_vacio');
            }
        });

        obj_cant_equipos.each(function(){

            if($(this).val() == '' || $(this).val() == null){
                $(this).addClass('campo_vacio');
                contador_vacios++;
            }
            else{
                $(this).removeClass('campo_vacio');
            }
        });

        obj_equipos.each(function(){

            if($(this).val() == '0'){
                $(this).addClass('campo_vacio');
                contador_vacios++;
            }
            else{
                $(this).removeClass('campo_vacio');
            }
        });

        if($(".fact_obj").val() == '' || $(".fact_obj").val() == null){
            $(".fact_obj").addClass('campo_vacio');
            contador_vacios++;
        }
        else{
            $(".fact_obj").removeClass('campo_vacio');
        }

        if(contador_vacios > 0){
            alert('Faltan campos por completar');
        }else{
            $("#facturacion_total").val( $('#fact_total').val() );
            $("#cotizacion").submit();
        }

        
    }

</script>
<body>
    <script type="text/javascript">

        var filas = parseInt(<?php echo (CANTIDAD_FILAS + 1);?>);

        function agregaFila() {

            var nuevaFila = '<tr class="filas_portabilidad">'
                                +'<td>'+
                                    '<div class="num'+filas+'"></div>'+
                                    '<input class="form-control" type="hidden" name="plan'+filas+'" id="plan'+filas+'">'+ 
                                '</td>'+
                                '<td>'+'<div class="tipo_linea'+filas+'"></div>'+  
                                    '<input class="form-control" type="hidden" name="tipo_linea'+filas+'" id="tipo_linea'+filas+'">'+
                                '</td>'+
                                '<td>'+ 
                                    '<input required class="form-control cant_linea_" name="cant_linea'+filas+'" id="cant_linea'+filas+'" type="number" oninput="total_linea('+filas+');total_facturacion('+filas+');">'+
                                '</td>'+    
                                '<td>'+  
                                    '<input required class="form-control" name="cargo_fijo'+filas+'" id="cargo_fijo'+filas+'" type="number" disabled>'+ 
                                '</td>'+ 
                                '<td>'+  
                                    '<input required class="form-control" name="total'+filas+'" id="total'+filas+'" type="number" disabled>'+ 
                                '</td>'+ 
                                '<td>'+ 
                                    '<input class="form-control cant_equipos_" name="cant_equipos'+filas+'" id="cant_equipos'+filas+'" oninput="cantidad_equipos('+filas+');" type="number">'+ 
                                '</td>'+ 
                                '<td>'+
                                    '<div class="llenarEquipos'+filas+'"></div>'+
                                    '<input type="hidden" name="equipo'+filas+'" id="equipo'+filas+'">'+
                                '</td>'+ 
                                '<td> '+ 
                                    '<input class="form-control" name="desc_equipos'+filas+'" id="desc_equipos'+filas+'" oninput="cantidad_descuento('+filas+');aplicar_descuento_equipo('+filas+')" type="number">'+ 
                                '</td>'+ 
                            '</tr>';
            $(".tabla").append(nuevaFila);

            var busca = parseInt(filas);

            
            //AJAX PARA LLENAR PLAN DINAMICAMENTE NUEVAS FILAS
            $.ajax({
                url     : "funciones.php",
                type    : "POST",
                data    : {"llenarPlan" : busca}, 
                success : function(result){
                $(".num"+busca).html(result);
            }});

            
            //AJAX PARA LLENAR TIPO LINEA DINAMICAMENTE NUEVAS FILAS
            $.ajax({
                url     : "funciones.php",
                type    : "POST",
                data    : {"llenarTipoLinea" : busca}, 
                success : function(result){
                $(".tipo_linea"+busca).html(result);
            }});

            //AJAX PARA LLENAR EQUIPOS DINAMICAMENTE NUEVAS FILAS
             $.ajax({
                url     : "funciones.php",
                type    : "POST",
                data    : {"llenarEquipos" : busca}, 
                success : function(result){
                $(".llenarEquipos"+busca).html(result);
            }});
            

            filas++;
            
            $("#total_filas_tabla").val(filas);
        }

        function eliminarFila() {
            var trs = $(".tabla tr").length;
            if(trs>1){
                // Eliminamos la ultima columna
                $(".tabla tr:last").remove();
            }
            
            filas--;
        
            $("#total_filas_tabla").val(filas);
        }

        function enviarDatos(e) {
            $("#total_filas_tabla").val(filas);
            return true;
        }

    </script>
    <header>
        <div>
            <img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
        </div> <!-- / #logo-header -->
        <span class="HeaderTitulo">Cotizador Portabilidad</span> 
        <span class="HeaderDerecha"> V1.0<br></span>
    </header>
    <form id="cotizacion" name="cotizacion" action="cotizar.php" method="POST" onsubmit="return enviarDatos(this);">
        <br>

        <input type="hidden" name="total_filas_tabla" id="total_filas_tabla" value="<?php echo CANTIDAD_FILAS;?>">
        <input type="hidden" name="porcentaje_fidelizacion" id="porcentaje_fidelizacion" value="" />
        <input type="hidden" name="cantidad_total_lineas" id="cantidad_total_lineas" value="" />
        <input type="hidden" name="facturacion_total" id="facturacion_total" value="" />


        <table border="1px" width="100%">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td align="center" colspan="2" bgcolor="#0072AE"><h2 style="color:white;">Datos Generales</h2></td>
                        </tr>
                        <tr>
                            <td align="right"> Raz&oacute;n Social:</td>
                            <td> 
                                <input class="form-control" name="nombre_cliente" type="text" size="33">
                            </td>
                        </tr>
                        <tr>
                            <td align="right"> Rut:</td>
                            <td> 
                                <input class="form-control" name="rut_cliente" type="text" size="33" onchange="Valida_Rut(this)">
                            </td>
                        </tr>
                        <tr>
                            <td align="right"> N&deg; de BPO:</td>
                            <td> 
                                <input class="form-control" name="numero_bpo" type="text" size="33">
                            </td>
                        </tr>
                        <tr>
                            <td align="right"> Canal:</td>
                            <td><?php echo llenarCanales(); ?></td>                    
                        </tr>
                    </table>
                </td>
                    <td>
                        <table width="100%">
                            <tr>
                                <td align="center" colspan="2" bgcolor="#0072AE"><h2 style="color:white;">Resumen Propuesta</h2></td>
                            </tr>
                            <tr>
                                <td align="right"> Tipo de Solicitud:</td>
                                
                                <td><?php echo llenarTipoSolicitud();?></td>
                            </tr>
                            <tr>
                                <td align="right"> L&iacute;neas Totales Voz Propuesta:</td>
                                <td> <input class="form-control" name='lineas_totales' id='lineas_totales' type='text'></td>
                            </tr>
                            <tr>
                                <td align="right"> Facturaci&oacute;n Total (sin IVA):</td>
                                <td> <input class="form-control" name="fact_total" id="fact_total" type="text"></td>
                            </tr>
                            <tr>
                                <td align="right"> Facturaci&oacute;n Objetivo (sin IVA):</td>
                                <td> <input class="form-control fact_obj" name="fact_obj" type="text"></td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>                
            </table>
            <table>
                <tr>
                    <td height="30">
                        
                    </td>
                </tr>
                
            </table>
            <table width="100%">
                <tr>
                    <td align="right" colspan="8" bgcolor="#0072AE">
                        <div class="row">
                            <div class="col-md-8">
                                <h2 style="color:white;">PROPUESTA DE PLANES MULTIMEDIA</h2>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <input type="button" onclick="agregaFila();" value="Agregar Fila" class="btn btn-success pull-right b1">
                                </div>
                                <div class="col-md-4">
                                    <input type="button" onclick="eliminarFila();" value="Eliminar Fila" class="btn btn-danger pull-left b1">
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#2E9AFE" style="color:white;"> Plan</td>
                    <td align="center" bgcolor="#2E9AFE" style="color:white;"> Tipo de L&iacute;nea</td>
                    <td align="center" bgcolor="#2E9AFE" style="color:white;"> Q</td>
                    <td align="center" bgcolor="#2E9AFE" style="color:white;"> Cargo Fijo Con IVA</td>
                    <td align="center" bgcolor="#2E9AFE" style="color:white;"> Total</td>
                    <td align="center" bgcolor="#2E9AFE" style="color:white;"> Cantidad de equipos</td>
                    <td align="center" bgcolor="#2E9AFE" style="color:white;"> Equipo</td>
                    <td align="center" bgcolor="#2E9AFE" style="color:white;"> Descuento equipos</td>
                </tr>
                <tbody class="tabla">
                    <?php
                        for ($i=0; $i < CANTIDAD_FILAS; $i++) { 
                    ?>    
                    <tr class="filas_portabilidad">                           
                        <td>
                            <?php 
                                echo llenarPlan($inicio); 
                            ?>
                            <input class="form-control" type="hidden" name="plan<?php echo $inicio;?>" id="plan<?php echo $inicio;?>"> 
                        </td>
                        <td><?php echo llenarTipoLinea($inicio) ?> 
                            <input class="form-control" type="hidden" name="tipo_linea<?php echo $inicio;?>" id="tipo_linea<?php echo $inicio;?>">
                        </td>
                        <td> 
                            <input required class="form-control cant_linea_" name='cant_linea<?php echo $inicio;?>' id='cant_linea<?php echo $inicio;?>' type='number' oninput='total_linea(<?php echo $inicio;?>);total_facturacion(<?php echo $inicio;?>);'>
                        </td>    
                        <td> 
                            <input required class="form-control" value="0" name='cargo_fijo<?php echo $inicio;?>' id='cargo_fijo<?php echo $inicio;?>' type='number' disabled>
                        </td>
                        <td> 
                            <input required class="form-control" name='total<?php echo $inicio;?>' id='total<?php echo $inicio;?>' type='number' disabled>
                        </td>
                        <td> 
                            <input class="form-control cant_equipos_" name='cant_equipos<?php echo $inicio;?>' id='cant_equipos<?php echo $inicio;?>' oninput='cantidad_equipos(<?php echo $inicio;?>);' type='number'>
                        </td>
                        <td> 
                            <?php 
                                echo llenarEquipos($inicio); 
                            ?>
                            <input type="hidden" name="equipo<?php echo $inicio;?>" id="equipo<?php echo $inicio;?>">            
                        </td>
                        <td> 
                            <input class="form-control" name='desc_equipos<?php echo $inicio;?>' id='desc_equipos<?php echo $inicio;?>' oninput="cantidad_descuento(<?php echo $inicio;?>);aplicar_descuento_equipo(<?php echo $inicio;?>);" type='number'>
                        </td>
                    </tr>
                <?php $inicio++; } ?> 
                </tbody>
            </table>
            <div class="row">
                <div class="col-sm-4" style=""></div>
                <div class="col-sm-4" align="center" style="">
                    <input type="button" name="cotizar" value="Cotizar" class="btn btn-info" onclick="enviar_cotizacion()">
                    <input type="button" name="volver" value="Volver" class="btn btn-warning" onclick="location.href='<?php echo $URL_home_pricipal;?>'" />
                </div>
                <div class="col-sm-4" style=""></div>
            </div>          
        </form>
        <?php  
            require('../footer.php');
        ?>