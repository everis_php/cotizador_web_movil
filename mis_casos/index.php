<?php session_start();
	require_once("seguridad.php");
	require_once("../header.php");
	require_once('../conexion/conexion_bd.php');

	function amoneda($numero){  
		return $numero;
	}  

	$codigo_identificador = null;
	$id_traslado = null;

	if (isset($_POST['buscar'])) {
		$codigo_identificador =$_POST['codigo_identificador'];
	}

	if (isset($_POST['recotizar'])) {
		$codigo_identificador ="";
	}
 ?> 
<body>
	<header>
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Cotizador Web VP Empresas - Mis Casos</span> 
		<span class="HeaderDerecha"> V1.0<br></span>
	</header>
	<section>
		<form action="<?php echo $_SERVER['PHP_SELF ']; ?>" method="POST" name="form1" class="form-hotizontal">
				<div class="row">
					<div class="col-md-5">
						<h3>Favor ingrese los siguientes datos:</h3>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">RUT Ejecutivo: </label>
						    <div class="col-sm-9">
								<input type="text" name="codigo_identificador" value="<?php echo $codigo_identificador;?>" required class="form-control">
							</div>
						</div>
	  					<div class="form-group">
	  						<div class="col-md-2"></div>
	  						<div class="col-md-4">
	  							<br>
	  							<input type="submit" name="buscar" value="Buscar" class="btn btn-info">
	  							&nbsp;
	  							<a href="<?php echo $URL_home_pricipal;?>" class="btn btn-warning">Volver</a>
	  							<input type="hidden" name="key" id="key">
	  						</div>
	  						<div class="col-md-6"></div>
	  					</div>
					</div>
					<div class="col-md-7"></div>	
				</div>
			<?php 

							if (isset($_POST['buscar'])){ 

								$consulta_codigo= "SELECT registro_internet.*, bw_nac,bw_int  FROM registro_internet, tarifas WHERE registro_internet.rut_canal='$codigo_identificador' 
								and registro_internet.id_tarifa=tarifas.id order by id desc";

								$ejecutar_codigo = $mysqli->query($consulta_codigo);

								if ($ejecutar_codigo != false) {
									?>
									<h1>Listado de Cotizaciones - Internet Dedicado</h1>
									&nbsp;&nbsp;
									<table class="table">
										<tr>
											<th>Rut</th>
											<th>Nombre Cliente</th>
											<th>Fecha Cotización</th>
											<th>Nro OFT</th>
											<th>BW</th>
											<th>Estado</th>
											<th>Codigo Identificador</th>
										</tr>
										<?php
										do{ 
										?>						
										<tr>
											<td align="center">
												<?php echo $filas_codigo['rut_cliente']; ?>
											</td>
											<td align="center">
												<?php echo $filas_codigo['nombre_cliente']; ?>
											</td>
											<td align="center">
												<?php echo $filas_codigo['fecha']; ?>
											</td>
											<td align="center">
												<?php echo $filas_codigo['nro_oft']; ?>
											</td>
											<td align="center">
												<?php echo $filas_codigo['bw_nac']."/".$filas_codigo['bw_int']; ?>
											</td>
											<td align="center">
												<?php if ($filas_codigo['ingresado']=="SI") {
													echo "Ingresado";
												} else {
													echo "Sin Ingresar";
												}
												 ?>
											</td>
											<td align="center">
												<a href="index.php?id=<?php echo $filas_codigo['id']; ?>"><?php echo $filas_codigo['id']; ?></a>
											</td>								
										</tr>
									<?php 
										} while ($filas_codigo = $ejecutar_codigo->fetch_array());?>
									</table>
									<?php
								} 

									$consulta_codigo = "SELECT cotizacion_firewall.*, nombre  FROM cotizacion_firewall, precio_equipo, firewall WHERE cotizacion_firewall.rut_ejecutivo='$codigo_identificador' 
									and cotizacion_firewall.id_precio=precio_equipo.id and precio_equipo.id_firewall=firewall.id order by id desc";
									$ejecutar_codigo = $mysqli->query($consulta_codigo);
									$registros = $ejecutar_codigo->num_rows;

								if ($registros != 0) {
									$filas_codigo = $ejecutar_codigo->fetch_assoc();
									?>
									<br><br>
									<h1>Listado de Cotizaciones - Firewall</h1>
									&nbsp;&nbsp;
									<table class="table">
										<tr>
											<th>Rut</th>
											<th>Nombre Cliente</th>
											<th>Fecha Cotización</th>
											<th>Tipo Firewall</th>
											<th>Estado</th>
											<th>Codigo Identificador</th>
										</tr>
										<?php
										do{ 
										?>						
										<tr>
											<td align="center">
												<?php echo $filas_codigo['rut_cliente']; ?>
											</td>
											<td align="center">
												<?php echo $filas_codigo['nombre_cliente']; ?>
											</td>
											<td align="center">
												<?php echo $filas_codigo['fecha']; ?>
											</td>
											<td align="center">
												<?php echo $filas_codigo['nombre']; ?>
											</td>
											<td align="center">
												<?php if ($filas_codigo['ingresado']=="SI") {
													echo "Ingresado";
												} else {
													echo "Sin Ingresar";
												}
												 ?>
											</td>
											<td align="center">
												<a href="index.php?id=<?php echo $filas_codigo['id']; ?>"><?php echo $filas_codigo['id']; ?></a>
											</td>								
										</tr>
									<?php 
										} while ($filas_codigo = $ejecutar_codigo->fetch_assoc());?>
									</table>

									<?php
								} 
								

							}

							 ?>




			<?php

			if (isset($_POST['volver']) and $_POST['volver']=="Volver") {
				header("location:".$URL_home_pricipal);
			}
			
			if (isset($_GET['id']) and $_GET['id']!=""){ 

				$codigo_identificador = $_GET['id'];

				$consulta_codigo= "SELECT registro_internet.*,Tarifas.bw_nac, Tarifas.bw_int,descuento.*, medio_acceso.* FROM REGISTRO_INTERNET, Tarifas,descuento, medio_acceso WHERE registro_internet.id_tarifa=tarifas.id and registro_internet.id_descuento=descuento.id_descuento and registro_internet.id_medio_acceso=medio_acceso.id_medio_acceso
									and registro_internet.id='$codigo_identificador'";
				mysql_query("SET NAMES 'utf8'");
				$ejecutar_codigo=mysql_query($consulta_codigo,$conexion) or die(mysql_error());
				$filas_codigo=mysql_fetch_assoc($ejecutar_codigo);
				
				
				$id_internet_dedicado=$filas_codigo['id'];
				$rut_ejecutivo = $filas_codigo['rut_canal'];
				$nombre_ejecutivo = $filas_codigo['nombre_canal'];
				$segmento_cliente = $filas_codigo['segmento_canal'];
				$rut_cliente = $filas_codigo['rut_cliente'];
				$nombre_cliente = $filas_codigo['nombre_cliente'];
				
				$numero_oft = $filas_codigo['nro_oft']; 
				$costo_oft = amoneda($filas_codigo['costo_oft'], "pesos");

				$bw_nac = $filas_codigo['bw_nac'];
				$bw_int = $filas_codigo['bw_int'];
				
				$renta_12 = $filas_codigo['precio_12'];
				$renta_24 = $filas_codigo['precio_24'];
				$renta_36 = $filas_codigo['precio_36'];
				$renta_48 = $filas_codigo['precio_48'];

				$cuota_1 = $filas_codigo['cuota_1'];
				$cuota_2 = $filas_codigo['cuota_2'];
				$cuota_3 = $filas_codigo['cuota_3'];

				$nombre_descuento=$filas_codigo['nombre_descuento'];
				$porcentaje_descuento=$filas_codigo['porcentaje_descuento'];
				$medio_acceso = $filas_codigo['nombre_medio_acceso'];
	
				$fecha_id = date("d-m-Y", strtotime($filas_codigo['fecha']));

				if ($filas_codigo!="") {
					?> 
						<h2>Tarifas por Internet Dedicado</h2><br>
								<table>
									
									<tr><td><b>Rut ejecutivo:</b></td>
										<td><?php echo $rut_ejecutivo; ?></td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td><b>Nombre Ejecutivo:</b></td>
										<td><?php echo $nombre_ejecutivo; ?></td>
									</tr>
									<tr><td><strong>Codigo Identificador:</strong></td>
										<td><?php echo $codigo_identificador; ?></td>
										<input type="hidden" name="id_internet"  value="<?php echo $codigo_identificador; ?>"></input>
									</tr>
									<tr><td><b>Fecha de Cotización:</b></td>
										<td><?php echo $fecha_id; ?></td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td><b>Segmento Cliente:</b></td>
										<td><?php echo $segmento_cliente; ?></td>
									</tr>
									<tr><td><b>Rut Cliente:</b></td>
										<td><?php echo $rut_cliente; ?></td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td><b>Nombre Cliente:</b></td>
										<td><?php echo $nombre_cliente; ?></td>
									</tr>
									<tr><td><b>Nro OFT:</b></td>
										<td><?php echo $numero_oft; ?></td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td><b>Costo OFT (CLP):</b></td>
										<td><?php echo "$".number_format($costo_oft,0,",","."); ?></td>
									</tr>
									<tr><td><b>Medio Acceso:</b></td>
										<td><?php echo $medio_acceso; ?></td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td><b>BW:</b></td>
										<td><?php echo $bw_nac."/".$bw_int; ?></td>
									</tr>
									<tr><td><b>Dscto. Utilizado:</b></td>
										<td><?php echo $nombre_descuento; ?></td>
									</tr>
									
									<tr>
										<td colspan="4" align="left">
											<b>- Las Tarifas por Internet Dedicado son las siguientes:</b>
										</td>
									</tr>
									<tr>
										<?php 
											if ($cuota_1!=0 and $cuota_2!=0 and $cuota_3!=0) {
										?>
												<td colspan=2>
											<table>
												<tr>
													<td colspan=4><b>-Cargo Inicial:</b></td>
												</tr>
												<tr>
													
													<td>1 Cuota de:</td>
													<td align="right"><?php echo $cuota_1; ?></td>
													<td>UF + IVA</td>
												</tr>
												<tr>
													
													<td>2 Cuotas de:</td>
													<td align="right"><?php echo $cuota_2; ?></td>
													<td>UF + IVA</td>
												</tr>
												<tr>
													
													<td>3 Cuotas de:</td>
													<td align="right"><?php echo $cuota_3; ?></td>
													<td>UF + IVA</td>
												</tr>
												<tr>
													<td>
														&nbsp;&nbsp;
													</td>
												</tr>
											</table>
										</td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<?php
											}
										?>
										<td td colspan=2>
											<table>
												<tr>
													<td colspan="4"><b>-Renta Mensual:</b></td>
													<?php if ($porcentaje_descuento!=0){ ?> 
													<td align ="center">&nbsp;&nbsp;</td>
													<td colspan=2><b><?php echo "Tarifa con ".($nombre_descuento) ?></b></td>
												<?php } ?>
												</tr>
												<tr>
													
													<td>12 pagos de:</td>
													<td align="right"><?php echo round($renta_12,2); ?></td>
													<td colspan=2>UF/Mes + IVA</td>
													<?php if ($porcentaje_descuento!=0){ ?> 
													<td align ="center">&nbsp;&nbsp;</td>
													<td align=right><?php echo round($renta_12*(1-$porcentaje_descuento),2); ?></td>
													<td>UF/Mes + IVA</td>
												<?php } ?>
												</tr>
												<tr>
													
													<td>24 pagos de:</td>
													<td align="right"><?php echo round($renta_24,2); ?></td>
													<td colspan=2>UF/Mes + IVA</td>
													<?php if ($porcentaje_descuento!=0){ ?> 
													<td align ="center">&nbsp;&nbsp;</td>
													<td align=right><?php echo round($renta_24*(1-$porcentaje_descuento),2); ?></td>
													<td>UF/Mes + IVA</td>
												<?php } ?>
												</tr>
												<tr>
													
													<td>36 pagos de:</td>
													<td align="right"><?php echo round($renta_36,2); ?></td>
													<td colspan=2>UF/Mes + IVA</td>
													<?php if ($porcentaje_descuento!=0){ ?> 
													<td align ="center">&nbsp;&nbsp;</td>
													<td align=right><?php echo round($renta_36*(1-$porcentaje_descuento),2); ?></td>
													<td>UF/Mes + IVA</td>
												<?php } ?>
												</tr>
												<?php if (stristr($segmento_cliente, "PYME")==FALSE) { ?>
												<tr>
													
													<td>48 pagos de:</td>
													<td align="right"><?php echo round($renta_48,2); ?></td>
													<td colspan=2>UF/Mes + IVA</td>
													<?php if ($porcentaje_descuento!=0){ ?> 
													<td align ="center">&nbsp;&nbsp;</td>
													<td align=right><?php echo round($renta_48*(1-$porcentaje_descuento),2); ?></td>
													<td>UF/Mes + IVA</td>
												<?php } ?>
												</tr>
												<?php } ?>
											</table>
										</td>
									</tr>
								</table>
								<table>
									
									<tr>
										<td colspan="2"><br>
											(*) Esta oferta es válida mientras OFT se encuentre vigente.
										</td>
									</tr>
									<tr>
										<td align="center"><br><input type="submit" name="recotizar" value="Realizar Otra búsqueda" class="boton">
										</td>
									</tr>
								</table>
								</section >
							</td>
							<?php 
							} else{

								require_once('conexion1.php');
								mysql_select_db($base,$conexion);


								$consulta_codigo= "SELECT cotizacion_firewall . * , firewall . * , precio_equipo . * , cotizacion_firewall.fecha AS  'fecha_cotizacion' FROM cotizacion_firewall, precio_equipo, firewall WHERE 
										cotizacion_firewall.id_precio=precio_equipo.id and precio_equipo.id_firewall=firewall.id
										and cotizacion_firewall.id='$codigo_identificador'";
							mysql_query("SET NAMES 'utf8'");
							$ejecutar_codigo=mysql_query($consulta_codigo,$conexion) or die(mysql_error());
							$filas_firewall=mysql_fetch_assoc($ejecutar_codigo);

							if ($filas_firewall!="") {
								?> 
						<h3>Tarifas por Firewall</h3>
								<table>
									<tr><td><h1>Datos Cliente:</h1></td></tr>
									<tr><td><strong>Rut ejecutivo:</strong></td>
										<td><?php echo $filas_firewall['rut_ejecutivo']; ?></td>
										<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td><strong>Nombre Ejecutivo:</strong></td>
										<td><?php echo $filas_firewall['nombre_ejecutivo']; ?></td>
									</tr>
									<tr><td><strong>Segmento Cliente:</strong></td>
										<td><?php echo $filas_firewall['segmento_cliente']; ?></td>
									</tr>
									<tr><td><strong>Rut Cliente:</strong></td>
										<td><?php echo $filas_firewall['rut_cliente']; ?></td>
										<td>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td><strong>Nombre Cliente:</strong></td>
										<td><?php echo $filas_firewall['nombre_cliente']; ?></td>
									</tr>
									<tr><td><strong>Tipo Firewall:</strong></td>
										<td colspan="4"><?php echo $filas_firewall['nombre']; ?></td>
									</tr>
									<tr><td><strong>Nro OFT:</strong></td>
										<td><?php echo $filas_firewall['nro_oft']; ?></td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
										<td><strong>Fecha de Cotización:</strong></td>
										<td><?php echo date("d-m-Y",strtotime($filas_firewall['fecha_cotizacion'])); ?></td>
									</tr>
									<tr><td><br></td></tr>
									
								</table>
								<strong>- Según OFT <?php echo $filas_firewall['nro_oft'];?> , las tarifas por Firewall son las siguientes:</strong>
								<table>
									<tr>
										<td>
											<table class="tabla" >
												<?php 
													$id_firewall = $filas_firewall['id_firewall'];
													$consulta_cotizacion= "SELECT * FROM cotizacion WHERE 
													cotizacion.id_firewall='$id_firewall'";
													mysql_query("SET NAMES 'utf8'");
													$ejecutar_cotizacion=mysql_query($consulta_cotizacion,$conexion) or die(mysql_error());
													$filas_cotizacion=mysql_fetch_assoc($ejecutar_cotizacion);

												 ?>
												<tr><td colspan="5" align="left"><strong>Renta por <?php echo $filas_firewall['nombre'];?>:</strong></td></tr>
												<tr>
													
													<td>12 Cuotas de:</td>
													<td align="right"><?php echo $filas_firewall['precio_12']; ?></td>
													<td>UF/Mes + IVA</td>
													
												</tr>
												<tr>
													
													<td>24 Cuota de:</td>
													<td align="right"><?php echo $filas_firewall['precio_24']; ?></td>
													<td>UF/Mes + IVA</td>
													
												</tr>
												<tr>
													
													<td>36 Cuota de:</td>
													<td align="right"><?php echo $filas_firewall['precio_36']; ?></td>
													<td>UF/Mes + IVA</td>
													
												</tr>
											</table>
										</td>
										<td>&nbsp;&nbsp;</td>
										<td>
											<table class="tabla">
											<tr><td colspan="4"><strong>Renta por mantención (Opcional):</strong></td></tr>
											<tr>
												
												<td>Mantención Básica:</td>
												<td align="right"><?php echo $filas_firewall['mantencion_basica']; ?></td>
												<td>UF/Mes + IVA</td>
											</tr>
											<tr>
												
												<td>Mantención Full:</td>
												<td align="right"><?php echo $filas_firewall['mantencion_full']; ?></td>
												<td>UF/Mes + IVA</td>
											</tr>
											<tr><td>&nbsp;&nbsp;&nbsp;</td></tr>
											
									</table>
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td><strong>- Código Identificador:</strong></td>
										<td><?php echo $codigo_identificador; ?></td>
									</tr>
									
									<tr>
										<td colspan="2">
											(*) Esta oferta es válida mientras OFT se encuentre vigente.
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<a href="<?php echo $URL_propuestas_fw_soporte.$filas_firewall['archivo_propuesta_normal'];?>" target="_blank">Descargar Propuesta a Cliente Normal</a>
											<a href="<?php echo $URL_propuestas_fw_soporte.$filas_firewall['archivo_propuesta_descuento'];?>" target="_blank">Descargar Propuesta a Cliente con Descuento</a>
										</td>
									</tr>
									<tr>
										
										<td align="center"><br><input type="submit" name="recotizar" value="Realizar Otra búsqueda" class="boton"></td>
										
									</tr>
								</table>
								</section >
							</td>
							<?php 
							} else{
								echo "<br><br><br><h1>No se encuentra el código consultado</h1> <br>";
								}
								}
						}		
							?> 
				</tr>
			</table>
		</form>
	</section>
	<?php  
		require_once('../footer.php');
	?>