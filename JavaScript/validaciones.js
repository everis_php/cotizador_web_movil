function cargarCargoFijo(valor,numero) {

    var i = parseInt(valor);
    var j = numero;
    $("#cargo_fijo"+j).val(i);
    $("#plan"+j).val(i);
    $("#total"+j).attr("disabled" ,true);
}
                        
function total_linea(num){

    var x = $("#cant_linea"+num).val();
    var y = $("#cargo_fijo"+num).val();
    var t = x*y;
    var total=0;
    var z=1;
    var i;
    var j=0;

    $("#total"+num).val(t);
    $("#total"+num).attr("disabled", true);
                            
    for(i = 0; i < num; i++){
        j = $("#cant_linea"+z).val();
        if(j>0){
            total = parseInt(total) + parseInt(j);
        }else{
            total = parseInt(total);
        }                              
        z++;
    }

    $("#lineas_totales").val(total);
    $("#lineas_totales").attr("disabled" ,true);
}
                        
function cantidad_equipos(num){

    var p ="cant_equipos"+num;
    var x = $("#"+p).val();
    var y = $("#cant_linea"+num).val();
    var t = y-x;
    
    if(t<0){
        alert("El valor ingresado es superior a cantidad de lineas contratadas");
        //$(p).focus();
        //$(p).select();
        return false;	
    }
}

function total_facturacion(num){
    var x;
    var total = 0;
    var i;
    var y = 1;
    alert(""+num)                    
    for(i=0;i<num;i++){
        x = $("#total"+y).value;
        if(x>0){
            total = parseInt(total) + parseInt(x);
        }else{
            total = parseInt(total);
        }                               
        y++;
    }
    
    $("#fact_total").val(total);
    $("#fact_total").attr("disabled" ,true);
}