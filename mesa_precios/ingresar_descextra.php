<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
        <?php 
        require('../links.php');
        header("Content-Type: text/html;charset=utf-8");
        require_once('../conexion/conexion_bd.php');

        $consulta = "SELECT * FROM cow_mae_tipoplan";
        $ejecutar = $mysqli->query($consulta);
        
         ?>

 
<body style="background:#E6E6E6">
	
	<header>	
		<div><img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/></div>			
		<span class="HeaderTitulo">Canal</span> 
		<span class="HeaderDerecha">V1.0<br></span>
	</header>   
    <form action="<?php echo 'ingresar_descex.php';?>" name="form1" method="POST">
        <br><br><br>
        <div class="container"> 
            <div class="form-group">       
                <div class="col-sm-3" style=""></div>   
                <div class="col-sm-6 tabla" style="background: white">
                    <br>
                    <table class="table table-striped" border="2">
                        <thead>
                            <tr>
                                <td colspan="3" style="text-align: center;background-color: #0072AE;color: white"><h3>Detalles del canal:</h3>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style=";background-color: #F08D06;color: #F08D06">h</td>
                            </tr>

                        </thead>
                                                 
                         <tbody>
                            <tr>
                                <td align="center" colspan="2">Tipo plan:</td>
                                <td align="center"> 
                                    <select name="nomb_plan">
                                        <?php while($filas = $ejecutar->fetch_array()){?>
                                        <option value="<?php echo $filas['NOMB_TIPOPLAN']?>"><?php echo $filas['NOMB_TIPOPLAN']?></option>
                                        <?php }?>
                                    </select>
                                </td>   
                                                      
                            </tr>
                            <tr>
                                <td align="center" colspan="2">Valor descuento:</td>
                                <td align="center"> <input name="vlor_desc" type="number" required="number"></td>

                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-3" style=""></div>   
            </div>
        </div>  
    <br>
    <div class="row">
                                <div class="col-sm-4" style=""></div>
                                <div class="col-sm-4" align="center" style=""><input type="submit" name="ingresar" value="Ingresar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="history.back()" /></div>
                                <div class="col-sm-4" style=""></div>
                            </div>
                
        </form>
    
		
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>