<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
        <?php 
        require('../links.php');
        header("Content-Type: text/html;charset=utf-8");
        require_once('../conexion/conexion_bd.php');
        $consulta= "SELECT * FROM cow_mae_equipo WHERE ESTD_EQUIPO='0'";
        $ejecutar=$mysqli->query($consulta);
        $cons= "SELECT * FROM cow_mae_plan WHERE ESTD_PLAN='0'";
        $ejec=$mysqli->query($cons);
         ?>

 
<body>
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Equipos-Plan</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
			
                        
		</span>
 
	</header>
	
    
    <form action="<?php echo 'ingresar_equipla.php';?>" name="form1" method="POST">
            <br><br>
            <table width="100%">
                <tr>
                    <td>
                        
                    </td>
                    <td style="width: 50%">
                        <table width="100%">
                            <tr>
                                <td align="center" colspan="2" bgcolor="#0072AE"><h2 style="color:white">Detalles del equipo-plan</h2></td>
                            </tr>
                            <tr>
                                <td align="center"> Nombre del plan:</td>
                                <td> 
                                    <select name="nomb_plan">
                                        <?php while($filas = $ejec->fetch_array()){?>
                                                <option value="<?php echo $filas['NOMB_PLAN']; ?>"><?php echo $filas['NOMB_PLAN']; ?></option>
                                        <?php } ?>                                            
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"> Nombre del equipo abreviado:</td>
                                <td> <input name="nomb_abrev" type="text"  size="25" required></td>
                            </tr>
                            <tr>
                                <td align="center"> Nombre del equipo full: :</td>
                                <td>
                                    <select name="nombre_full">
                                        <?php while($consulta = $ejecutar->fetch_array()){?>
                                                <option value="<?php echo $consulta['NOMB_EQUIPOFULL']; ?>"><?php echo $consulta['NOMB_EQUIPOFULL']; ?></option>
                                        <?php } ?>                                            
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"> Valor en arriendo:</td>
                                <td> <input name="valor_arriendo" type="text"  size="25" required></td>
                            </tr>
                            <tr>
                                <td align="center"> Valor equipo-plan:</td>
                                <td> <input name="valor_equipo" type="text"  size="25" required></td>
                            </tr>
                            <tr>
                                <td align="center"> Valor en descuento:</td>
                                <td> <input name="valor_desc" type="text"  size="25" required></td>
                            </tr>
                            
                           
                            
                        </table>
                    </td>
                    <td>
                        
                    </td>
                </tr>            
            </table>
            <table>
                <tr>
                    <td height="82">
                        
                    </td>
                </tr>
                
            </table>
            
            <div class="row">
                <div class="col-sm-4" style=""></div>
                <div class="col-sm-4" align="center" style=""><input type="submit" name="ingresar" value="Ingresar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="location='buscar_equipos_plan.php'" /></div>
                <div class="col-sm-4" style=""></div>
            </div>
            <br><br>
        </form>
    
		
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>