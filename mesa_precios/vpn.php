<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
<script type="text/javascript">

            function validar(e) {
                tecla = (document.all) ? e.keyCode : e.which;
                if (tecla==8) return true; //Tecla de retroceso (para poder borrar)
                if (tecla==44) return true; //Coma ( En este caso para diferenciar los decimales )
                if (tecla==48) return true;
                if (tecla==49) return true;
                if (tecla==50) return true;
                if (tecla==51) return true;
                if (tecla==52) return true;
                if (tecla==53) return true;
                if (tecla==54) return true;
                if (tecla==55) return true;
                if (tecla==56) return true;
                patron = /1/; //ver nota
                te = String.fromCharCode(tecla);
                return patron.test(te); 
            } 
            function validarInicio(inicio,fin){
                var y = fin;
                var n = inicio;
                var x = document.getElementById('cant_inicio').value;
                x=parseInt(x);

                if(x<n || x>y){
                alert("Rango ingresado invalido..");                                
                document.getElementById('cant_inicio').focus();
                document.getElementById('cant_inicio').select();
                return false;   
                }       
            }

            function validarFin(inicio,fin){
                var y=fin;
                var n =inicio;
                var x = document.getElementById('cant_final').value;
                x=parseInt(x);

                if(x<n || x>y){
                alert("Rango ingresado invalido..");                                
                document.getElementById('cant_final').focus();
                document.getElementById('cant_final').select();
                return false;   
                }
        
            }

            function validarMin(){
                var n = 15;
                var x = document.getElementById('cant_inicio').value;
                x = parseInt(x);

                if(x < n){
                alert("Rango debe ser superior a 15");                                
                document.getElementById('cant_inicio').focus();
                document.getElementById('cant_inicio').select();
                return false;   
                }
            }


        </script>

        <?php 
        require('../links.php');
        header("Content-Type: text/html;charset=utf-8");
        require_once('../conexion/conexion_bd.php');

        $indice = $_POST['vpn'];
        $consulta = "SELECT * FROM cow_mae_vpn where CANT_INICIO='$indice'";
	    $ejecutar = $mysqli->query($consulta);
        $filas = $ejecutar->fetch_array();


        if(isset($_POST['eliminar'])){
            
            $indice = $_POST['vpn'];
            $consulta2 = "UPDATE cow_mae_vpn SET ESTD_VPN='1' WHERE CANT_INICIO='$indice'";
            $ejecutar2 = $mysqli->query($consulta2);
            header("location:buscar_vpn.php");
        } 

        $consulta2 = "SELECT * FROM cow_mae_vpn";
        $ejecutar2 = $mysqli->query($consulta2);
        $i = 1;
        $numero = $ejecutar2->num_rows;
        $fin = 0;
        $inicio = 1000;
        $maximo;
        $minimo;

        while($filas2 = $ejecutar2->fetch_array()){
            $minimo = $filas2["CANT_INICIO"];
            $maximo = $filas2["CANT_FIN"];
            
            if($maximo>$fin){
                $fin=$maximo;
                
            }
            if($minimo<$inicio){
                $inicio=$minimo;
                
            }
            $i++;
            
        }
        
         ?>

 
<body>
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">VPN</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
			
                        
		</span>
 
	</header>
	
    
        <form action="<?php echo 'modificar_vpn.php' ?>" method="POST" name="form1" id="form1">
            <table>
                <tr>
                    <td height="80">
                        
                    </td>
                </tr>
                
            </table>
            <table width="100%">
                <tr>
                    <td>
                        
                    </td>
                    <td style="width: 50%">
                        <table width="100%">
                            <tr>
                                <td align="center" colspan="2" bgcolor="#0072AE"><h2 style="color:white">Descripcion del VPN</h2></td>
                            </tr>
                            <tr>
                                <td align="center"><p>Cantidad de inicio:</p></td>
                                <td> 
                                    <input type="number" min="<?php echo $filas["CANT_INICIO"];?>" max="<?php echo $filas["CANT_FIN"];?>" name="cant_inicio" id="cant_inicio" required value="<?php echo $filas["CANT_INICIO"];?>" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center"> Cantidad final:</td>
                                <td> <input name="cant_final" id="cant_final" min="<?php echo $filas["CANT_INICIO"];?>" max="<?php echo $filas["CANT_FIN"];?>" required  type="number" value="<?php echo $filas["CANT_FIN"];?>"></td>
                            </tr>
                            <tr>
                                <td align="center"> Valor VPN:</td>
                                <td> <input name="valor_vpn" type="number" required  value="<?php echo $filas["VLOR_VPN"];?>" >
                                    <input type="hidden" name="indice" value="<?php echo $indice?>">
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                    <td>
                        
                    </td>
                </tr>            
            </table>
            <table>
                <tr>
                    <td height="92">
                        
                    </td>
                </tr>
                
            </table>
            
            <table>
                <tr>
                            <div class="row">
                                <div class="col-sm-4" style=""></div>
                                <div class="col-sm-4" align="center" style=""><input type="submit" name="ingresar" value="Ingresar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="history.back()" /></div>
                                <div class="col-sm-4" style=""></div>
                            </div>
								
							</tr>
                
            </table>
            <table>
                <tr>
                    <td height="188">
                        
                    </td>
                </tr>
                
            </table>
        </form>
    
		
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>