<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>
        
</head>

<?php 
    require('../links.php');
    $error=$_GET['v'];
    $nombre=$_GET['c'];
    if(isset($error)){
        echo '<script language="javascript">alert("El nombre del Tipo de Canal ya existe...");</script>'; 
    }
?>

<body style="background:#E6E6E6">
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Canal</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
			
                        
		</span>
 
	</header>
	
    <br><br><br>
    <form action="ingresar_can.php" name="form1" method="POST">
        <br><br>
        <div class="container"> 
            <div class="form-group">       
                <div class="col-sm-3" style=""></div>   
                <div class="col-sm-6 tabla" style="background: white">
                    <br>
                    <table class="table table-striped" border="2" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <td colspan="3" style="text-align: center;background-color: #0072AE;color: white"><h3>Detalles del canal:</h3>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="background-color: #F08D06;color: #F08D06">
                                    <div style="height: 12px" colspan="3" style="background-color: #F08D06;color: #F08D06"></div>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="text-align: center;"><h5>Nombre del canal:</h5></td>
                                <td colspan="2"><input class="form-control"  type="text" name="nombre_canal" value="<?php echo $_GET['c'] ?>" required>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;"><h5>Tipo de comision:</h5></td>
                                <td colspan="2">
                                    <select name="valor_fact">
                                        <option value='Mensual'>Mensual</option>
                                        <option value='Diferenciada'>Diferenciada</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-3" style=""></div>   
            </div>
        </div>           
        <br><br>
        <div class="row">
            <div class="col-sm-4" style=""></div>
            <div class="col-sm-4" align="center" style=""><input type="submit" name="ingresar" value="Ingresar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="location='buscar_tipo_canal.php'" />
            </div>
            <div class="col-sm-4" style=""></div>
        </div>
        <br><br><br><br><br>       
    </form>	
    <footer><?php require('../footer.php');?></footer> <!-- / #main-footer -->	
</body>
</html>