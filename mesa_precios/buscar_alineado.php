<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
<?php
    session_start();
    require('../links.php');       
    //require_once('funciones.php');
    require_once('../conexion/conexion_bd.php');
    header("Content-Type: text/html;charset=utf-8");
           
    $query = "SELECT * FROM cow_mae_equipo where ESTD_ALINEADO='1'";
    $consulta = $mysqli->query($query);

?>
<body style="background:#E6E6E6">	
	<header>		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Buscar equipo alineado</span> 
		<span class="HeaderDerecha">V1.0<br></span> 
	</header>
    <br><br><br><br><br>
    <form action="<?php echo "alineados.php?token=''".$_COOKIE['tokens']  ?>" method="POST" name="form1" id="form1">     
        <div class="container"> 
            <div class="form-group">       
                <div class="col-sm-3" style=""></div>   
                <div class="col-sm-6 tabla" style="background: white">
                    <br>
                    <table class="table table-striped" border="2">
                        <thead>
                            <tr>
                                <td colspan="3" style="text-align: center;background-color: #0072AE;color: white"><h3>Lista de equipos alineados:</h3>
                                </td>
                            </tr>
                        
                            <tr style="background-color: #F08D06;">
                                <th colspan="1"></th>
                                <th colspan="1" style="text-align: center;color: white;">Nombre del equipo</th>
                                <th colspan="1" style="text-align: center;color: white;">Valor facturación</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i=0;
                                while($filas=$consulta->fetch_array()){
                                    if($i==0){ 
                            ?>
                                <tr>
                                    <td ><input type="radio" name="alineado" id="alineado" value="<?php echo $filas[1];?>" checked="checked">
                                    </td>
                                    <?php }else{ ?>
                                    <td ><input type="radio" name="alineado" id="alineado" value="<?php echo $filas[1];?>"></td>
                                    <?php } ?>
                                    <td nowrap style="text-align: center;"><?php echo $filas['NOMB_EQUIPOFULL'];?></td>
                                    <td style="text-align: center;">
                                        <?php 
                                            $numero = $filas['VLOR_ALINEADO'];
                                            $numero = number_format($numero, 0, ',', '.');
                                            echo $numero;
                                        ?>
                                    </td>
                                </tr>
                            <?php                   
                                    $i++;
                                }                       
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-3" style=""></div>   
            </div>
        </div>                                     
        <div class="row">
            <div class="col-sm-4" style=""></div>
            <div class="col-sm-4" align="center" style=""><input type="submit" name="desactivar" value="Desactivar" class="boton"/> <input type="submit" name="modificar" value="Modificar" class="boton"/><input type="button" name="volver" value="Volver" class="boton" onclick="location.href='index.php?token='".$_COOKIE['tokens'] />
            </div>
            <div class="col-sm-4" style=""></div>
        </div>   
        <br><br><br><br><br>             
    </form>
    <br><br><br>
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>