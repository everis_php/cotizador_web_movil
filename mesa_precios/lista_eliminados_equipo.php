<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>




<?php
        session_start();
        require('../links.php');
        header("Content-Type: text/html;charset=utf-8");
        require_once('../conexion/conexion_bd.php');

        $consulta = "SELECT * FROM cow_mae_equipo WHERE ESTD_EQUIPO='1'";
        $ejecutar = $mysqli->query($consulta);
?>
 
<body style="background:#E6E6E6">
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Buscar equipo</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
		</span>
 
	</header>
    <br><br>
    <form action="<?php echo 'buscar_equipos.php' ?>" method="POST" name="form1" id="form1">                
        <div class="row">              
            <div class="col-md-1"></div>            
            <div class="col-md-10 tabla" style="background: white">
                <h3 align="center">Lista de equipos ingresados:</h3>
                <br>
                <div class="form-group">
                    <label style="align-self: center;" class="control-label col-sm-1" for="canal"></label>
                    <label style="text-align: center;" class="control-label col-sm-2" for="canal">Nombre abreviado</label>
                    <label style="text-align: center;" class="control-label col-sm-2" for="canal">Nombre Completo</label>
                    <label style="text-align: center;" class="control-label col-sm-1" for="canal">Valor</label>
                    <label style="text-align: center;" class="control-label col-sm-1" for="canal">Arriendo</label>
                    <label style="text-align: center;" class="control-label col-sm-2" for="canal">Alineado</label>
                    <label style="text-align: center;" class="control-label col-sm-2" for="canal">Valor Alineado</label>
                </div>
                
                <?php
                    $i=0;
                    $j=1;
                    while($filas = $ejecutar->fetch_array()){ 
                    $alineado = $filas['ESTD_ALINEADO'];                                 
                ?>
                
                <div class="form-group">
                <?php if($i==0){ ?>                   
                    <div class="col-sm-1">
                    <input type="radio" name="equipo" id="equipo" value="<?php echo $filas[0];?>" checked="checked">
                    </div>
                    <div class="col-sm-2" style="text-align: center;"><?php echo $filas['NOMB_EQUIPOABREV'];?></div>
                    <div class="col-sm-2" style="text-align: center;"><?php echo $filas['NOMB_EQUIPOFULL'];?></div>
                    <div class="col-sm-1" style="text-align: center;"><?php echo $filas['VLOR_COSTOEQUIPO'];?> </div>
                    <div class="col-sm-1" style="text-align: center;"><?php echo $filas['VLOR_MMFF'];?> </div>
                    
                        <?php 
                            $validar = "SELECT * FROM cow_par_estadoparam WHERE CODI_ESTADOPARAM = '$alineado'";
                            $con = $mysqli->query($validar);
                            $datos = $con->fetch_array();
                        ?>
                        <label style="text-align: center;" class="control-label col-sm-2" for="canal"><?php echo $datos['DESC_ESTADO']?></label>
                                                
                    
                    <div class="col-sm-2" style="text-align: center;"><?php 
                        $numero = $filas['VLOR_ALINEADO'];
                        $numero = number_format($numero, 0, ',', '.');
                        echo $numero;
                    ?> </div>
                    <?php }else{ ?>
                    <div class="col-sm-1">
                        <input type="radio" name="equipo" id="equipo" value="<?php echo $filas[0];?>">
                    </div>
                    <div class="col-sm-2" style="text-align: center;"><?php echo $filas['NOMB_EQUIPOABREV'];?></div>
                    <div class="col-sm-2" style="text-align: center;"><?php echo $filas['NOMB_EQUIPOFULL'];?></div>
                    <div class="col-sm-1" style="text-align: center;"><?php echo $filas['VLOR_COSTOEQUIPO'];?> </div>
                    <div class="col-sm-1" style="text-align: center;"><?php echo $filas['VLOR_MMFF'];?> </div>
                    <?php 
                        $validar2 = "SELECT * FROM cow_par_estadoparam WHERE CODI_ESTADOPARAM = '$alineado'";
                        $con2 = $mysqli->query($validar2);
                        $datos2 = $con2->fetch_array();
                    ?>
                    <label style="text-align: center;" class="control-label col-sm-2" for="canal"><?php echo $datos2['DESC_ESTADO']?></label>                                      
                    <div class="col-sm-2" style="text-align: center;"><?php 
                        $numero = $filas['VLOR_ALINEADO'];
                        $numero = number_format($numero, 0, ',', '.');
                        echo $numero;
                    ?> </div>
                    <?php } ?>
                </div> 

                <?php 
                     $i++;
                    }
                ?>
            <br>
            </div>  
            <div class="col-md-1"></div>                   
        </div>
        <div class="row">
            <div class="col-sm-3" style=""></div>
            <div class="col-sm-5" align="center" style=""><input type="submit" name="activar" id="activar" value="Activar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="location='buscar_equipos.php'" /></div>
            <div class="col-sm-4" style=""></div>
        </div>
    </form>
    <table>
                <tr>
                    <td height="242">
                        
                    </td>
                </tr>
                
            </table>
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>