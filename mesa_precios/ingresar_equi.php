<?php 
    require('../links.php');
    require_once('../conexion/conexion_bd.php');
    header("Content-Type: text/html;charset=utf-8");
    session_start();
    date_default_timezone_set("America/Santiago");

    $abr_equipo=$_POST['abr_equipo'];
    $nombre=$_POST['nomb_equipo'];
    $valor=$_POST['valor_equipo'];
    $arriendo=$_POST['arriendo_equipo'];
    $estd_alineado=$_POST['estd_alineado'];
    $valor_alineado=$_POST['valor_alineado'];
    $fecha = date("Y-m-d");
    $usuario = $_COOKIE["nombre_usuario"]." ".$_COOKIE["apellido_usuario"];

    $verificar = "SELECT * FROM cow_mae_equipo WHERE NOMB_EQUIPOABREV='$abr_equipo'";
    $consulta = $mysqli->query($verificar);
    $row = $consulta->num_rows;
    if($row > 0){
            header("location: ingresar_equipo.php?v=error&c=".$canal."&n=".$tipo_comision);
    }else{   
        $consulta = "INSERT INTO `cow_mae_equipo`(`NOMB_EQUIPOABREV`, `NOMB_EQUIPOFULL`, `VLOR_COSTOEQUIPO`, `VLOR_MMFF`, `FECH_REGISTRO`, `NOMB_USUARIOREG`, `FECH_ACTUALIZACION`, `NOMB_USUARIOACTUALIZACION`, `ESTD_EQUIPO`, `ESTD_ALINEADO`, `VLOR_ALINEADO`) VALUES ('$abr_equipo','$nombre','$valor','$arriendo','$fecha','$usuario','','','0','$estd_alineado','$valor_alineado')";
	   $ejecutar = $mysqli->query($consulta);
        
        if($ejecutar){
            header("location: buscar_equipos.php");
            exit;
        }
    }
?>