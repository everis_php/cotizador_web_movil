<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>

<script type="text/javascript">


    function ingresa_plan(nombre){
        
        $.ajax({
                    url : "nuevo_tipoplan.php",
                    type : "POST",
                    data : {"nombre" : nombre}, 
                success: function(result){
                   
                    $("#desc_tipo_plan").html(result);
                }});

    }
</script>
<?php 
    require('../links.php');
    //require('header.php');
    header("Content-Type: text/html;charset=utf-8");
    require_once('../conexion/conexion_bd.php');
    
    $consulta = "SELECT * FROM cow_mae_tipoplan";
    $ejecutar = $mysqli->query($consulta);

     $error=$_GET['v'];
    $nombre=$_GET['c'];
    if(isset($error)){
        echo '<script language="javascript">alert("El nombre del plan ya existe...");</script>'; 
    }
?>
<body style="background:#E6E6E6">
       
    <header>
               
        <div>
            <img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>            
        </div> <!-- / #logo-header -->
        <span class="HeaderTitulo">Datos a ingresar </span> 
        <span class="HeaderDerecha"> V1.0 <br> </span>
    </header>
        
    <form action="<?php echo 'ingresar_pl.php' ?>" method="POST" name="form1">
            <br>
            <div class="row">                                
                <div class="col-sm-3" style=""></div>
                <div class="col-sm-6 tabla" style="background: white">
                    <h3 align="center">Datos a ingresar:</h3>
                    <br>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Nombre:</label>
                        <div class="col-sm-4"><input type="text" size="25" name="nombre_plan" required="text">
                        </div>                
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Valor:</label>
                        <div class="col-sm-4"><input type="number" size="28" name="valor_plan" required="number"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Min. Emi:</label>
                        <div class="col-sm-4"><input type="number" size="25" name="min_emi" required="number">  </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Min. Rec:</label>
                        <div class="col-sm-4"><input type="number" size="25" name="min_rec" required="number"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Val. MB:</label>
                        <div class="col-sm-4"><input type="number" size="25" name="val_mb" required="number"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Comi Venta:</label>
                        <div class="col-sm-4"><input type="number" size="25" name="comi_venta" required="number"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Comi Porta:</label>  
                        <div class="col-sm-4"><input type="number" size="25" name="comi_porta" required="number"></div>
                        </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Desc Tipo Plan:</label>
                        <div class="col-sm-2">
                            <select name="desc_tipo_plan" id="desc_tipo_plan">
                            <?php while($filas = $ejecutar->fetch_array()){?>
                                        <option value="<?php echo $filas['NOMB_TIPOPLAN']?>"><?php echo $filas['NOMB_TIPOPLAN']?></option>
                                        <?php }?>
                            </select>
                        </div>
                        <label class="control-label col-sm-2" for="canal">Nuevo tipo plan:</label>  
                        <div class="col-sm-3"><input type="text" name="nuevo_plan" id="nuevo_plan"></div>
                        <div class="col-sm-2"><input type="button" class="boton" name="ingresar_plan" value="Ingresar" onclick="ingresa_plan(nuevo_plan.value);"/></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Equipo Asociado:</label>   
                        <div class="col-sm-4"><select name="equipo">
                            <option value="SI">SI</option>
                            <option value="NO">NO</option>
                        </select></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Cant. Min:</label>
                        <div class="col-sm-4"><input type="text" size="25" name="cant_min" required="text"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Cant. MB:</label>
                        <div class="col-sm-4"><input type="number" size="25" name="cant_mb" required="number"></div>
                    
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">App:</label>
                        <div class="col-sm-4"><input type="text" size="25" name="vlor_app" required="text"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="canal">Desc. Portabilidad:</label>
                        <div class="col-sm-4">
                            <select name="desc_porta">
                                <option value="0">SI</option>
                                <option value="1">NO</option>
                            </select>
                        </div>
                    </div>
                                          
            </div>
            <div class="row">
                <div class="col-sm-4" style=""></div>
                <div class="col-sm-4" align="center" style=""><input type="submit" name="ingresar" value="Ingresar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="location='buscar_planes.php'" /></div>
                <div class="col-sm-4" style=""></div>
            </div>
            <br>
        </form>   

    <footer>
        <?php  require('../footer.php'); ?>      
    </footer> <!-- / #main-footer -->
   
</body>
</html>