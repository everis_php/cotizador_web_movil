<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
        <?php 
        session_start();
        //require_once("seguridad.php");
        require('../links.php');
        header("Content-Type: text/html;charset=utf-8");
        require_once('../conexion/conexion_bd.php');

        $consulta = "SELECT * FROM cow_mae_descextras WHERE ESTD_DESCEXTRA='0'";
	    $ejecutar = $mysqli->query($consulta);

        if(isset($_POST['activar'])){    
            $indice = $_POST['alineado'];
            $consulta2 = "UPDATE cow_mae_descextras SET ESTD_DESCEXTRA='0' WHERE ID_DESCEXTRAS='$indice'";
            $ejecutar2 = $mysqli->query($consulta2);
            header("location:buscar_descuentos_extras.php"); 
    } 
         ?>

 
<body style="background:#E6E6E6">
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Buscar descuentos extras</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
		</span>
 
	</header>
    
    <br>
    <form action="<?php echo "descuentos_extras.php?token='".$_COOKIE['tokens'] ?>" method="POST" name="form1" id="form1">
        <br>
        <div class="container"> 
            <div class="form-group">       
                <div class="col-sm-3" style=""></div>   
                <div class="col-sm-6 tabla" style="background: white">
                    <br>
                    <table class="table table-striped" border="2">
                        <thead>
                            <tr>
                                <td colspan="3" style="text-align: center;background-color: #0072AE;color: white"><h3>Lista de descuentos extras:</h3>
                                </td>
                            </tr>
                        
                            <tr style="background-color: #F08D06;">
                                <th colspan="1"></th>
                                <th colspan="1" style="text-align: center;color: white;">Tipo de plan</th>
                                <th colspan="1" style="text-align: center;color: white;">Descuento</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i=0;
                                $j=1;
                                while($filas = $ejecutar->fetch_array()){ 
                            ?>
                                <tr>
                                    <?php if($i==0){ ?>
                                    <td ><input type="radio" name="alineado" id="alineado" value="<?php echo $filas[0];?>" checked="checked"><input type="hidden" name="opcion" id="opcion" value="<?php echo $i?>">
                                    </td>
                                    <?php }else{ ?>
                                    <td ><input type="radio" name="alineado" id="alineado" value="<?php echo $filas[0];?>"><input type="hidden" name="opcion" id="opcion" value="<?php echo $i?>">
                                    </td>
                                    <?php } ?>
                                    <td nowrap style="text-align: center;"><label for="<?php echo $filas[1];?>"><?php echo $filas[1];?></label></td>
                                    <td style="text-align: center;"><?php echo $filas[2];?></td>
                                    
                                </tr>
                            <?php                   
                                    $i++;
                                }                       
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-3" style=""></div>   
            </div>
        </div>   
        <div class="row">
            <div class="col-sm-4" style=""></div>
            <div class="col-sm-4" align="center" style=""><input type="button" name="ingresar" value="Ingresar" class="boton" onclick="location.href='ingresar_descextra.php'" /> <input type="submit" name="eliminar" id="eliminar" value="Eliminar" class="boton"><input type="submit" name="modificar" value="Modificar" class="boton"/><input type="button" name="eliminados" value="Eliminados" class="boton" onclick="location.href='lista_eliminados_descextras.php'" /><input type="button" name="volver" value="Volver" class="boton" onclick="location.href='index.php?token='".$_COOKIE['tokens'] />
            </div>
            <div class="col-sm-4" style=""></div>
        </div>
    </form>
   <br>
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>