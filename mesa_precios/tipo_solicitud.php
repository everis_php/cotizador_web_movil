<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>
        
</head>
<?php 
    require('../links.php');
    require_once('../conexion/conexion_bd.php');
    header("Content-Type: text/html;charset=utf-8");
        
    $indice = $_POST['solicitud'];
    $consulta = "SELECT * FROM cow_mae_tipo_solicitud where CODI_TIPSOLICITUD='$indice'";
    $ejecutar = $mysqli->query($consulta);
    $filas = $ejecutar->fetch_array();
    if(isset($_POST['eliminar'])){
        $indice = $_POST['solicitud'];
        $consulta2 = "UPDATE cow_mae_tipo_solicitud SET ESTD_TIPSOLICITUD='1' WHERE CODI_TIPSOLICITUD='$indice'";
        $ejecutar2 = $mysqli->query($consulta2);
        header("location:buscar_tipo_solicitud.php");
    }                     
?>
<body style="background:#E6E6E6">
	
	<header>
		<div>
            <img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>		  
		</div>	
		<span class="HeaderTitulo">Tipos de Solicitudes</span> 
        <span class="HeaderDerecha">V1.0<br></span> 	
	</header>	 
	<form action="modificar_tipo_solicitud.php" method="POST" name="form1">	
		<table>
            <tr>
                <td height="80"></td>
            </tr>            
        </table>    
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 tabla" style="background: white">
                <h3 align="center" >Datos a modificar:</h3>
                <br><br>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="canal">Nombre Tipo de Solicitud</label>
                    <div class="col-sm-6">
                        <input class="form-control"  type="text" name="tipo_solicitud" value="<?php echo $filas['NOMB_TIPOSOLICITUD']; ?>" required>
                        <input name="indice" type="hidden" value="<?php echo $indice; ?>"> 
                    </div>
                </div>
                
                <br>
            </div>  
            <div class="col-md-3"></div>
        </div>        
                    
        <div class="row">
            <div class="col-sm-4" style=""></div>
            <div class="col-sm-4" align="center" style=""><input type="submit" name="ingresar" value="Modificar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="location='buscar_tipo_solicitud.php'" /></div>
            <div class="col-sm-4" style=""></div>
        </div>
          
    </form>
    <table>
        <tr>    
            <td height="100"></td>    
        </tr>            
    </table>
		
	<footer>
		<?php require('../footer.php'); ?> 	
	</footer> 
 
	
</body>
</html>