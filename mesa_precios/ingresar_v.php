<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>
        
</head>
        <script type="text/javascript">

            function validarMin(){
                var n = 15;
                var m = 100;
                var x = document.getElementById('lin_ini').value;
                x = parseInt(x);

                if(x < n){
                alert("Rango debe ser superior a 15");                                
                document.getElementById('lin_ini').focus();
                document.getElementById('lin_ini').select();
                return false;   
                }


            }


            function valida_inicio(rango_inicial){
                $.ajax({
                    url : "validar_rango.php",
                    type : "POST",
                    data : {"rango_inicial" : rango_inicial}, 
                success: function(result){
                   
                    var obj = JSON.parse(result);


                    for(var i = 0; i < obj.length; i++){
                        for (var j = obj[i].inicial; j <= obj[i].final; j++) {
                            //alert(obj[i].inicial+" "+obj[i].final);
                            if(rango_inicial == j){
                                alert("Valor dentro de rango... "); 
                                document.getElementById('lin_ini').focus();  
                                document.getElementById('lin_ini').select();
                                
                            }
                            
                        }

                        

                    }

                }});
            }

            function valida_final(rango_inicial){
                $.ajax({
                    url : "validar_rango.php",
                    type : "POST",
                    data : {"rango_inicial" : rango_inicial}, 
                success: function(result){
                   
                    var obj = JSON.parse(result);


                    for(var i = 0; i < obj.length; i++){
                        for (var j = obj[i].inicial; j <= obj[i].final; j++) {
                            //alert(obj[i].inicial+" "+obj[i].final);
                            if(rango_inicial == j){
                                alert("Valor dentro de rango... "); 
                                document.getElementById('lin_fin').focus();  
                                document.getElementById('lin_fin').select();
                                
                            }
                            
                        }

                        

                    }

                }});
            }

        </script>

        <?php 
        require('../links.php');
        header("Content-Type: text/html;charset=utf-8");
       	require_once('../conexion/conexion_bd.php');

        $consulta = "SELECT * FROM cow_mae_vpn";
        $ejecutar = $mysqli->query($consulta);
        $i = 1;
        $numero = $ejecutar->num_rows;
        $fin = 0;
        $inicio = 1000;
        $maximo;
        $minimo;

        while($filas = $ejecutar->fetch_array()){
            $minimo = $filas["CANT_INICIO"];
            $maximo = $filas["CANT_FIN"];
            
            if($maximo>$fin){
                $fin=$maximo;
                
            }
            if($minimo<$inicio){
                $inicio=$minimo;
                
            }
            $i++;
            
        }
        //echo "min".$inicio."max".$fin;
         ?>

 
<body>
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">VPN</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
			
                        
		</span>
 
	</header>
	
    
    <form action="<?php echo 'ingresar_vpn.php';?>" name="form1" method="POST">
            <table>
                <tr>
                    <td height="80">
                        
                    </td>
                </tr>
                
            </table>
            <table width="100%">
                <tr>
                    <td>
                        
                    </td>
                    <td style="width: 50%">
                        <table width="100%">
                            <tr>
                                <td align="center" colspan="2" bgcolor="#0072AE"><h2 style="color:white">Detalles del VPN</h2></td>
                            </tr>
                            <tr>
                                <td align="center"><p>Lineas iniciales:</p></td>
                                <td> <input name="lin_ini" type="number" id="lin_ini" required="number"></td>
                            </tr>
                            <tr>
                                <td align="center"> Lineas finales:</td>
                                <td> <input name="lin_fin" type="number" id="lin_fin" required="number" onclick="valida_inicio(lin_ini.value);validarMin()" /> </td>
                            </tr>
                            <tr>
                                <td align="center"> Valor VPN:</td>
                                <td> <input name="val_vpn" type="number" required="number" onclick="valida_inicio(lin_ini.value);valida_final(lin_fin.value);validarMin();validarMax();"></td>
                            </tr>
                            
                        </table>
                    </td>
                    <td>
                        
                    </td>
                </tr>            
            </table>
            <table>
                <tr>
                    <td height="82">
                        
                    </td>
                </tr>
                
            </table>
            
            <table>
                <tr>
                    <div class="row">
                                <div class="col-sm-4" style=""></div>
                                <div class="col-sm-4" align="center" style=""><input type="submit" name="ingresar" value="Ingresar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="history.back()" /></div>
                                <div class="col-sm-4" style=""></div>
                            </div>
                </tr>
                
            </table>
            <table>
                <tr>
                    <td height="82">
                        
                    </td>
                </tr>
                
            </table>
        </form>
    
		
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>