<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>
</head>
<?php 
        require('../links.php');
        require_once('../conexion/conexion_bd.php');
        header("Content-Type: text/html;charset=utf-8");

        $indice = $_POST['equipo'];
        $consulta = "SELECT * FROM cow_mae_equipo where CODI_EQUIPO='$indice'";
        $ejecutar = $mysqli->query($consulta);
        $filas = $ejecutar->fetch_array();
        $estado = $filas['ESTD_EQUIPO'];
        $estado_ali = $filas['ESTD_ALINEADO'];
        echo $estado_ali;

        $validar = "SELECT * FROM cow_par_estadoparam";
        $con = $mysqli->query($validar);

        $validar2 = "SELECT * FROM cow_par_estadoparam";
        $con2 = $mysqli->query($validar2);
        
       

        if(isset($_POST['eliminar'])){
            
            $indice=$_POST['equipo'];
            $consulta2 = "UPDATE cow_mae_equipo SET ESTD_EQUIPO='1' WHERE CODI_EQUIPO='$indice'";
            $ejecutar2 = $mysqli->query($consulta2);
            header("location:buscar_equipos.php");
        } 
?>
<body style="background:#E6E6E6">
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Equipos</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
			
                        
		</span>
 
	</header>
	
    
        <form action="<?php echo 'modificar_equipo.php' ?>" method="POST" name="form1">
            <br>
            <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 tabla" style="background: white">
                        <h3 align="center" >Detalles del equipo:</h3>
                        <br><br>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="canal">Nombre del equipo:</label>
                            <div class="col-sm-6">
                                <input class="form-control"  type="text" name="nomb_equipo" value="<?php echo $filas['NOMB_EQUIPOFULL']; ?>" required disabled>
                                <input name="indice" type="hidden" value="<?php echo $indice; ?>"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="canal">Costo del equipo:</label>
                            <div class="col-sm-6">
                                <input class="form-control"  type="text" name="valor_equipo" value="<?php echo $filas['VLOR_COSTOEQUIPO']; ?>" required>                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="canal">Costo de arriendo:</label>
                            <div class="col-sm-6">
                                <input class="form-control"  type="text" name="arriendo_equipo" value="<?php echo $filas['VLOR_MMFF']; ?>" required> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="canal">Estado alineado:</label>
                            
                            <div class="col-sm-6">
                                <select name="estd_alineado">
                                    <?php

                                        while($datos2 = $con2->fetch_array()){
                                            echo $datos2;
                                        $com2 = $datos2['VLOR_ESTADOPARAM'];
                                        echo "hola".$com2;
                                        if($estado_ali == $com2){ ?>
                                            <option value="<?php echo $datos2['VLOR_ESTADOPARAM'];?>" selected><?php echo $datos2['DESC_ESTADOPARAM']?> </option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $datos2['VLOR_ESTADOPARAM'];?>"><?php echo $datos2['DESC_ESTADOPARAM']?> </option>
                                    <?php }
                                        }
                                    ?>
                                    
                                </select>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="canal">Valor alineado:</label>
                            <div class="col-sm-6">
                              <input class="form-control"  type="text" name="vlor_alineado" value="<?php echo $filas['VLOR_ALINEADO']; ?>" required>                                
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="col-md-3"></div>
            </div>
            <br>                       
            <div class="row">
                <div class="col-sm-4" style=""></div>
                <div class="col-sm-4" align="center" style=""><input type="submit" name="buscar" value="Modificar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="location='buscar_equipos.php'" /></div>
                <div class="col-sm-4" style=""></div>
            </div>
            <br>
        </form>
    
		
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>