<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
<?php 

    require('../links.php');
    header("Content-Type: text/html;charset=utf-8");
    $error=$_GET['v'];
    $nombre=$_GET['c'];
    if(isset($error)){
        echo '<script language="javascript">alert("El nombre del equipo ya existe...");</script>'; 
    }
        
?>
<body style="background:#E6E6E6">
	
	<header>		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Equipos</span> 
		<span class="HeaderDerecha">V1.0<br></span>
	</header>
   
    <form action="<?php echo 'ingresar_equi.php';?>" name="form1" method="POST">
            <br><br>

            <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 tabla" style="background: white">
                        <h3 align="center">Datos a ingresar:</h3>
                        <br><br>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="canal">Abreviacion del equipo:</label>
                            <div class="col-sm-6">
                                <input class="form-control"  type="text" name="abr_equipo" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="canal">Nombre del equipo:</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="nomb_equipo" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="canal">Valor :</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="number" name="valor_equipo" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="canal">Valor en arriendo:</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="number" name="arriendo_equipo" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="canal">Alineado:</label>
                            <div class="col-sm-6">
                                <select name="estd_alineado">
                                    <option value="1">ACTIVO</option>
                                    <option value="0">INACTIVO</option>
                                </select>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="canal">Valor alineado:</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="valor_alineado">
                            </div>
                        </div>
                    <br>
                    </div>  
                    <div class="col-md-3"></div>
            </div>
            <br>
            
            <div class="row">
                <div class="col-sm-4" style=""></div>
                <div class="col-sm-4" align="center" style=""><input type="submit" name="ingresar" value="Ingresar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="location='buscar_equipos.php'" /></div>
                <div class="col-sm-4" style=""></div>
            </div>
            <br><br>
        </form>
    
		
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>