<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
    <?php 
    session_start();
        //require_once("seguridad.php");
    require('../links.php');
    header("Content-Type: text/html;charset=utf-8");
    require_once('../conexion/conexion_bd.php');
        
    $consulta = "SELECT * FROM cow_mae_tlinea where ESTD_TIPOLINEA='0'";
	$ejecutar = $mysqli->query($consulta);
	
    if(isset($_POST['activar'])){    
            $indice = $_POST['linea'];
            $consulta2 = "UPDATE cow_mae_tlinea SET ESTD_TIPOLINEA='0' WHERE ID_TLINEA='$indice'";
            $ejecutar2 = $mysqli->query($consulta2);
            header("location:buscar_tipo_linea.php"); 
    } 
?>
 
<body style="background:#E6E6E6">
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Lista de tipos de lineas</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
		</span>
 
	</header>
   
        <br>
           
        <form action="<?php echo 'tipo_linea.php' ?>" method="POST" name="form1" id="form1">
                    
                        
                            <div class="row">
                                <div class="col-sm-4" style=" height: 80px"></div>
                                <div class="col-sm-4" style=" background-color: #0072AE"><h3 align="center" style="color: white">Tipos de lineas</h3></div>
                                <div class="col-sm-4" ></div>
                               
                            </div>
                    
                            <div class="row">
                                <div class="col-sm-4" style=""></div>
                                
                                <div class="col-sm-1" style=" background-color: #0072AE;"><h4 align="center" style="color: white"><br> </h4></div>
                                <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="left" style="color: white">Nombre </h4></div>
                                
                               
                            </div>
                        </tr>
                        <tr>
                            <?php
                            $i=0;
                            $j=1;
                            while($filas = $ejecutar->fetch_array()){    
                               
                            ?>
                                                   
                            <div class="row">
                                <div class="col-sm-4" style=""></div>
                                <?php if($i==0){ ?>
                                    <div class="col-sm-1" align="left"><input type="radio" name="linea" id="linea" value="<?php echo $filas[0];?>" checked="checked"></div>
                                <?php }else{ ?>
                                <div class="col-sm-1" align="left"><input type="radio" name="linea" id="linea" value="<?php echo $filas[0];?>"></div>
                                <?php } ?>
                                <div class="col-sm-2" align="left"><label for="<?php echo $filas[1];?>"><?php echo $filas[1];?></label><br></div>
                                
                                <div class="col-sm-4" align="rigth" style=""></div>
                            </div>
                            <?php 
                            $i++;
                            }?>
                        </tr>
                        
                        <br>
			<tr>
                            <div class="row">
                                <div class="col-sm-4" style=""></div>
                                <div class="col-sm-4" align="center" style=""><input type="button" onclick=" location.href='ingresar_linea.php' " class="boton" value="Ingresar" name="boton" /> <input type="submit"  class="boton" id="eliminar" value="Eliminar" name="eliminar" /> <input type="submit" name="modificar" value="Modificar" class="boton"/><input type="button" name="eliminados" value="Eliminados" class="boton" onclick="location.href='lista_eliminados_tipolinea.php'" /><input type="button" name="volver" value="Volver" class="boton" onclick="location='index.php'" /></div>
                                <div class="col-sm-4" style=""></div>
                            </div>
								
							</tr>
							<tr><td>&nbsp;&nbsp;</td></tr>

							
							
						</table>
                    </form>
    <table>
                <tr>
                    <td height="242">
                        
                    </td>
                </tr>
                
            </table>
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>