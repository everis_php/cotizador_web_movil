
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
<?php 
        require('../links.php');
        require('header.php');
        header("Content-Type: text/html;charset=utf-8");
        $indice=$_POST['plan'];
        require_once('../conexion/conexion_bd.php');

        $consulta = "SELECT * FROM cow_mae_plan where NOMB_PLAN='$indice'";
        $ejecutar = $mysqli->query($consulta);
        $filas = $ejecutar->fetch_array();
        $comiti = $filas['DESC_TIPOPLA'];

        $query2 = "SELECT * FROM cow_mae_tipoplan";
        $consulta2 = $mysqli->query($query2);
        
        

        if(isset($_POST['eliminar'])){
            
            $indice = $_POST['plan'];
            $consulta2 = "UPDATE cow_mae_plan SET ESTD_PLAN='1' WHERE NOMB_PLAN='$indice'";
            $ejecutar2 = $mysqli->query($consulta2);
            header("location:buscar_planes.php");
        } 
         ?>
<body>
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Datos a modificar del Plan</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
			
                        
		</span>
 
	</header>
	
    
    <form action="<?php echo 'modificar_plan.php' ?>" method="POST" name="form1">
            <table>
                <tr>
                    <td height="40">
                        
                    </td>
                </tr>
                
            </table>
                <div class="row">                                
                    <div class="col-sm-4" style=""></div>
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Nombre</h4>
                    </div>
                    <div class="col-sm-3"><input type="text" size="26" name="nombre_plan" value="<?php echo $filas['NOMB_PLAN'];?>" disabled>
                        <input type="hidden" name="indice" value="<?php echo $indice; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Valor</h4>
                    </div>
                    <div class="col-sm-2"><input type="number" size="25" name="valor_plan" value="<?php echo $filas['VLOR_CARGOFIJOCONIVA'];?>" required></div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Min. Emi</h4></div>
                    <div class="col-sm-2"><input type="number" size="25" name="min_emi" value="<?php echo $filas['VLOR_MINEMI'];?>" required></div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Min. Rec</h4></div>
                    <div class="col-sm-2"><input type="number" size="25" name="min_rec" value="<?php echo $filas['VLOR_MINRECEP'];?>" required></div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Val. MB </h4></div>
                    <div class="col-sm-2"><input type="number" size="25" name="val_mb" value="<?php echo $filas['VLOR_MB'];?>" required></div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>                
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Comi Venta</h4></div>
                    <div class="col-sm-2"><input type="number" size="25" name="comi_venta" value="<?php echo $filas['VLOR_COMVENTAPYME'];?>" required></div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>  
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Comi Porta</h4></div>
                    <div class="col-sm-2"><input type="number" size="25" name="comi_porta" value="<?php echo $filas['VLOR_COMPORTAPYME'];?>" required></div>
                </div>
                
                <div class="row">
                    <div class="col-sm-4"></div>               
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Desc Tipo Plan</h4></div>
                    <div class="col-sm-2">
                        <select name="desc_tipo_plan">
                            <?php while($filas2 = $consulta2->fetch_array()){
                                        $comi = $filas2['ID_TIPOPLAN']; 

                                        if($comiti == $comi){ ?>
                                            <option value="<?php echo $comi; ?>" selected><?php echo $filas2['NOMB_TIPOPLAN']?> </option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $comi; ?>" ><?php echo $filas2['NOMB_TIPOPLAN']?> </option>
                                    <?php }
                                        }
                                    ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>   
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Equipo Asociado</h4></div>
                    <div class="col-sm-2">
                    <select name="equipo">
                        <?php
                            $op = $filas['FLAG_PERMITEEQUIPO'];
                            if($op == "Si" || $op == "SI"){
                        ?>
                                <option value="SI" selected>SI</option>
                                <option value="NO">NO</option>
                        <?php   }else{ ?>
                                <option value="SI">SI</option>
                                <option value="NO" selected>NO</option>
                        <?php
                        }
                        ?>
                    </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Cant. Min</h4></div>
                    <div class="col-sm-2"><input type="text" size="25" name="cant_min" value="<?php echo $filas['CANT_MIN'];?>" required></div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Cant. MB</h4></div>
                    <div class="col-sm-2"><input type="number" size="25" name="cant_mb" value="<?php echo $filas['CANT_MEGASPLAN'];?>" required></div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">App Ilim.</h4></div>
                    <div class="col-sm-2"><input type="text" size="25" name="vlor_app" value="<?php echo $filas['VLOR_APP'];?>" required></div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Desc Portabilidad</h4></div>
                    <div class="col-sm-2">
                    <select name="desc_porta">
                        <?php
                            $portabi = $filas['APLIC_DESC_PORT'];
                            if($portabi == 0){
                        ?>
                                <option value="0" selected>SI</option>
                                <option value="1">NO</option>
                        <?php   }else{ ?>
                                <option value="0">SI</option>
                                <option value="1" selected>NO</option>
                        <?php
                        }
                        ?>
                    </select>

                    </div>
                </div>            
                
            <table>
                <tr>
                    <td height="40">
                        
                    </td>
                </tr>
                
            </table>
            
            <table>
                <tr>
                    <div class="row">
                                <div class="col-sm-4" style=""></div>
                                <div class="col-sm-4" align="center" style=""><input type="submit" name="modificar" value="Modificar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="history.back()" /></div>
                                <div class="col-sm-4" style=""></div>
                            </div>
                </tr>
                
            </table>
            <table>
                <tr>
                    <td height="82">
                        
                    </td>
                </tr>
                
            </table>
        </form>
    
		
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>