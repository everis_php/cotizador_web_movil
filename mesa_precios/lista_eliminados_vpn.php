<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
        <?php 
        require('../links.php');
        header("Content-Type: text/html;charset=utf-8");
        require_once('../conexion/conexion_bd.php');
        
        $consulta=  "SELECT * FROM cow_mae_vpn WHERE ESTD_VPN='1'";
	   $ejecutar = $mysqli->query($consulta);
         ?>

 
<body>
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Buscar VPN</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
		</span>
 
	</header>
    <table>
                <tr>
                    <td height="120">
                        
                    </td>
                </tr>
                
            </table>
                    <form action="<?php echo 'buscar_vpn.php' ?>" method="POST" name="form1" id="form1">
                    <table width="100%">
                        <tr>
                            <div class="row">
                                <div class="col-sm-4" style=" height: 80px"></div>
                                <div class="col-sm-4" style=" background-color: #0072AE"><h3 align="center" style="color: white">Detalles de lineas VPN</h3></div>
                                <div class="col-sm-4" ></div>
                               
                            </div>
                        </tr>
                        <tr>
                            <div class="row">
                                <div class="col-sm-4    " style=""></div>
                                <div class="col-sm-1" style="background-color: #0072AE;"><h4 align="center" style="color: white"><br> </h4></div>
                                <div class="col-sm-1" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Inicial </h4></div>
                                <div class="col-sm-1" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Final</h4></div>
                                <div class="col-sm-1" style=" background-color: #0072AE;"><h4 align="center" style="color: white">Valor VPN </h4></div>
                                <div class="col-sm-1" style=""></div>
                               
                            </div>
                        </tr>
                        <tr>
                            <?php
                            $i=0;
                            while($filas = $ejecutar->fetch_array()){
                            ?>
                            
                            <div class="row">
                                <div class="col-sm-4" style=""></div>
                                <?php if($i==0){ ?>
                                    <div class="col-sm-1" align="center"><input type="radio" name="vpn" id="vpn" value="<?php echo $filas[0];?>" checked="checked"></div>
                                <?php }else{ ?>
                                <div class="col-sm-1" align="center"><input type="radio" name="vpn" id="vpn" value="<?php echo $filas[0];?>"></div>
                                <?php } ?>
                                
                                <div class="col-sm-1" align="center"><label for="<?php echo $filas["CANT_INICIO"];?>"><?php echo $filas["CANT_INICIO"];?></label><br></div>
                                <div class="col-sm-1" style="" align="center"><?php echo $filas["CANT_FIN"];?></div>
                                <div class="col-sm-1" style="" align="center"><?php echo $filas["VLOR_VPN"];?></div>
                                <div class="col-sm-3" align="rigth" style=""></div>
                            </div>
                            <?php 
                            $i++;
                            }?>
                        <br>
			<tr>
                            <div class="row">
                                <div class="col-sm-4" style=""></div>
                                <div class="col-sm-4" align="center" style=""><input type="submit" name="activar" value="Activar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="history.back();" /></div>
                                <div class="col-sm-4" style=""></div>
                            </div>
								
							</tr>
							<tr><td>&nbsp;&nbsp;</td></tr>

							
							
						</table>
                    </form>
    <table>
                <tr>
                    <td height="242">
                        
                    </td>
                </tr>
                
            </table>
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>