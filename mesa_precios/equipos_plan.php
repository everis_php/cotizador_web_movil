<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
        <?php 
        require('../links.php');
        header("Content-Type: text/html;charset=utf-8");
        $indice=$_POST['alineado'];
        require_once('../conexion/conexion_bd.php');

        $consulta = "SELECT * FROM cow_rel_equipoplan where NOMB_PLAN='$indice'";
        $ejecutar = $mysqli->query($consulta);
        $filas = $ejecutar->fetch_array();

        if(isset($_POST['eliminar'])){
            
            $indice=$_POST['alineado'];
            $consulta2 = "UPDATE cow_rel_equipoplan SET ESTD_EQUIPOPLAN='1' WHERE NOMB_PLAN='$indice'";
            $ejecutar2 = $mysqli->query($consulta2);
            header("location:buscar_equipos_plan.php");
        } 
         ?>

 
<body>
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Equipos</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
			
                        
		</span>
 
	</header>
	
    
        <form action="<?php echo 'modificar_equipo_plan.php' ?>" method="POST" name="form1">
            <table>
                <tr>
                    <td height="80">
                        
                    </td>
                </tr>
                
            </table>
            <table width="100%">
                <tr>
                    <td>
                        
                    </td>
                    <td style="width: 50%">
                        <table width="100%">
                            <tr>
                                <td align="center" colspan="2" bgcolor="#0072AE"><h2 style="color:white">Detalles del equipo</h2></td>
                            </tr>
                            <tr>
                                <td align="center"> Nombre del plan:</td>
                                <td> <td> <input name="nomb_plan" type="text" value="<?php echo $filas[0];?>" size="25" disabled></td>
                                        <input name="indice" type="hidden" value="<?php echo $indice;?>"></td>
                            </tr>
                            <tr>
                                <td align="center"> Nombre del equipo abreviado:</td>
                                <td> <td> <input name="nomb_abrev" type="text" value="<?php echo $filas[1];?>" size="25" disabled></td></td>
                            </tr>
                            <tr>
                                <td align="center"> Nombre del equipo full: :</td>
                                <td> <td> <input name="nombre_full" type="text" value="<?php echo $filas[2];?>" size="25" disabled></td></td>
                            </tr>
                            <tr>
                                <td align="center"> Valor en arriendo:</td>
                                <td> <td> <input name="valor_arriendo" type="text" value="<?php echo $filas[3];?>" size="25" required></td></td>
                            </tr>
                            <tr>
                                <td align="center"> Valor equipo-plan:</td>
                                <td> <td> <input name="valor_equipo" type="text" value="<?php echo $filas[4];?>" size="25" required></td></td>
                            </tr>
                            <tr>
                                <td align="center"> Valor en descuento:</td>
                                <td> <td> <input name="valor_desc" type="text" value="<?php echo $filas[7];?>" size="25" required></td></td>
                            </tr>
                            
                        </table>
                    </td>
                    <td>
                        
                    </td>
                </tr>            
            </table>
            <table>
                <tr>
                    <td height="82">
                        
                    </td>
                </tr>
                
            </table>
            
            
                    <div class="row">
                                <div class="col-sm-4" style=""></div>
                                <div class="col-sm-4" align="center" style=""><input type="submit" name="modificar" value="Modificar" class="boton" /><input type="button" name="volver" value="Volver" class="boton" onclick="history.back()" /></div>
                                <div class="col-sm-4" style=""></div>
                            </div>
               <br><br>
        </form>
    
		
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>