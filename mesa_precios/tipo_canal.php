<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>
</head>
 
<?php 
    require('../links.php');
    header("Content-Type: text/html;charset=utf-8");
    require_once('../conexion/conexion_bd.php');

    $indice = $_POST['solicitud'];

    $query = "SELECT * FROM cow_mae_tipocanal where CODI_TIPOCANAL='$indice'";
    $consulta = $mysqli->query($query);
    $filas = $consulta->fetch_array();
    
    $estado=$filas['ESTD_TIPOCANAL'];
    $comiti=$filas['DESC_CANAL'];

    $validar = "SELECT * FROM cow_par_estadoparam WHERE VLOR_ESTADOPARAM BETWEEN 2 AND 3";
    $con = $mysqli->query($validar);
    //$datos = mysql_fetch_array($con);


    if(isset($_POST['eliminar'])){    
        $indice=$_POST['solicitud'];
        $query3="UPDATE cow_mae_tipocanal SET ESTD_TIPOCANAL='0' WHERE CODI_TIPOCANAL='$indice'";
        $consulta3 = $mysqli->query($query3);
        header("location:buscar_tipo_canal.php"); 
    }            
?>

<body style="background:#E6E6E6">
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Tipo de Canal</span> 
		<span class="HeaderDerecha">V1.0<br></span> 
	</header>		
    
    <form action="modificar_tipo_canal.php" method="POST" name="form1">
        <br><br>
        <div class="container"> 
            <div class="form-group">       
                <div class="col-sm-3" style=""></div>   
                <div class="col-sm-6 tabla" style="background: white">
                    <br>
                    <table class="table table-striped" border="2" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <td colspan="3" style="text-align: center;background-color: #0072AE;color: white"><h3>Resumen Tipo de Canal:</h3>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="background-color: #F08D06;color: #F08D06">
                                    <div style="height: 12px" colspan="3" style="background-color: #F08D06;color: #F08D06"></div>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="text-align: center;"><h5>Nombre del canal:</h5></td>
                                <td colspan="2"><input class="form-control"  type="text" name="nombre_canal" value="<?php echo $filas['NOMB_CANAL']; ?>" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;"><h5>Tipo de comision:</h5></td>
                                <td colspan="2">
                                    <select name="tipo_comision">
                                    <?php while($filas2 = $con->fetch_array()){
                                        $comi = $filas2['DESC_ESTADOPARAM']; 

                                        if($comiti == $comi){ ?>
                                            <option value="<?php echo $comi; ?>" selected><?php echo $comi;?> </option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $comi; ?>" ><?php echo $comi;?> </option>
                                    <?php }
                                        }
                                    ?>
                                </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-3" style=""></div>   
            </div>
        </div>           
        <br><br>            
        <div class="row">
            <div class="col-sm-4" style=""></div>
            <div class="col-sm-4" align="center" style=""><input type="submit" name="ingresar" value="Modificar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="location='buscar_tipo_canal.php'" />
            </div>
            <div class="col-sm-4" style=""></div>
        </div>               
		<br><br><br><br>    
    </form>	
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer>	
</body>
</html>