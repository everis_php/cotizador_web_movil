<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>
        
</head>

<?php 
    require('../links.php');
    $error=$_GET['v'];
    $nombre=$_GET['c'];
    if(isset($error)){
        echo '<script language="javascript">alert("El nombre del Tipo de Canal ya existe...");</script>'; 
    }
?>

<body style="background:#E6E6E6">
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Carga Masiva</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
			
                        
		</span>
 
	</header>
	
    
    <form form action='cargaMasivaEquipoPlanaBD.php' method='post' enctype="multipart/form-data">
            <br><br>
            <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 tabla" style="background: white">
                        <h3 align="center">Ingreso de carga masiva</h3>
                        <br><br>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="canal">Importar archivo:</label>
                            <div class="col-sm-6">
                                <input type='file' name='sel_file' size='20'>
                            </div>
                            <div class="col-sm-2">
                            <input type='submit' class="boton" name='submit' value='Cargar'>
                            </div>
                        </div>
                    </div>  
                    <div class="col-md-3"></div>
            </div>
            <br><br>
            
            <table>
                <tr>
                    <div class="row">
                                <div class="col-sm-4" style=""></div>
                                <div class="col-sm-4" align="center" style=""><input type="button" name="volver" value="Volver" class="boton" onclick="location='buscar_equipos_plan.php'" /></div>
                                <div class="col-sm-4" style=""></div>
                            </div>
                </tr>
                
            </table>
            <table>
                <tr>
                    <td height="82">
                        
                    </td>
                </tr>
                
            </table>
        </form>
    
		
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>