<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
 <?php 
    session_start();
    require('../links.php');
    header("Content-Type: text/html;charset=utf-8");
    require_once('../conexion/conexion_bd.php');
    $query= "SELECT * FROM cow_mae_plan WHERE ESTD_PLAN='1'";
    $consulta = $mysqli->query($query);


    $query3= "SELECT * FROM cow_mae_plan WHERE ESTD_PLAN='1'";
    $consulta3 = $mysqli->query($query3);
    $filas2 = $consulta3->fetch_array();
    

?>

<body style="background:#E6E6E6">
    <header>
		
	<div>
            <img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>			
	</div> <!-- / #logo-header -->
	<span class="HeaderTitulo">Buscar plan</span> 
	<span class="HeaderDerecha">V1.0<br></span>					
    </header>
    <br>
    <form action="<?php echo 'buscar_planes.php' ?>" method="POST" name="form1" id="form1">   
            <br>
<!--<div <div class="col-md-12"></div>         
  <div class="col-md-1"></div>                           
  <div class="col-md-10" style="text-align: center; background-color: #FF8633; color: white;"><h2>Lista de Planes</h2>  </div>  
  <div class="col-md-1"></div>     
</div>   -->              
<div class="container" style="background: white" > 
<br>           
  <table class="table table-striped" border="2">

    <thead>
    	<tr>
    		<td colspan="14" style="text-align: center;background-color: #0072AE;color: white"><h2>Lista de Planes</h2></td>
    	</tr>
    	 	
      <tr style="background-color: #F08D06;">
      	<th ></th>
        <th style="text-align: center;color: white;">Nombre del plan</th>
        <th style="text-align: center;color: white;">Valor plan</th>
        <th style="text-align: center;color: white;">Minutos Emi</th>
        <th style="text-align: center;color: white;">Minutos Rec</th>
        <th style="text-align: center;color: white;">Valor MB</th>
        <th style="text-align: center;color: white;">Comisión Venta</th>
        <th style="text-align: center;color: white;">Comisión Porta</th>
        <th style="text-align: center;color: white;">Desc. T. Plan</th>
        <th style="text-align: center;color: white;">Equipo Asociado</th>
        <th style="text-align: center;color: white;">Cantidad Min</th>
        <th style="text-align: center;color: white;">Cantidad MB</th>
        <th style="text-align: center;color: white;">App Ilimitada</th>
        <th style="text-align: center;color: white;">Desc portabilidad</th>
      </tr>
    </thead>
    <tbody>
    	<?php
            $i=0;
        	while($filas=$consulta->fetch_array()){
                    $codigo = $filas['DESC_TIPOPLA'];
                    $query2= "SELECT * FROM cow_mae_tipoplan WHERE ID_TIPOPLAN='$codigo'";
                    $consulta2 = $mysqli->query($query2);
                    $desc = $consulta2->fetch_array();
        			if($i==0){ 
        ?>
        			<tr>
                        <td ><input type="radio" name="plan" id="plan" value="<?php echo $filas[0];?>" checked="checked"></td>
                        <?php }else{ ?>
                        <td ><input type="radio" name="plan" id="plan" value="<?php echo $filas[0];?>"></td>
                        <?php } ?>
                        <td nowrap style="text-align: center;"><?php echo $filas['NOMB_PLAN'];?></td>
                        <td style="text-align: center;"><?php echo $filas['VLOR_CARGOFIJOCONIVA'];?></td>            
                        <td style="text-align: center;"><?php echo $filas['VLOR_MINEMI'];?></td>
                        <td style="text-align: center;"><?php echo $filas['VLOR_MINRECEP'];?></td>
                        <td style="text-align: center;"><?php echo $filas['VLOR_MB'];?></td>
                        <td style="text-align: center;"><?php echo $filas['VLOR_COMVENTAPYME'];?></td>
                        <td style="text-align: center;"><?php echo $filas['VLOR_COMPORTAPYME'];?></td>
                        <td style="text-align: center;"><?php echo $desc['NOMB_TIPOPLAN'];?></td>
                        <td style="text-align: center;"><?php echo $filas['FLAG_PERMITEEQUIPO'];?></td>
                        <td style="text-align: center;"><?php echo $filas['CANT_MIN'];?></td>
                        <td style="text-align: center;"><?php echo $filas['CANT_MEGASPLAN'];?></td>
                        <td nowrap style="text-align: center;"><?php echo $filas['VLOR_APP'];?></td>
                        <td style="text-align: center;">
                            <?php
                                $num = $filas['APLIC_DESC_PORT'];
                                if($num == 0){
                                    echo "Si";
                                }else{
                                    echo "No";
                                }
                            ?>
                            
                        </td>

                    </tr>
            <?php    				
			$i++;
				}						
            ?>
    </tbody>
  </table>
</div>							
<div class="row">
                                <div class="col-sm-4" style=""></div>
                                <div class="col-sm-4" align="center" style=""><input type="submit" name="activar" value="Activar" class="boton"><input type="button" name="volver" value="Volver" class="boton" onclick="history.back();" /></div>
                                <div class="col-sm-4" style=""></div>
                        
                            </div>							
						
                        
                    </form>
    
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
	
</body>
</html>