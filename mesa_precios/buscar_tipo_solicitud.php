<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Cotizador Web VP Empresas</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/estiloTraslado.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../JavaScript/pregunta.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../JavaScript/validarut.js"></script>
    <script src="../JavaScript/formatoNumero.js"></script>
    <script src="../JavaScript/validaciones.js"></script>       
</head>
<?php 
    session_start();
    require('../links.php');
    header("Content-Type: text/html;charset=utf-8");
    require_once('../conexion/conexion_bd.php');
    $consulta = "SELECT * FROM cow_mae_tiposolicitud WHERE ESTD_TIPOSOLICITUD='0'";
    $ejecutar = $mysqli->query($consulta);
    

    if(isset($_POST['activar'])){    
        $indice = $_POST['solicitud'];
        $consulta2 = "UPDATE cow_mae_tiposolicitud SET ESTD_TIPOSOLICITUD='0' WHERE CODI_TIPSOLICITUD='$indice'";
        $ejecutar2 = $mysqli->query($consulta2);
        header("location:buscar_tipo_solicitud.php"); 
    } 
    
?>
<body style="background:#E6E6E6">
	
	<header>		
		<div><img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/></div>			
		<span class="HeaderTitulo">Buscar Tipos de Solicitudes</span> 
		<span class="HeaderDerecha">V1.0<br></span>					
	</header>
    <br><br>       
    <form action="<?php echo 'tipo_solicitud.php' ?>" method="POST" name="form1" id="form1">
        <div class="row">              
            <div class="col-md-3"></div>            
            <div class="col-md-6 tabla" style="background: white">
                <h3 align="center">Lista de tipos de solicitudes:</h3>
                <br><br>
                <?php
                    $i=0;
                    $j=1;
                    while($filas = $ejecutar->fetch_array()){                                  
                ?>
                <div class="form-group">
                <?php if($i==0){ ?>
                    <label class="control-label col-sm-2" for="canal"></label>
                    <div class="col-sm-2">
                    <input type="radio" name="solicitud" id="solicitud" value="<?php echo $filas[0];?>" checked="checked">
                    </div>
                    <div class="col-sm-3"><?php echo $filas['NOMB_TIPOSOLICITUD'];?></div>
                    <?php }else{ ?>
                    <label class="control-label col-sm-2" for="canal"></label>
                    <div class="col-sm-2">
                        <input type="radio" name="solicitud" id="solicitud" value="<?php echo $filas[0];?>">
                    </div>
                    <div class="col-sm-3"><?php echo $filas['NOMB_TIPOSOLICITUD'];?> </div>
                    <?php } ?>
                </div>                        
                <?php 
                     $i++;
                    }
                ?>
            <br>
            </div>  
            <div class="col-md-3"></div>                   
        </div>
        <br>
        <table>
            <tr>
                <div class="row">
                    <div class="col-sm-4" style=""></div>
                    <div class="col-sm-4" align="center" style=""><input type="button" onclick=" location.href='ingresar_solicitud.php' " class="boton" value="Ingresar" name="boton" /> <input type="submit" name="eliminar" id="eliminar" value="Eliminar" class="boton"/><input type="submit" name="modificar" value="Modificar" class="boton"/><input type="button" onclick=" location.href='lista_eliminados_solicitud.php' " class="boton" value="Eliminados" name="boton" /><input type="button" name="volver" value="Volver" class="boton" onclick="location='index.php'" />
                    </div>
                    <div class="col-sm-4" style=""></div>
                </div>
            </tr>                
        </table>               
    </form>

    <table>
        <tr>
             <td height="242"></td>       
        </tr>                
    </table>                
                                          
	<footer>
		<?php require('../footer.php'); ?> 
	</footer>		

</body>
</html>