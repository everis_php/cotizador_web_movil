<?php
require_once ('portabilidad/funciones.php');
require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;
?>
<?php

//Recuperacion e asignacion de variables de Página Soporte
$infGral = [
    "Nombre" => "",
    "Rut"    => "",
    "F_Prop" => "",
    "V_Prop" => "",
    "Folio"  => "",
    "VLOR_PORCDESCFID" => ""
];

//_____________________________________________ QUERYS ___________________________________________--
$id_cotizacion  = $_GET['id_cotizacion'];
$cotizacion     = get_cotizacion_por_id($id_cotizacion);
$detalle_coti   = get_detallecotizacion_por_idcotizacion($id_cotizacion);
$detalle_coti2  = get_detallecotizacion_por_idcotizacion($id_cotizacion);

while($row = $cotizacion->fetch_array()) {

    $array_fec_propuesta             = explode(" ",$row['FECH_COTIZACION']);
    
    $infGral['Nombre']               = $row['NOMB_RAZONSOCIAL'];
    $infGral['Rut']                  = $row['NRUT_CLIENTE'];
    $infGral['F_Prop']               = $array_fec_propuesta[0];
    $infGral['Folio']                = $row['FOLIO'];
    $infGral['porcentaje_fideliza']  = $row['VLOR_PORCDESCFID'];
}

$fecha_actual             = date('Y-m-d');
$fecha_vigencia_propuesta = strtotime ( '+30 day' , strtotime ( $fecha_actual ) ) ;
$fecha_vigencia_propuesta = date ( 'Y-m-d' , $fecha_vigencia_propuesta );

$infGral['V_Prop']        = $fecha_vigencia_propuesta;

$DetPanesEC[0] = [
    "NomCuoTrafico"=>"Empresa Conectada 600 MB",
    "CuoTrafico"=>"1,2 GB",
    "Minutos"=>"200",
    "AppIlimitada"=>"-",
    "Cantidad"=>"5",
    "CarFijSIVA"=>"$ 8.395",
    "Descuento"=>"",
    "DesPromocion"=>"",
    "CFFinalSIVA"=>"",
];
$DetPanesEC[1]= 
[
    "NomCuoTrafico"=>"22222Empresa Conectada 600 MB",
    "CuoTrafico"=>"2221,2 GB",
    "Minutos"=>"222200",
    "AppIlimitada"=>"2-",
    "Cantidad"=>"25",
    "CarFijSIVA"=>"$222 8.395",
    "Descuento"=>"22",
    "DesPromocion"=>"222",
    "CFFinalSIVA"=>"222"
];     

$carFijoNetox18="$"."453.995";
$carFijoNetox19="$"."453.995";

$tblDecEnEquSIVA[0]=[
    "nomEquipo"=>"Samsung Galaxy J5 2016",
    "qEquipos"=>"5",    
    "valCouIni"=>"$"." 99.990",
    "descuento"=>"100"."%",
    "totAPagar"=>"$"." -", 
    "Plan"=>"Empresa Conectada PRO 30GB"
];
$tblDecEnEquSIVATotal="111";
//Se pasa toda la página a una variable PHP para despues desplegar el PDF

$contHTML='
<html>
  <body>
      <link rel="stylesheet" type="text/css" href="css/estilotablapdf.css">
    <table>
          <tr>
              <td colspan="2" align=center id="cab_prim_full">
                  INFORMACIÓN GENERAL
              </td>
              <td colspan="6"></td>
              <td rowspan="3"><img id="img_logo" src="Imagenes/Logo_Entel.png">
              </td>
          </tr>
          <tr>
              <td id="cab_sec">
              Nombre Cliente
              </td>
              <td NOWRAP>
              '?><?php 
                $contHTML.= $infGral["Nombre"] ?><?php 
                $contHTML.='
              </td>
              <td colspan="6"></td>
              </tr>
              <tr>
              <td id="cab_sec">
              RUT
              </td>
              <td NOWRAP>
              '?><?php 
                $contHTML.= $infGral["Rut"] ?><?php 
                $contHTML.='
              </td>
              <td colspan="6"></td>
              </tr>
              <tr>
              <td NOWRAP id="cab_sec">
              Fecha Propuesta
              </td>
              <td NOWRAP>
              '?><?php 
                $contHTML.= $infGral["F_Prop"] ?><?php 
                $contHTML.='
              </td>
              <td colspan="6"></td>
              <td >
              </td>
              </tr>
              <tr>
              <td id="cab_sec">
              Vigencia Propuesta      
              </td>
              <td NOWRAP>
            '?><?php 
                $contHTML.= $infGral["V_Prop"] ?><?php 
                $contHTML.='
              </td>
              <td colspan="6"></td>
              <td >
              </td>
              </tr>
              <tr>
              <td id="cab_sec">
              Folio
              </td>
              <td NOWRAP>
              '?><?php 
                $contHTML.= $infGral["Folio"] ?><?php 
                $contHTML.='
              </td>
              <td colspan="6"></td>
              <td >
              </td>
          </tr>
          <tr>
              <td colspan="9" align="center" id="cab_prim_full">
              DETALLE PLANES EMPRESA CONECTADA
              </td>
          </tr>
              <!-- Cabecera de la tabla Detalle planes empresa conectada -->  
          <tr>
              <td id="cab_sec" align="center">Nombre/Cuota de trafico</td>
              <td id="cab_sec" align="center">Tipo de línea</td>
              <td id="cab_sec" align="center">Cantidad</td>
              <td id="cab_sec" align="center">Cargo Fijo S/IVA</td>
              <td id="cab_sec" align="center">Fidelizacion</td>
              <td id="cab_sec" align="center">Extra</td>
              <td id="cab_sec" align="center">Portabilidad</td>
              <td id="cab_sec" align="center">Total</td>
              <td id="cab_sec" align="center">CF Final S/IVA</td>
          </tr>
             <!--Datos de las tablas -->
              '?> <?php 


                  $por_fidelizacion          = (int)$infGral['porcentaje_fideliza'];

                  $desc_portabilidad         = get_descuento_portabilidad();
                  $total_cargo_fijo_siniva   = 0;
                  $canti_linea_total         = 0;
                  $total_final_extra         = 0; 
                  $total_final_portabili     = 0;
                  $cfx18                     = 0;
                  $cfx19                     = 0;
                  $acumulado_total_descuento = 0;
                  $total_lin                 = 0;

                  while ( $row = $detalle_coti->fetch_array()) {



                            $id_plan                   = (int)$row['CODI_PLAN'];
                            $plan                      = get_nombre_plan($id_plan);
                            $desc_extra                = get_descuento_extra_plan($id_plan);

                            $tipoLinea                 = $row['NOMB_TIPOLINEA'];
                            $cant_linea                = (int)$row['CANT_LINEA'];

                            $total_lin                 = $total_lin + $cant_linea;

                            $cargo_fijo_coniva         = get_cargo_con_iva($id_plan);
                            $cargo_fijo_siniva         = get_cargo_sin_iva($id_plan); 
                            $VLOR_TOTALCFSINIVA        = $VLOR_TOTALCFSINIVA + $cargo_fijo_siniva;

                            $total_descuento           = $por_fidelizacion + $desc_extra + $desc_portabilidad;
                            $acumulado_total_descuento = $acumulado_total_descuento + $total_descuento;
                            $total_final_fila          = $cargo_fijo_siniva * $cant_linea * (1 - ($total_descuento / 100));

                            $total_cargo_fijo_siniva   = $total_cargo_fijo_siniva + ($cargo_fijo_siniva * $cant_linea); //(columna)


                            $total_final_extra         = $total_final_extra + ($cargo_fijo_siniva * $cant_linea * ($desc_extra / 100));
                            $total_final_portabili     = $total_final_portabili + ($cargo_fijo_siniva * $cant_linea * ($desc_portabilidad / 100));

                            $cfx18                     = $cfx18 + $total_final_fila;


                          //for ($i = 0; $i < (count($DetPanesEC)); $i++) {
                            $contHTML.='<tr>';
                                $contHTML.='<td align="center">';
                                $contHTML.= $plan;
                                $contHTML.='</td>';
                                $contHTML.='<td align="center">';
                                $contHTML.= utf8_encode($tipoLinea);
                                $contHTML.='</td>';
                                $contHTML.='<td align="center">';
                                $contHTML.= $cant_linea;
                                $contHTML.='</td>';
                                $contHTML.='<td align="center">';
                                $contHTML.= number_format(round($cargo_fijo_siniva), 0, '', '.');
                                $contHTML.='</td>';
                                $contHTML.='<td align="center">';
                                $contHTML.= $por_fidelizacion;
                                $contHTML.='%</td>';
                                $contHTML.='<td align="center">';
                                $contHTML.= $desc_extra;
                                $contHTML.='%</td>';
                                $contHTML.='<td align="center">';
                                $contHTML.= $desc_portabilidad;
                                $contHTML.='%</td>';
                                $contHTML.='<td align="center">';
                                $contHTML.= $total_descuento;
                                $contHTML.='%</td>';
                                $contHTML.='<td align="center">';
                                $contHTML.=number_format(round($total_final_fila), 0, '', '.');
                                $contHTML.='</td>';
                            $contHTML.='</tr>';
                }



                $total_fidelizacion             = ($total_cargo_fijo_siniva * $por_fidelizacion) / 100;
                $cfx19                          = $total_cargo_fijo_siniva + $total_fidelizacion + $total_final_extra;

          $contHTML.='<tr>';
              $contHTML.='<td align="center">';
              $contHTML.= '';
              $contHTML.='</td>';
              $contHTML.='<td align="center">';
              $contHTML.= 'TOTAL LINEAS';
              $contHTML.='</td>';
              $contHTML.='<td align="center">';
              $contHTML.= $total_lin;
              $contHTML.='</td>';
              $contHTML.='<td align="center">';
              $contHTML.= " ". number_format(round($total_cargo_fijo_siniva), 0, '', '.');
              $contHTML.='</td>';
              $contHTML.='<td align="center">';
              $contHTML.= number_format($total_fidelizacion, 0, '', '.');
              $contHTML.='</td>';
              $contHTML.='<td align="center">';
              $contHTML.= number_format($total_final_extra, 0, '', '.');
              $contHTML.='</td>';
              $contHTML.='<td align="center">';
              $contHTML.= "- ". number_format($total_final_portabili, 0, '', '.');
              $contHTML.='</td>';
              $contHTML.='<td align="center">';
              $contHTML.= '';
              $contHTML.='</td>';
              $contHTML.='<td align="center">';
              $contHTML.= '';
              $contHTML.='</td>';
      $contHTML.='</tr>';




            ?><?php 
              $contHTML.='
          <!-- Informacion cargo fijo neto 18 y desde 19 meses -->  
          <tr>
              <td colspan="6"></td>
              <td colspan="2" id=cab_terc>Cargo fijo neto por 18 meses</td>
              <td id="cab_terc_det"> '?><?php 
              $contHTML.= "$ ".number_format($cfx18, 0, '', '.') ?><?php 
              $contHTML.='</td>
          </tr>
          <tr>
              <td colspan="6"></td>
              <td colspan="2" id=cab_terc>Cargo fijo neto desde mes 19</td>
              <td id="cab_terc_det">'?><?php 
              $contHTML.= "$ ".number_format($cfx19, 0, '', '.') ?><?php 
              $contHTML.='</td>
          </tr>      
           <tr>
              <td colspan="9"> </td>
          </tr>
          <tr>
              <td colspan="9"> </td>
          </tr> 
          <tr>
              <td colspan="9">* Descuento válido por 18 meses, aplica sólo para portabilidades con cargo fijo desde $16.990 iva incluido.</td>
          </tr>
          <tr>
              <td colspan="9"> </td>
          </tr>
          <tr>
              <td colspan="9"> </td>
          </tr>
          <!-- Tabla DESCUENTO EN EQUIPOS (Valores Sin iva) -->
          <tr align="center">
              <td colspan="9" id="cab_prim_full">
              Tabla DESCUENTO EN EQUIPOS (Valores Sin iva) 
              </td>       
          </tr>
          <tr>
          <!-- Cabecera tabla-->
              <td id="cab_sec" align="center">Plan</td>
              <td id="cab_sec" align="center">Modelo equipo</td>
              <td id="cab_sec" align="center">Q EQUIPOS</td>
              <td id="cab_sec" align="center">Valor Cuota Inicial</td>
              <td id="cab_sec" align="center">Valor en Plan</td>
              <td id="cab_sec" align="center">% Descuento</td>
              <td id="cab_sec" align="center">Total Descto Aprobado</td>
              <td id="cab_sec" align="center">Total a Pagar</td>
              <td id="cab_sec" align="center">Código Descuento</td>
          </tr>
          <!-- Detalle tabla -->
           '?> <?php 


                    
                    //for ($i = 0; $i < (count($tblDecEnEquSIVA)); $i++) {
                    while ($row2 = $detalle_coti2->fetch_array()) {


                        $cant_equipo               = (int)$row2['CANT_LINEA'];
                        $nombre_plan               = '';
                        $nombre_modelo_equipo      = '';
                        $valor_cuota_inicial;
                        $valor_en_plan;
                        $descuento                 = 0;
                        $alineado; //Tabla cow_mae_alineados
                        $codigo_desc               = '';
                        $tipoLinea                 = '';
                        $total_a_pagar             = 0;

                      if($cant_equipo > 0){

                          $id_equipo                 = (int)$row2['CODI_EQUIPO'];
                          $id_plan                   = (int)$row2['CODI_PLAN'];

                          $nombre_plan               = get_nombre_plan($id_plan);
                          $nombre_modelo_equipo      = get_nombre_modelo_equipo($id_equipo);
                          $valor_cuota_inicial       = get_valor_cuota_inicial_equipoplan($id_plan,$id_equipo);
                          $valor_en_plan             = get_valor_en_plan_equipoplan($id_plan,$id_equipo);
                          $descuento                 = get_porcentaje_descuento_equipoplan($id_plan,$id_equipo);
                          $alineado                  = get_existencia_equipo_alineado($id_equipo);

                          $total_valor_cuota_inicial = $total_valor_cuota_inicial + ($valor_cuota_inicial * $cant_equipo);
                          $total_valor_en_plan       = $total_valor_en_plan + ($valor_en_plan * $cant_equipo);
                          $total_cantidad_equipos    = $total_cantidad_equipos + $cant_equipo;


                          $total_a_pagar              = $valor_en_plan * (1 - ($descuento/100)) * $cant_equipo;
                          $total_descuento            = ($valor_en_plan * $cant_equipo / 1.19) - $total_a_pagar * 1.19;

                          $total_descuento_aprobado   = $total_descuento_aprobado + $total_descuento;


                          if($alineado){
                            $total_a_pagar = 0;
                            $codigo_desc   = '296 Alineados';
                          }
                          else{
                              $codigo_desc   = '320 Mesa EV';
                          }

                          $total_final_a_pagar        = $total_final_a_pagar + $total_a_pagar;


                        $contHTML.='<tr>';
                              $contHTML.='<td align="center">';
                              $contHTML.= $nombre_plan;
                              $contHTML.='</td>';
                              $contHTML.='<td align="center">';
                              $contHTML.= $nombre_modelo_equipo;
                              $contHTML.='</td>';
                              $contHTML.='<td align="center">';
                              $contHTML.= $cant_equipo;
                              $contHTML.='</td>';
                              $contHTML.='<td align="center">';
                              $contHTML.= number_format(round($valor_cuota_inicial), 0, '', '.');
                              $contHTML.='</td>';
                              $contHTML.='<td align="center">';
                              $contHTML.= number_format(round($valor_en_plan), 0, '', '.');
                              $contHTML.='</td>';
                              $contHTML.='<td nowrap align="center">';
                              $contHTML.= $descuento;
                              $contHTML.='</td>';
                              $contHTML.='<td nowrap align="center">';
                              $contHTML.= number_format(round($total_descuento), 0, '', '.');
                              $contHTML.='</td>';
                              $contHTML.='<td nowrap align="center">';
                              $contHTML.= number_format(round($total_a_pagar), 0, '', '.');
                              $contHTML.='</td>';
                              $contHTML.='<td nowrap align="center">';
                              $contHTML.= $codigo_desc;
                              $contHTML.='</td>';
                         $contHTML.='</tr>';
                       }
                  } // Cierre While


                  $contHTML.='<tr>';
                        $contHTML.='<td align="center">';
                        $contHTML.= "";
                        $contHTML.='</td>';
                        $contHTML.='<td align="center">';
                        $contHTML.= "TOTAL";
                        $contHTML.='</td>';
                        $contHTML.='<td align="center">';
                        $contHTML.= $total_cantidad_equipos;
                        $contHTML.='</td>';
                        $contHTML.='<td align="center">';
                        $contHTML.= number_format(round($total_valor_cuota_inicial), 0, '', '.');
                        $contHTML.='</td>';
                        $contHTML.='<td align="center">';
                        $contHTML.= number_format(round($total_valor_en_plan), 0, '', '.');
                        $contHTML.='</td>';
                        $contHTML.='<td nowrap align="center">';
                        $contHTML.= "";
                        $contHTML.='</td>';
                        $contHTML.='<td nowrap align="center">';
                        $contHTML.= number_format(round($total_descuento_aprobado), 0, '', '.');
                        $contHTML.='</td>';
                        $contHTML.='<td nowrap align="center">';
                        $contHTML.= number_format(round($total_final_a_pagar), 0, '', '.');
                        $contHTML.='</td>';
                        $contHTML.='<td nowrap align="center">';
                        $contHTML.= "";
                        $contHTML.='</td>';
                   $contHTML.='</tr>';


                      
            ?><?php 
              $contHTML.='
        <!-- totales tabla -->
          <tr>
           
          </tr>   
    </table>

    <br> <br>

    <p class="page_break">
      
        <ul style="color: #366097;">
            <li> Los planes con datos ilimitados incluyen navegación ilimitada para uso libre dentro del territorio nacional. El uso de estos datos, la calidad de visualización de videos o de reproducción de música, corresponderá a la red disponible a la que acceda el cliente, a la calidad propia del contenido y las características técnicas del equipo utilizado. </li>
            <li> Puedes llevar el control de tu consumo de datos desde tu celular ingresando a App Entel, Mi Entel o marcando el código *116# (consulta gratis).</li>
            <li>Una vez finalizado el mes calendario los servicios del plan se reiniciarán a contar del primer día del mes siguiente. </li>
            <li>Los planes con minutos ilimitados incluyen llamadas de voz originadas y terminadas dentro de Chile en una cantidad ilimitada de minutos hasta 300 números distintos de destino en un mes o hasta alcanzar la cantidad de 1.000 minutos en llamadas a un mismo número móvil en un mes. Una vez excedidos estos 300 números o la cantidad de minutos referida, las llamadas a números distintos de los primeros 300 o en exceso de dichos minutos, comenzarán a cobrarse a $99 el minuto. </li>
            <li> Los planes con SMS ilimitados incluyen el envío SMS originadas y terminadas dentro de Chile en una cantidad ilimitada de mensajes hasta 300 números distintos de destino en un mes o hasta alcanzar la cantidad de 1.000 mensajes a un mismo número móvil en un mes. Una vez excedida la cantidad de mensajes enviados, ya sea a 300 números distintos de destino o enviados al mismo número móvil de destino, los mensajes comenzarán a cobrarse a $99 cada uno.</li>
            <li> Los SMS incluidos en el plan no aplican para concursos, juegos u otros servicios especiales, los cuales tienen sus propias condiciones comerciales.</li>
            <li> Los planes con datos ilimitados estarán sujetos a las políticas de fraude y administración de tráfico que Entel aplica a sus planes. </li>
            <li> Una vez consumida la cuota de datos del plan se corta la navegación. Para continuar navegando, el Administrador podrá entregar permisos de navegación adicional a los móviles que desee (en Data manager), a cargo del administrador según valor del MB adicional de cada plan. Adicionalmente, el usuario puede contratar bolsas de navegación adicionales, con cargo a su cuenta personal (accede desde portal Mi Entel, en web Personas).</li>
            <li> Todos los planes Empresa Conectada incluyen llamadas ilimitadas entre los números móviles de la misma cuenta (Intra-cuenta).</li>
            <li> El descuento de fidelización dependerá de la contratación total de las líneas con planes Empresa Conectada, por lo que puede verse modificado ante nuevas contrataciones, cambios de los planes contratados y términos de contratos. </li>
            <li> Los planes Empresa Conectada Pro y Empresa Conectada SIM desde cargo fijo $13.990 cuentan con una cuota de navegación adicional para correo y WhatsApp sin corte. La cuota adicional de tráfico mensual es de 20GB solo para uso de correo y WhatsApp, y no para otras aplicaciones aun cuando se acceda a ellas a través de alguna de éstas. Esta cuota se aplica una vez que el cliente consume tanto la cuota de tráfico mensual del plan como los servicios de internet que contrate en forma adicional y aplica sólo para el uso de los protocolos de correo que se detallan en el punto siguiente. Consumida la cuota adicional, la velocidad máxima que se suministrará a dicho móvil en lo que resta del mes calendario será de hasta 32 kbps (0,03125 mbps) por segundo para la descarga y para la subida de correos y WhatsApp.</li>
            <li>La cuota adicional destinada a WhatsApp no considera llamadas y video llamadas por medio de la aplicación o cualquier otro enlace que navega fuera de la aplicación. </li>
            <li> Los protocolos de correo que incluye la cuota adicional son Exchange 5.5 o superior + Office 365; aplicaciones: Android e Ios, Windows (Outlook 2010 o superior) y Osx (Outlook 2010 o superior y cliente nativo mac); Pop3 con y sin SSL; Smtp con y sin Ssl; imap4 con y sin ssl;; outlook.com hotmail.com , aplicaciones: Android e ios, Windows y osx; yahoo.com, aplicaciones: android e ios; gmail.com (personal y pagado) , aplicaciones: android e ios. En el caso del protocolo de correo Exchange el cliente debe instalar en el terminal la aplicación Microsoft Outlook (Microsoft Corporation) disponible en tiendas de aplicaciones de los terminales móviles, en la cual configurará su cuenta de correo. Esa aplicación debe usarse para recibir, enviar y redactar correos.</li>
            <li> Las cuotas de datos no son acumulables para el mes siguiente en caso de no ser utilizada y se renueva al inicio de cada mes.</li>
            <li>Planes SIM no acceden a equipos con contrato de arriendo con opción de compra, cliente podrá acceder a un equipo comprándolo directamente. </li>
        </ul>
    </p>

  </body>
</html>

'
?>
<?php

$contHTML; 
// instantiate and use the dompdf class


$dompdf = new Dompdf();

$dompdf->loadHtml($contHTML,'UTF-8');

// (Optional) Setup the paper size and orientation
//$dompdf->setPaper('A4','landscape');
$customPaper = array(0,0,1170,900);
$dompdf->set_paper($customPaper);

// Render the HTML as PDF
$dompdf->render();

$pdf=$dompdf->output();
// Output the generated PDF to Browser
$fechaCompleta=date("Ymd_Hms");

$nameDocto = "Cotizacion_".$fechaCompleta.".pdf";
//$dompdf->stream($nameDocto);
// Para mostrar el PDF en el navegador sin necesidad de descargar

$dompdf->stream($nameDocto, array("Attachment" => false));
exit(0);

?>