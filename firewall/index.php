<?php session_start();
	
	require_once("seguridad.php");
	require_once("../header.php");
?> 
<body>
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Cotizador de Firewall</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
			<a href=<?php echo $URL_home_pricipal; ?>>Volver</a>
		</span>
 
	</header>
	<section class="main">
		<article class="panelIzq">
			<?php

			require_once('conexion.php');
			mysql_select_db($base,$conexion);

			$rut_ejecutivo =null;
			$nombre_ejecutivo=null;
			$segmento_cliente=null;
			$rut_cliente=null;
			$nombre_cliente=null;
			$tipo_firewall=null;
			$numero_oft=null; 

			if (isset($_POST['calcular'])) {
				$rut_ejecutivo =$_POST['rut_ejecutivo'];
				$nombre_ejecutivo=$_POST['nombre_ejecutivo'];
				$segmento_cliente=$_POST['segmento_cliente'];
				$rut_cliente=$_POST['rut_cliente'];
				$nombre_cliente=$_POST['nombre_cliente'];
				$tipo_firewall=$_POST['tipo_firewall'];
				$numero_oft=$_POST['numero_oft']; 
								
			}

			if (isset($_POST['recotizar'])) {
				$rut_ejecutivo =null;
				$nombre_ejecutivo=null;
				$segmento_cliente=null;
				$rut_cliente=null;
				$nombre_cliente=null;
				$tipo_firewall=null;
				$numero_oft=null;			
			}

			if (isset($_POST['limpiar'])) {
				$rut_ejecutivo =null;
				$nombre_ejecutivo=null;
				$segmento_cliente=null;
				$rut_cliente=null;
				$nombre_cliente=null;
				$tipo_firewall=null;
				$numero_oft=null;				
			}

			function cadenaAleatoria($length = 10) 
				{ 
	    		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length); 
	    		}

			?>
			<h3>Favor ingrese los siguientes datos:</h3>
			<form action="index.php" method="POST" name="form">
						<br>
						<table >
							<tr>
								<td>Rut Ejecutivo:	</td>
								<td><input type="text" name="rut_ejecutivo"  onchange="Valida_Rut(this)" value="<?php echo $rut_ejecutivo; ?>"></input> (Ej: 99999999-9)</td>
							</tr>
							<tr>
								<td>Nombre Ejecutivo:</td>
								<td><input type="text" name="nombre_ejecutivo" value="<?php echo $nombre_ejecutivo; ?>"></input></td>
							</tr>
							<tr><td> <br> </td></tr>
							<tr>
								<?php 

								$consulta_segmento_cliente= "SELECT * FROM SEGMENTO_CLIENTE";
								mysql_query("SET NAMES 'utf8'");
								$ejecutar_segmento_cliente=mysql_query($consulta_segmento_cliente,$conexion) or die(mysql_error());
								$filas_segmento_cliente=mysql_fetch_assoc($ejecutar_segmento_cliente);
								$numero_segmento_cliente=mysql_num_rows($ejecutar_segmento_cliente);
								
								?>

								<td>Segmento Cliente:</td>
								<td><select name="segmento_cliente">
									<option>Ingrese Segmento Cliente</option>
									<?php do{?>
									<option <?php if ($segmento_cliente==$filas_segmento_cliente['nombre_segmento']) { echo "selected"; }?>><?php echo $filas_segmento_cliente['nombre_segmento']?></option>
									<?php } while ($filas_segmento_cliente= mysql_fetch_assoc($ejecutar_segmento_cliente));?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Rut Cliente:</td>
								<td><input type="text" name="rut_cliente" onchange="Valida_Rut(this)" value="<?php echo $rut_cliente; ?>"></input> (Ej: 99999999-9)</td>
							</tr>
							<tr>
								<td>Nombre Cliente:</td>
								<td><input type="text" name="nombre_cliente" value="<?php echo $nombre_cliente; ?>"></input></td>
							</tr>
							<tr><td> <br> </td></tr>
							<tr>
								<td>Número OFT:</td>
								<td><input type="text" name="numero_oft" maxlength ="11" onchange="soloNumero(this)" value="<?php echo $numero_oft; ?>"></input></td>
							</tr>
							<tr>
								<?php 

								$consulta_tipo_producto= "SELECT * FROM firewall where firewall.activo='1'";
								mysql_query("SET NAMES 'utf8'");
								$ejecutar_tipo_producto=mysql_query($consulta_tipo_producto,$conexion) or die(mysql_error());
								$filas_tipo_producto=mysql_fetch_assoc($ejecutar_tipo_producto);
								$numero_tipo_producto=mysql_num_rows($ejecutar_tipo_producto);
								
								?>

								<td>Ingrese Firewall sugerido en OFT:</td>
								<td><select name="tipo_firewall">
									<option>Ingrese Firewall</option>
									<?php do{?>
									<option value="<?php echo $filas_tipo_producto['id']; ?>" <?php if ($tipo_firewall==$filas_tipo_producto['id']) { echo "selected"; }?>>1 <?php echo $filas_tipo_producto['nombre']?></option>
									<?php } while ($filas_tipo_producto= mysql_fetch_assoc($ejecutar_tipo_producto));?>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="left">
									<br>(*)Si la cantidad o el modelo de Firewall no aparece en la lista, debes realizar la cotización vía Mesa de Precios
								</td>
							</tr>
							<table><tr>
								<td><br><input type="submit" name="calcular" value="Calcular" class="boton"></td>
								<td><br><input type="submit" name="limpiar" value="Limpiar" class="boton"></td>
                                                                <td> <br><input type="submit" name="volver" value="Volver" class="boton" onClick="document.form.action='<?php echo $URL_home_pricipal; ?>'; document.form.submit();"></td>
							</tr></table>
						</table>
		</article>
		<article class="panelDer">
			<?php
			if (isset($_POST['calcular'])){ 

				if ($_POST['rut_ejecutivo'] != "" and $_POST['nombre_ejecutivo'] != "" and $_POST['segmento_cliente'] != "Ingrese Segmento Cliente" and $_POST['rut_cliente'] != "" and 
					$_POST['nombre_cliente'] != "" and $_POST['tipo_firewall'] != "Ingrese Firewall" and $_POST['numero_oft'] != ""){
				
				$rut_ejecutivo = $_POST['rut_ejecutivo'];
				$nombre_ejecutivo = $_POST['nombre_ejecutivo'];
				$segmento_cliente = $_POST['segmento_cliente'];
				$rut_cliente = $_POST['rut_cliente'];
				$nombre_cliente = $_POST['nombre_cliente'];
				$numero_oft = $_POST['numero_oft']; 
				$tipo_firewall = $_POST['tipo_firewall'];

				$consulta_parametros= "SELECT * FROM precio_equipo WHERE id_firewall = '$tipo_firewall' and vigente='1'";
				mysql_query("SET NAMES 'utf8'");
				$ejecutar_parametros=mysql_query($consulta_parametros,$conexion) or die(mysql_error());
				$filas_precio=mysql_fetch_assoc($ejecutar_parametros);

				$id_precio = $filas_precio['id'];

				

				$consulta_parametros= "SELECT * FROM parametros WHERE id = (SELECT MAX( id ) FROM parametros )";
				mysql_query("SET NAMES 'utf8'");
				$ejecutar_parametros=mysql_query($consulta_parametros,$conexion) or die(mysql_error());
				$filas_parametros=mysql_fetch_assoc($ejecutar_parametros);

				$id_parametro = $filas_parametros['id'];
				
				$codigo_identificador = cadenaAleatoria().$numero_oft;
				
				$fecha_traslado = date("Y-m-d");

					$insertar_traslado = "INSERT INTO cotizacion_firewall ( id, rut_ejecutivo, nombre_ejecutivo, segmento_cliente, rut_cliente, 
						nombre_cliente, nro_oft, id_precio, id_parametro, fecha,codigo_identificador, ingresado, ingresado_precio, ingresado_mantencion, usv_contrato, fecha_ingreso) 
					VALUES('', '$rut_ejecutivo', '$nombre_ejecutivo', '$segmento_cliente', '$rut_cliente', 
						'$nombre_cliente', '$numero_oft', '$id_precio', '$id_parametro', '$fecha_traslado','$codigo_identificador', '', '', '', '', '')";
				
					mysql_query("SET NAMES 'utf8'");
					mysql_query("LOCK TABLES COTIZACION_FIREWALL WRITE",$conexion);
					$ejecuta_traslado = mysql_query($insertar_traslado, $conexion) or die(mysql_error());
					$ultimo_id = mysql_insert_id($conexion);
					mysql_query("UNLOCK TABLES",$conexion);


					$consulta_firewall= "SELECT * FROM cotizacion_firewall, precio_equipo, firewall WHERE 
										cotizacion_firewall.id_precio=precio_equipo.id and firewall.id=precio_equipo.id_firewall 
										and cotizacion_firewall.id='$ultimo_id'";
					mysql_query("SET NAMES 'utf8'");
					$ejecutar_firewall=mysql_query($consulta_firewall,$conexion) or die(mysql_error());
					$filas_firewall=mysql_fetch_assoc($ejecutar_firewall);

			?>
								<h3>Tarifas por Firewall</h3>
								<table>
									<tr><td><br></td></tr>
									<tr>
										<td colspan="3" align="left">
											- Según OFT <?php echo $filas_firewall['nro_oft'];?> , las tarifas por Firewall son las siguientes:
										</td>
									</tr>
									<tr><td><br>Renta por <?php echo $filas_firewall['nombre'];?>:</td></tr>
									<tr>
										<td>12 Cuotas de:</td>
										<td align="right"><?php echo $filas_firewall['precio_12']; ?></td>
										<td>UF/Mes + IVA</td>
									</tr>
									<tr>
										<td>24 Cuota de:</td>
										<td align="right"><?php echo $filas_firewall['precio_24']; ?></td>
										<td>UF/Mes + IVA</td>
									</tr>
									<tr>
										<td>36 Cuota de:</td>
										<td align="right"><?php echo $filas_firewall['precio_36']; ?></td>
										<td>UF/Mes + IVA</td>
									</tr>
									<tr><td><br></td></tr>
									<tr><td>Renta por administración (Opcional):</td></tr>
									<tr>
										<td></td>
										<td colspan="2" align="center"><strong>Tarifa Normal</strong></td>
										<td>&nbsp;&nbsp;</td>
										<td colspan="2" align="center"><strong>Tarifa con Dscto</strong></td>
									</tr>
									<tr>
										<td>Administración Básica:</td>
										<td align="right"><?php echo $filas_firewall['mantencion_basica']; ?></td>
										<td>UF/Mes + IVA</td>
										<td></td>
										<td align="right"><?php echo round($filas_firewall['mantencion_basica']*.9,2); ?></td>
										<td>UF/Mes + IVA</td>
									</tr>
									<tr>
										<td>Administración Full:</td>
										<td align="right"><?php echo $filas_firewall['mantencion_full']; ?></td>
										<td>UF/Mes + IVA</td>
										<td></td>
										<td align="right"><?php echo round($filas_firewall['mantencion_full']*.9,2); ?></td>
										<td>UF/Mes + IVA</td>
									</tr>
								</table>
								<table>
									
									<tr>
										<td>- Código Identificador:</td>
										<td><?php echo $ultimo_id; ?></td>
									</tr>
									<tr>
										<td Colspan = "2">
											(Favor enviar este código a Soporte Comercial en caso de que Cliente acepte esta propuesta)
										</td>
									</tr>
									<tr><br></tr>
									<tr><br></tr>
									<tr>
										<td colspan="2">
											(*) Esta oferta es válida mientras OFT se encuentre vigente.
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<a href="propuestas/<?php echo $filas_firewall['archivo_propuesta_normal'];?>" onclick="archivo()">Descargar Propuesta a Cliente Normal</a>
											&nbsp;&nbsp;&nbsp;
											<a href="propuestas/<?php echo $filas_firewall['archivo_propuesta_descuento'];?>" onclick="archivo()">Descargar Propuesta a Cliente con Descuento</a>
										</td>
									</tr>
									<tr>
										<td align="center"><br><input type="submit" name="recotizar" value="Realizar Otra Cotización" class="boton">
										</td>
									</tr>
								</table>
								</section >
							</td>
							<?php 
							} else{
								echo "<h3>Para poder realizar la cotización, debe ingresar los campos solicitados:</h3> <br>";
								if ($_POST['rut_ejecutivo']==""){echo "-Falta Ingresar Rut Ejecutivo <br><br>";}
								if ($_POST['nombre_ejecutivo']==""){echo "-Falta Ingresar Nombre Ejecutivo <br><br>";}
								if ($_POST['segmento_cliente']=="Ingrese Segmento Cliente"){echo "-Debe escoger Segmento del Cliente <br><br>";}
								if ($_POST['rut_cliente']==""){echo "-Falta Ingresar Rut Cliente <br><br>";}
								if ($_POST['nombre_cliente']==""){echo "-Falta Ingresar Nombre Cliente <br><br>";}
								if ($_POST['numero_oft']==""){echo "-Falta Ingresar Número de OFT <br><br>";}
								if ($_POST['tipo_firewall']=="Ingrese Firewall"){echo "-Debe escoger Firewall señalado en OFT <br><br>";}
								
								}
			}
							?> 
						</tr>
					</table>
					</form>
		</article>
	</section>
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>