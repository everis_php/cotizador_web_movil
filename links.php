<?php 
	session_start();

//Seteo Ubicación
$IP_adress = "http://".$_SERVER['HTTP_HOST']."/";
//$IP_adress = "localhost";
$prefijo = "";
$prefijo_bd = "";

//Logos
$URL_logo_Entel = "../Imagenes/Isotipo_Fondo_Azul.jpg";
$URL_logo_mis_casos = "../Imagenes/Internet.jpg";
$URL_logo_ID = "../Imagenes/fibra-optica.jpg";
$URL_logo_traslados = "../Imagenes/tecnologia.jpg";
$URL_logo_firewall = "../Imagenes/firewall.jpg";
$URL_logo_portabilidad = "../Imagenes/portabilidad.jpg";
$URL_logo_mesa_precio = "../Imagenes/mesa_precio.jpg";
$URL_logo_casos_portabilidad = "../Imagenes/casos_portabilidad.jpg";
$URL_logo_busqueda = "../Imagenes/casos_portabilidad.jpg";

//Canal de Ventas
//Principal
$URL_mis_casos = $IP_adress.$prefijo."entel/mis_casos/index.php?token=".$_COOKIE["tokens"];
$URL_internet_dedicado = $IP_adress.$prefijo."entel/internetdedicado/index.php?token=".$_COOKIE["tokens"];
$URL_traslados = $IP_adress.$prefijo."entel/traslados/index.php?token=".$_COOKIE["tokens"];
$URL_firewall = $IP_adress.$prefijo."entel/firewall/index.php?token=".$_COOKIE["tokens"];
$URL_portabilidad = $IP_adress.$prefijo."entel/portabilidad/index.php?token=".$_COOKIE["tokens"];
$URL_mesa_precios = $IP_adress.$prefijo."entel/mesa_precios/index.php?token=".$_COOKIE["tokens"];
$URL_casos_portabilidad = $IP_adress.$prefijo."entel/casos_portabilidad/index.php?token=".$_COOKIE["tokens"];
$URL_busqueda = $IP_adress.$prefijo."entel/cotizadorwebempresas/busqueda.php?token=".$_COOKIE["tokens"];

//Volver al Principal
$URL_home_pricipal = "../cotizadorwebempresas/index.php?token=".$_COOKIE["tokens"];
$URL_home_pricipal_soporte = "../soportecotizadorwebempresas/soporte.php?token=".$_COOKIE["tokens"];
$URL_home_pricipal_mesa = "../mesacotizadorwebempresas/soporte.php?token=".$_COOKIE["tokens"];

//Cerrar Sesion
$URL_cerrar_sesion_soporte = "../cerrarSesion.php";
$URL_cerrar_sesion_mantenedor = "../cerrarSesion.php";
$URL_cerrar_sesion_mesa = "../cerrarSesion.php";

//Soporte
//Principal
$URL_internet_dedicado_soporte = $IP_adress.$prefijo."internetdedicadosoporte/index.php?token=".$_COOKIE["tokens"];
$URL_traslados_soporte = $IP_adress.$prefijo."trasladossoporte/";
$URL_firewall_soporte = $IP_adress.$prefijo."firewallsoporte/";
$URL_internet_dedicado_RUT = $IP_adress.$prefijo."internetdedicadosoporte/busqueda_rut.php?token=".$_COOKIE["tokens"];
$URL_traslados_RUT = $IP_adress.$prefijo."trasladossoporte/busqueda_rut.php?token=".$_COOKIE["tokens"];
$URL_firewall_RUT = $IP_adress.$prefijo."firewallsoporte/busqueda_rut.php?token=".$_COOKIE["tokens"];
$URL_movil_RUT = $IP_adress.$prefijo."consultamovilsoporte/busqueda_rut.php?token=".$_COOKIE["tokens"];
$URL_movil_folio = $IP_adress.$prefijo."consultamovilsoporte/";
$URL_mesa_RUT = $IP_adress.$prefijo."consultamesa/busqueda_rut.php?token=".$_COOKIE["tokens"];
$URL_mesa_folio = $IP_adress.$prefijo."consultamesa/index.php?token=".$_COOKIE["tokens"];

//FirewallSoporte
$URL_cotizaciones_fw_soporte = $IP_adress.$prefijo."firewall/cotizaciones/";
$URL_propuestas_fw_soporte = $IP_adress.$prefijo."firewall/propuestas/";
?>