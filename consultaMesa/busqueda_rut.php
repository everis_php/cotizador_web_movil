<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title>Consulta Evaluación Móvil - Soporte</title>
 
        <link rel="stylesheet" href="CSS/estiloTraslado.css">
        <SCRIPT SRC="JavaScript/formatoNumero.js"></SCRIPT>
        <SCRIPT SRC="JavaScript/pregunta.js"></SCRIPT>
        <?php 
        require('../links.php');
        header("Content-Type: text/html;charset=utf-8");
         ?>
 
</head>
 
<body>
	
	<header>
		
		<div>
			<img src=<?php echo $URL_logo_Entel; ?> alt="Logo" width="85"/>
			
		</div> <!-- / #logo-header -->
		<span class="HeaderTitulo">Consulta Evaluación Móvil - Soporte</span> 
		<span class="HeaderDerecha"> 
			V1.0<br>
			<a href=<?php echo $URL_cerrar_sesion_mesa; ?>>Cerrar Sesion</a>
		</span>
 
	</header>
	<section class="main">
		<?php
			session_start();
			if ($_SESSION["session_username"]=="") { 

				header("location:".$URL_cerrar_sesion_mesa);


			}else{

				require_once('conexionUsuario.php');
				mysql_select_db($base,$conexion);


				$rut = $_SESSION["session_username"];
				$consulta= "SELECT * FROM usuario WHERE rut='$rut'";
				mysql_query("SET NAMES 'utf8'");
				$ejecutar=mysql_query($consulta,$conexion) or die(mysql_error());
				$filas=mysql_fetch_assoc($ejecutar);

				?>
		<article class="panelIzq">
			<?php

			function amoneda($numero){  
					return $numero;
					}  

			require_once('conexion.php');
			mysql_select_db($base,$conexion);

			$codigo_identificador =null;
			

			if (isset($_POST['buscar'])) {
				$codigo_identificador =$_POST['codigo_identificador'];
			}

			if (isset($_POST['recotizar'])) {
				$codigo_identificador ="";
			}

			?>
			<h3>Hola <?php echo $filas['nombre']." ".$filas['apellido']."," ?></h3>
			<br>
			<h3>Favor ingrese los siguientes datos:</h3>
			<form action="busqueda_rut.php" method="POST" name="form1" id="form1">
						<br>
						<table >
							<tr>
								<td>RUT Cliente:	</td>
								<td><input type="text" name="codigo_identificador"  value="<?php echo $codigo_identificador; ?>"></input></td>
							</tr>
							<tr>
								<td align="right"><br><input type="submit" name="buscar" value="Buscar" class="boton"></td>
								<td align="left"><br><input type="submit" name="volver" value="Volver" class="boton"></td>
								<input type="hidden" name="key" id="key"></input>
							</tr>
							<tr><td>&nbsp;&nbsp;</td></tr>

							
							
						</table>
		</article>
		<article class="panelDer">
			<?php 

							if (isset($_POST['buscar'])){ 

								$consulta_codigo= "SELECT * FROM cliente, propuesta WHERE cliente.rut_cliente=propuesta.rut_cliente and propuesta.rut_cliente='$codigo_identificador' order by propuesta.fecha_respuesta desc";
								mysql_query("SET NAMES 'utf8'");
								$ejecutar_codigo=mysql_query($consulta_codigo,$conexion) or die(mysql_error());
								$filas_codigo=mysql_fetch_assoc($ejecutar_codigo);

								if ($filas_codigo!=0) {
									?>
									<h2>Listado de Cotizaciones Cliente <?php echo $filas_codigo['nombre_cliente']; ?></h2>
									&nbsp;&nbsp;
									<table class="table">
										<tr>
											<th>Folio</th>
											<th>Fecha Solicitud</th>
											<th>Fecha Respuesta</th>
											<th>Vigencia</th>
										</tr>
										<?php
										do{ 
										?>						
										<tr>
											<td align="center">
												<a href="busqueda_rut.php?id=<?php echo $filas_codigo['folio_propuesta']; ?>"><?php echo $filas_codigo['folio_propuesta']; ?></a>
											</td>
											<td align="center">
												<?php echo date("d-m-Y",strtotime($filas_codigo['fecha_solicitud'])); ?>
											</td>
											<td align="center">
												<?php echo date("d-m-Y",strtotime($filas_codigo['fecha_respuesta'])); ?>
											</td>
											<td align="center">
												<?php 
												if ($filas_codigo['segmento']=='GGCC') {
													echo date("d-m-Y", strtotime('+90 day',strtotime($filas_codigo['fecha_respuesta'])));
												}else{
													echo date("d-m-Y", strtotime('+60 day',strtotime($filas_codigo['fecha_respuesta'])));
												}
												 ?>
											</td>					
										</tr>
									<?php 
										} while ($filas_codigo= mysql_fetch_assoc($ejecutar_codigo));?>
									</table>
									<?php
								} else {
									echo "<tr><td colspan=2>"."No hay registros para el RUT consultado"."</td></tr>";
								}
								

							}

							 ?>




			<?php

			if (isset($_POST['volver']) and $_POST['volver']=="Volver") {
				header("location:".$URL_mesa_folio);
			}
			
			if (isset($_GET['id']) and $_GET['id']!=""){ 

				$folio = $_GET['id'];

				$consulta_propuesta= "SELECT * from propuesta,cliente where propuesta.folio_propuesta='$folio' and propuesta.rut_cliente=cliente.rut_cliente";
				mysql_query("SET NAMES 'utf8'");
				$ejecutar_codigo=mysql_query($consulta_propuesta,$conexion) or die(mysql_error());
				$filas_propuesta=mysql_fetch_assoc($ejecutar_codigo);
				$numero_propuesta=mysql_num_rows($ejecutar_codigo);

				echo "<h3>Folio: ".$filas_propuesta['folio_propuesta']."</h3>";

				?>

				<br>
				<table class="table">
					<tr>
						<th>Nombre Cliente</th>
						<th>Rut</th>
						<th>Cuenta</th>
						<th>Tipo Solicitud</th>
						<th>Fecha Respuesta</th>
						<th>Vigencia Propuesta</th>
					</tr>
					<?php
						do{ 
						?>						
					<tr>
						<td align="center">
							<?php echo $filas_propuesta['nombre_cliente']; ?>
						</td>
						<td align="center">
							<?php echo $filas_propuesta['rut_cliente']; ?>
						</td>
						<td align="center">
							<?php echo $filas_propuesta['cuenta']; ?>
						</td>
						<td align="center">
							<?php echo $filas_propuesta['tipo_solicitud']; ?>
						</td>
						<td align="center">
							<?php echo date("d-m-Y", strtotime($filas_propuesta['fecha_respuesta'])); ?>
						</td>
						<td align="center">
							<?php 
							if ($filas_propuesta['segmento']=='GGCC') {
								echo date("d-m-Y", strtotime('+90 day',strtotime($filas_propuesta['fecha_respuesta'])));
							}else{
								echo date("d-m-Y", strtotime('+60 day',strtotime($filas_propuesta['fecha_respuesta'])));
							}
							 ?>
						</td>
					</tr>
						<?php 
						} while ($filas_propuesta= mysql_fetch_assoc($ejecutar_codigo));?>
				</table>
				<br>

				<?php 
						$consulta_EC2= "SELECT * FROM linea, propuesta, propuesta_linea, tipo_linea 
						WHERE propuesta_linea.id_linea = linea.id_linea AND propuesta_linea.folio_propuesta = propuesta.folio_propuesta AND tipo_linea.id_tipo_linea = linea.id_tipo_linea 
						AND ((propuesta.folio_propuesta='$folio') AND (tipo_linea.nombre_tipo_linea='Empresa Conectada 2.0'))";
						mysql_query("SET NAMES 'utf8'");
						$ejecutar_codigo=mysql_query($consulta_EC2,$conexion) or die(mysql_error());
						$filas_consulta_EC2=mysql_fetch_assoc($ejecutar_codigo);
						$numero_EC2=mysql_num_rows($ejecutar_codigo);

				if ($numero_EC2<>0) { ?>
				<h3>Detalle Planes Empresa Conectada 2.0</h3>
				<br>
				<table class="table">
					<tr>
						<th>Nombre Línea</th>
						<th>Cant. Líneas CE</th>
						<th>Cant. Líneas SE</th>
						<th>Precio Línea</th>
						<th>Dscto. Fidelización</th>
						<th>Dscto. Adicional</th>
						<th>Total</th>
						<th>CF Final</th>
					</tr>
					<?php
						do{ 
						?>						
					<tr>
						<td align="center">
							<?php echo $filas_consulta_EC2['nombre_linea']; ?>
						</td>
						<td align="center">
							<?php echo $filas_consulta_EC2['cantidad_linea_CE']; ?>
						</td>
						<td align="center">
							<?php echo $filas_consulta_EC2['cantidad_linea_SE']; ?>
						</td>
						<td align="center">
							<?php echo "$".number_format($filas_consulta_EC2['precio_linea'],0,",","."); ?>
						</td>
						<td align="center">
							<?php echo number_format($filas_consulta_EC2['descto_fidelizacion']*100,2,",",".")."%"; ?>
						</td>
						<td align="center">
							<?php echo number_format($filas_consulta_EC2['descto_adicional']*100,2,",",".")."%"; ?>
						</td>
						<td align="center">
							<?php echo number_format(($filas_consulta_EC2['descto_fidelizacion']+$filas_consulta_EC2['descto_adicional'])*100,2,",",".")."%"; ?>
						</td>
						<td align="center">
							<?php echo "$".number_format($filas_consulta_EC2['precio_linea']*($filas_consulta_EC2['cantidad_linea_CE']+$filas_consulta_EC2['cantidad_linea_SE'])*(1-($filas_consulta_EC2['descto_fidelizacion']+$filas_consulta_EC2['descto_adicional'])),0,",","."); ?>
						</td>
					</tr>
						<?php 
						} while ($filas_consulta_EC2= mysql_fetch_assoc($ejecutar_codigo));?>
				</table>
				<?php } 
				
		$consulta_EC= "SELECT * FROM linea, propuesta, propuesta_linea, tipo_linea 
						WHERE propuesta_linea.id_linea = linea.id_linea AND propuesta_linea.folio_propuesta = propuesta.folio_propuesta AND tipo_linea.id_tipo_linea = linea.id_tipo_linea 
						AND ((propuesta.folio_propuesta='$folio') AND (tipo_linea.nombre_tipo_linea='Empresa Conectada'))";
						mysql_query("SET NAMES 'utf8'");
						$ejecutar_codigo=mysql_query($consulta_EC,$conexion) or die(mysql_error());
						$filas_consulta_EC=mysql_fetch_assoc($ejecutar_codigo);
						$numero_EC=mysql_num_rows($ejecutar_codigo);
				
				if ($numero_EC<>0) { ?>
				
				<br>
				<h3>Detalle Planes Empresa Conectada</h3>
				<br>
				<table class="table">
					<tr>
						<th>Nombre Línea</th>
						<th>Cant. Líneas CE</th>
						<th>Cant. Líneas SE</th>
						<th>Precio Línea</th>
						<th>Dscto. Fidelización</th>
						<th>Dscto. Adicional</th>
						<th>Dscto. AG CE</th>
						<th>Dscto. AG SE</th>
					</tr>
					<?php

						do{ 
						?>						
					<tr>
						<td align="center">
							<?php echo $filas_consulta_EC['nombre_linea']; ?>
						</td>
						<td align="center">
							<?php echo $filas_consulta_EC['cantidad_linea_CE']; ?>
						</td>
						<td align="center">
							<?php echo $filas_consulta_EC['cantidad_linea_SE']; ?>
						</td>
						<td align="center">
							<?php echo "$".number_format($filas_consulta_EC['precio_linea'],0,",","."); ?>
						</td>
						<td align="center">
							<?php echo number_format($filas_consulta_EC['descto_fidelizacion']*100,2,",",".")."%"; ?>
						</td>
						<td align="center">
							<?php echo number_format($filas_consulta_EC['descto_adicional']*100,2,",",".")."%"; ?>
						</td>
						<td align="center">
							<?php echo number_format($filas_consulta_EC['descto_autogestion_CE']*100,2,",",".")."%"; ?>
						</td>
						<td align="center">
							<?php echo number_format($filas_consulta_EC['descto_autogestion_SE']*100,2,",",".")."%"; ?>
						</td>
						
					</tr>
						<?php 
						} while ($filas_consulta_EC= mysql_fetch_assoc($ejecutar_codigo));?>
				</table>
				<?php } 

										$consulta_GES= "SELECT * FROM linea, propuesta, propuesta_linea, tipo_linea 
						WHERE propuesta_linea.id_linea = linea.id_linea AND propuesta_linea.folio_propuesta = propuesta.folio_propuesta AND tipo_linea.id_tipo_linea = linea.id_tipo_linea 
						AND ((propuesta.folio_propuesta='$folio') AND (tipo_linea.nombre_tipo_linea='GES Datos'))";
						mysql_query("SET NAMES 'utf8'");
						$ejecutar_codigo=mysql_query($consulta_GES,$conexion) or die(mysql_error());
						$filas_consulta_GES=mysql_fetch_assoc($ejecutar_codigo);
						$numero_GES=mysql_num_rows($ejecutar_codigo);

				if ($numero_GES<>0) { ?>
				<br>
				<h3>Detalle Planes Gestión Datos</h3>
				<br>
				<table class="table">
					<tr>
						<th>Nombre Línea</th>
						<th>Cant. Líneas CE</th>
						<th>Cant. Líneas SE</th>
						<th>Precio Línea</th>
						<th>Dscto. Fidelización</th>
						<th>Dscto. Adicional</th>
						<th>Dscto. AG CE</th>
						<th>Dscto. AG SE</th>
					</tr>
					<?php

						do{ 
						?>						
					<tr>
						<td align="center">
							<?php echo $filas_consulta_GES['nombre_linea']; ?>
						</td>
						<td align="center">
							<?php echo $filas_consulta_GES['cantidad_linea_CE']; ?>
						</td>
						<td align="center">
							<?php echo $filas_consulta_GES['cantidad_linea_SE']; ?>
						</td>
						<td align="center">
							<?php echo "$".number_format($filas_consulta_GES['precio_linea'],0,",","."); ?>
						</td>
						<td align="center">
							<?php echo number_format($filas_consulta_GES['descto_fidelizacion']*100,2,",",".")."%"; ?>
						</td>
						<td align="center">
							<?php echo number_format($filas_consulta_GES['descto_adicional']*100,2,",",".")."%"; ?>
						</td>
						<td align="center">
							<?php echo number_format($filas_consulta_GES['descto_autogestion_CE']*100,2,",",".")."%"; ?>
						</td>
						<td align="center">
							<?php echo number_format($filas_consulta_GES['descto_autogestion_SE']*100,2,",",".")."%"; ?>
						</td>
						
					</tr>
						<?php 
						} while ($filas_consulta_GES= mysql_fetch_assoc($ejecutar_codigo));?>
				</table>
				<?php } 

						$consulta_MMFF= "SELECT * FROM mmff, propuesta WHERE mmff.folio_propuesta = propuesta.folio_propuesta AND ((propuesta.folio_propuesta=$folio))";
						mysql_query("SET NAMES 'utf8'");
						$ejecutar_codigo=mysql_query($consulta_MMFF,$conexion) or die(mysql_error());
						$filas_consulta_MMFF=mysql_fetch_assoc($ejecutar_codigo);
						$numero_MMFF=mysql_num_rows($ejecutar_codigo);

				if ($filas_consulta_MMFF['cantidad_lineas_MMFF']<>0) { ?>
				<br>
				<h3>Planes MMFF</h3>
				<br>
				<table class="table">
					<tr>
						<th>Cant. Líneas MMFF</th>
						<th>Tarifa MMFF</th>
						<th>Total Min. MMFF</th>
						<th>Total Min. X Línea</th>
						<th>Mínimo Facturable</th>
						<th>Facturación MMFF</th>
					</tr>
					<?php

						do{ 
						?>						
					<tr>
						<td align="center">
							<?php echo $cantidad_lineas_MMFF=$filas_consulta_MMFF['cantidad_lineas_MMFF']; ?>
						</td>
						<td align="center">
							<?php $tarifa_mmff=$filas_consulta_MMFF['tarifa_mmff']; echo "$".$tarifa_mmff; ?>
						</td>
						<td align="center">
							<?php $total_minutos_mmff=$filas_consulta_MMFF['total_minutos_mmff']; echo number_format($total_minutos_mmff,0,",","."); ?>
						</td>
						
						<td align="center">
							<?php if ($cantidad_lineas_MMFF==0 ){$total_minutos_linea =0;}else{$total_minutos_linea = $total_minutos_mmff/$cantidad_lineas_MMFF;} echo $total_minutos_linea; ?>
						</td>
						<td align="center">
							<?php $minimo_facturable = $total_minutos_linea*$tarifa_mmff; echo "$".number_format($minimo_facturable,0,",","."); ?>
						</td>
						<td align="center">
							<?php $facturacion_mmff=$minimo_facturable*$cantidad_lineas_MMFF; echo  "$".number_format($facturacion_mmff,0,",","."); ?>
						</td>					
					</tr>
						<?php 
						} while ($filas_consulta_EC= mysql_fetch_assoc($ejecutar_codigo));?>
				</table>
				<?php }

					$consulta_Equipo= "SELECT linea.nombre_linea, equipo.nombre_equipo, propuesta_equipo.cantidad_equipo, propuesta_equipo.monto_equipo_comercial, 
					propuesta_equipo.monto_equipo_plan, propuesta_equipo.porcentaje_descuento_equipo, propuesta_equipo.codigo_descuento
					FROM equipo, linea, propuesta, propuesta_equipo, propuesta_linea
					WHERE propuesta_equipo.folio_propuesta = propuesta.folio_propuesta AND propuesta_linea.id_propuesta_linea = propuesta_equipo.id_propuesta_linea AND propuesta_linea.folio_propuesta = propuesta.folio_propuesta AND equipo.id_equipo = propuesta_equipo.id_equipo AND linea.id_linea = propuesta_linea.id_linea AND ((propuesta_linea.folio_propuesta=$folio))";
					mysql_query("SET NAMES 'utf8'");
					$ejecutar_codigo=mysql_query($consulta_Equipo,$conexion) or die(mysql_error());
					$filas_consulta_Equipo=mysql_fetch_assoc($ejecutar_codigo);
					$numero_Equipo=mysql_num_rows($ejecutar_codigo);

					if ($numero_Equipo<>0) { ?>
				<br>
					<h3>Detalle Equipos</h3>
				<br>
				<table class="table">
					<tr>
						<th>Plan</th>
						<th>Modelo Equipo</th>
						<th>Q Equipos</th>
						<th>Valor Cuota Inicial</th>
						<th>Valor en Plan</th>
						<th>% Descto</th>
						<th>Total Dscto Aprobado</th>
						<th>Total a Pagar</th>
						<th>Código Descto</th>
					</tr>
					<?php

						do{ 
						?>						
					<tr>
						<td align="center">
							<?php echo $filas_consulta_Equipo['nombre_linea']; ?>
						</td>
						<td align="center">
							<?php echo $filas_consulta_Equipo['nombre_equipo']; ?>
						</td>
						<td align="center">
							<?php echo $filas_consulta_Equipo['cantidad_equipo']; ?>
						</td>
						<td align="center">
							<?php echo "$".number_format($filas_consulta_Equipo['monto_equipo_comercial'],0,",","."); ?>
						</td>
						<td align="center">
							<?php echo "$".number_format($filas_consulta_Equipo['monto_equipo_plan'],0,",","."); ?>
						</td>
						<td align="center">
							<?php echo number_format($filas_consulta_Equipo['porcentaje_descuento_equipo']*100,2,",",".")."%"; ?>
						</td>
						<td align="center">
							<?php echo "$".number_format(($filas_consulta_Equipo['cantidad_equipo']*$filas_consulta_Equipo['monto_equipo_plan'])*($filas_consulta_Equipo['porcentaje_descuento_equipo']),0,",","."); ?>
						</td>
						<td align="center">
							<?php echo "$".number_format(($filas_consulta_Equipo['cantidad_equipo']*$filas_consulta_Equipo['monto_equipo_plan'])*(1-$filas_consulta_Equipo['porcentaje_descuento_equipo']),0,",","."); ?>
						</td>
						<td align="center">
							<?php echo $filas_consulta_Equipo['codigo_descuento']; ?>
						</td>

						
					</tr>
						<?php 
						} while ($filas_consulta_Equipo= mysql_fetch_assoc($ejecutar_codigo));?>
				</table>
				<?php } 

				 }?> 
						
						</section>	
						</tr>
					</table>
					</form>
		</article>
		<?php } ?>
	</section>
	<footer>
		<?php  
			require('../footer.php');
		?>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>